"""
Model predictive control of a double integrator to illustrate the usage of
the toolbox.

Feel free to play around with the various presets limits and weights.
"""
import logging
import numpy as np
import casadi as ca

from mpctoolbox import (
    System,
    OCPMode, OptimalControlProblem,
    MPCMode, MPController,
    InitialValueProblem, Simulator,
    Plotter
)


def create_system():
    # Set the system name.
    name = "double_integrator"

    # Set dimensions.
    x_dim = 2   # dimension of system state
    u_dim = 1   # dimension of system input

    # CasADi symbolic variables (only CasADi SX and MX symbolics are supported)
    # Note: MX symbolics are preferable for saving a string representation of
    # the system's rhs via System.save(). Regardless, SX symbolics will be used
    # internally for all future calculations.
    x = ca.MX.sym("x", x_dim)   # symbolic variable for state
    u = ca.MX.sym("u", u_dim)   # symbolic variable for input

    # Collect all symbolic variables in dictionary sym_dict.
    # This dictionary must contain entries 'x' and 'u'.
    # Additional entries with user-defined keys will be treated as parameters.
    sym_dict = dict(x=x, u=u)

    # Optional: Add user-defined descriptions.
    sxup_dict = {'name': name,
                 'x_0': 'position',
                 'x_1': 'velocity',
                 'u_0': 'acceleration',
                 }

    # Provide state-space representation (rhs) of the system using the
    # previously defined symbolic variables.
    x_dot_p = [x[1], u[0]]

    # Use vertical concatenation for MX expressions.
    x_dot_p = ca.vertcat(*x_dot_p)

    # Create System object. See constructor docstring for details.
    sys = System(x_dot_p, sym_dict, sxup_dict=sxup_dict)

    return sys


def create_ocp(sys):
    # Set the ocp mode and name.
    mode = OCPMode.XN
    name = f"{sys.sxup_dict['name']}_{mode}"

    # set a grid to use for discretisation
    t_grid = np.linspace(0, 1.5, num=100)
    t_cnt = len(t_grid)

    # Set initial system state and previous system input.
    x_0 = np.array([1, 0])
    uprev = np.zeros(sys.n_u)

    # Set state and input references either give a fixed reference or on for
    # each step.
    x_ref = np.zeros((sys.n_x, t_cnt))
    # x_ref[0, :-1] = 2   # stay position 2 if possible
    # x_ref[0, -1] = -.1  # make sure to end in 0, though
    u_ref = np.zeros((sys.n_u, t_cnt - 1))  # the less is more

    # Set lower bounds for state 'x', input 'u' and change of input 'Du'.
    lb = {
        "x": np.array([-5, -10]),
        "u": -1e2,
        "Du": -1e1,
    }
    # Set upper bounds for state 'x', input 'u' and change of input 'Du'.
    ub = {
        "x": np.array([5, 10]),
        "u": 1e2,
        "Du": 1e1,
    }

    # nonlinear constraints (state and input dependent)

    # step constraints (all but the last step)
    e = ca.vertcat(*[-sys.x[0], ])  # stay positive
    e_fun = ca.Function("e_func", [sys.x], [e], ['x'], ['e'])

    # final constraints (only last step)
    ef = ca.vertcat(*[(sys.x[0] + .05)**2, sys.x[1]**2])  # land in origin
    ef_fun = ca.Function("ef_func", [sys.x], [ef], ['x'], ['e'])

    # Set weight matrices for deviation from reference state and input
    q_mat = np.diag((100, 1))
    r_mat = np.diag((.1, ))

    # Specify keyword arguments of OCP constructor (can be omitted).
    extra_args = {
        "lb": lb,
        "ub": ub,
        # "e_func": e_fun,
        # "ef_func": ef_fun,
        "u_prev": uprev,
        "q_mat": q_mat,
        "r_mat": r_mat,
    }

    # Create OptimalControlProblem object. See constructor docstring for
    # details.
    ocp = OptimalControlProblem(sys, mode, x_0, t_grid, x_ref, u_ref,
                                **extra_args)
    return ocp


def create_controller(ocp):
    # Specify keyword arguments of MPCSolver constructor.
    kwargs = dict(hor_len=75,
                  verbosity=0,  # change this value to get more information
                  # show_step=True,  # enable this to get a plot for every step
                  )

    # Create MPCSolver object. See constructor docstring for details.
    solver = MPController(ocp, mode=MPCMode.UNLIMITED, **kwargs)
    return solver


def create_ivp(sys):
    # specify a finer time grid for simulation
    t_f = 1.5
    t_grid = np.linspace(0, t_f, num=1000)

    # specify an initial state (thsi can differ from the one used in planning)
    x0 = np.array([1, 0])

    ivp = InitialValueProblem(sys, x0, t_grid)
    return ivp


def plot_results(sim, sol, fname=None):
    to_plot = [[["x_0"], ["x_1"], ["u_0"]]]
    fig_size = [30/2.54, 20/2.54]
    font_size = 20
    fig_name = "_".join([sim.ivp.sys.sxup_dict["name"],
                         sim.cont.ocp.mode.name,
                         sim.cont.mode.name])

    tud_blue = "#00305d"
    colors = [[[tud_blue], [tud_blue], [tud_blue]]]
    drawstyles = [[["default"], ["default"], ["steps-post"]]]
    linewidths = [[[2], [2], [2]]]
    x_min = -0.03
    x_max = 1.53
    x_ticks = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5]

    labels = {'t':      r'$t$ in s $\rightarrow$',
              'x_0':    r'$\uparrow$' + '\n' + r'$x_0$ in m',
              'x_1':    r'$\uparrow$' + '\n' + r'$x_1$ in '
                        + r'$\mathrm{\frac{m}{s}}$',
              'u_0':    r'$\uparrow$' + '\n' + r'$u$ in '
                        + r'$\mathrm{\frac{m}{s^2}}$'}

    x_array = sol["t_grid"]
    x_label = labels["t"]

    y_arrays = []
    y_labels = []
    for i in range(len(to_plot)):
        pltr = Plotter(x_label, x_array)
        y_arrays.append([])
        for k in range(len(to_plot[i])):
            y_arrays[i].append([])
            y_labels.append('')
            for j in range(len(to_plot[i][k])):
                key = to_plot[i][k][j]
                if 'x' in key:
                    y_arrays[i][k].append(sol["x_sim"][:, int(key[-1])])
                elif 'u' in key:
                    y_arrays[i][k].append(sol["u_sim"][:, int(key[-1])])
                y_labels[-1] += key
            y_labels[-1] = labels[y_labels[-1]]
            pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                             drawstyles=drawstyles[i][k],
                             linewidths=linewidths[i][k])
        pltr.create_plot(fig_size, fig_name, font_size=font_size, show=True,
                         x_min=x_min, x_max=x_max, x_ticks=x_ticks, y_lpad=30,
                         file_path="../docs/source/intro")


def main():
    # create system, optimal control problem (ocp) and controller
    sys = create_system()
    ocp = create_ocp(sys)
    cont = create_controller(ocp)

    # formulate ivp and create a simulator
    ivp = create_ivp(sys)
    sim = Simulator(ivp, cont)

    # simulate the system
    sol = sim.solve()

    # show the results.
    plot_results(sim, sol)


if __name__ == '__main__':
    logging.basicConfig()

    # you want more info on what is going on? enable one of these:
    logging.getLogger("mpctoolbox.core.mpc").setLevel(logging.INFO)
    logging.getLogger("mpctoolbox.core.simulation").setLevel(logging.INFO)

    main()
