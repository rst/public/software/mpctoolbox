Utils Module
============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   animation.rst
   functions.rst
   plotter.rst
