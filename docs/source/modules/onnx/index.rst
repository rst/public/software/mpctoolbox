ONNX Module
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   adapter.rst
   model_import.rst
   parser.rst
   verifier.rst


The ONNX Metadata interface
---------------------------
The raw data is::

    path(str):                  Path to the .onnx file.
    input_tuple:                list of tuples (input_name_n,input_shape_n). order matters!
    input_transform:            how input data were prepared for training.
    input_means:
    input_variances:
    output_tuple:               list of tuples (output_name_n,output_shape_n). oder matters!
    output_transform:           how output data were prepared for training.
    output_means:
    output_variances:
    max_pred_horizon:           number of steps for which the model predictions are useable.
    input_data_range:           values ranges of inputdata used for training

