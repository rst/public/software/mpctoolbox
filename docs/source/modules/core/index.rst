Core Module
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   system.rst
   ocp.rst
   mpc.rst
   simulation.rst
   transforms.rst
