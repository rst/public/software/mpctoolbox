Getting started
---------------

To get to now the toolbox, take a look at the ``double_integrator.py`` script
in the ``examples`` folder.

System definition
=================

First, the system dynamics have to be specified. This can be done by using the
the class :class:`mpctoolbox.system`.

.. literalinclude:: ../../../examples/double_integrator.py
    :pyobject: create_system

Problem formulation
===================

Once the dynamics are done, we have to define what we want to do with the
system (for example a transition between two steady states).
Furthermore, the constraints and costs are defined in this step.
All Information are then to construct a
:class:`mpctoolbox.OptimalControlProblem` or `OCP` for short.

.. literalinclude:: ../../../examples/double_integrator.py
    :pyobject: create_ocp

Controller design
=================

Next, the controller (including the solver) to use has to be specified.
For all possible options have a look at :class:`mpctoolbox.MPController`.

.. literalinclude:: ../../../examples/double_integrator.py
    :pyobject: create_controller

Simulation
==========

At first we define the initial value problem to be solved

.. literalinclude:: ../../../examples/double_integrator.py
    :pyobject: create_ivp

then we pass that and out controller to the simulator and run it:

.. literalinclude:: ../../../examples/double_integrator.py
    :pyobject: main


Post Processing
===============

The see what actually happened, we call :func:`plot_results`:

.. image:: double_integrator_XN_UNLIMITED.png
