Documentation of the mpctoolbox package
=======================================

You probably want to have a look at the getting started guide first.
After that, you can find the complete API documentation for each module
below.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro/getting_started.rst
   modules/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
