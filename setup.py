import setuptools
import os

curr_dir = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(curr_dir, "README.rst"), "r") as f:
    long_desc = f.read()
short_desc = long_desc.split("\n\n")[1]

with open(os.path.join(curr_dir, "requirements.txt"), "r") as f:
    requirements = f.read()

setuptools.setup(
    name="mpctoolbox",
    version="0.0.1",
    author="Stefan Ecklebe",
    author_email="stefa.ecklebe@tu-dresden.de",
    description=short_desc,
    lonng_description=long_desc,
    long_description_content_type="text/x-rst",
    url="TODO",
    license="GPLv3",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPL License",
        "Operating System :: OS Independent",
    ],
    python_requieres=">=3.6",
    install_requires=requirements,
    extras_require={
        # "tests": ["codecov>=2.0.15"],
        "docs": ["sphinx>=3.2.1",
                 "sphinx-autoapi>=1.5.0",
                 "sphinx-rtd-theme>=0.5.0"],
        "ann": ["onnx",
                "netron",
                "torch",
                "keras",
                ]
    },
)
