"""
Classes and routines to perform simulations of open or closed loop systems

For the simplest use case, an `InitialValueProblem` (IVP) is constructed by
providing a system and an initial condition.
This IVP can then (together witch a controller) be passed to the `Simulator`
to be simulated.
"""
import logging
import numpy as np
import casadi as ca

from .system import System
from .mpc import SolverError


logger = logging.getLogger(__name__)


class InitialValueProblem:
    """
    Wrapper for initial value problems

    In general these take the form :math:`\dot x(t) = f(t, x(t), u(t))` with
    :math:`x(0) = x_0` .

    Args:
        sys (System): System to solve.
        x0 (np.ndarray): Initial values of the system.
        t_grid (numpy.ndarray): Time grid to use for simulation.
    """
    def __init__(self, sys, x0, t_grid):
        self.sys = sys
        assert len(x0) == sys.n_x
        self.x0 = x0
        self.t_grid = t_grid
        self.t_step = t_grid[1] - t_grid[0]


class Simulator:
    """
    Simulator for closed loop systems

    Args:
        ivp (InitialValueProblem): Initial value problem to solve via numeric
            simulation.
        control (MPController): Controller to close the loop.
        steps_receding (int): Number of time steps the optimization horizon
            recedes, thus, the length of the input sequence that is applied
            between two optimisation steps. Default to 1.
        sim_des_map (callable): Mapping from the state of the simulation model
            to the state of the design model. If not provided, he systems are
            queried via `get_state_map` for the mapping.
        atol (float): Absolute tolerance for the ode solver used in the
            simulation.
        rtol (float): Relative tolerance for the ode solver used in the
            simulation.
    """
    def __init__(self, ivp,
                 control=None, steps_receding=1,
                 sim_des_map=None,
                 atol=1e-14, rtol=1e-3):
        self.ivp = ivp
        self.cont = control
        if steps_receding != 1:
            raise NotImplementedError("Only input series of 1 supported")
        self.N_u = steps_receding

        self.sim_des_map = sim_des_map
        if self.sim_des_map is None:
            self.sim_des_map = ivp.sys.get_state_map(control.ocp.sys)
            if self.sim_des_map == NotImplemented:
                err = ValueError("`sim_des_map` not provided and deduction "
                                 "failed.")
                logger.exception(err)
                raise err

        # setup simulator
        ode = dict(x=self.ivp.sys.x,
                   p=ca.vcat((self.ivp.sys.u,
                              # self.cont.ocp.x_sp,
                              # self.cont.ocp.u_sp
                              )),
                   ode=self.ivp.sys.x_dot,
                   # quad=self.ivp.l_expr,
                   )
        options = dict(tf=self.ivp.t_step,
                       abstol=atol,
                       reltol=rtol)
        self.simulator = ca.integrator('simulator',
                                       'cvodes',
                                       ode,
                                       options,
                                       )

        # setup fields
        self.t_grid = self.ivp.t_grid
        self.x_sim = np.nan * np.ones((len(self.t_grid), len(self.ivp.x0)))
        self.x_sim[0] = ivp.x0
        self.u_sim = np.nan * np.zeros((len(self.t_grid),
                                        self.cont.ocp.sys.n_u))
        self.status = "ready"
        self.sol = None

        # if ocp_sim is not None:
        #     self.data['ocp_sim_hash'] = self.ocp_sim.data_hash
        #     if ocp_sim_x_dict is not None:
        #         self.data['ocp_sim_x_dict_hash'] = joblib_hash(ocp_sim_x_dict)

    def _simulate_steps(self, n_0, N_u, u_traj):
        """
        Propagate the simulation by a number of time steps.

        Args:
            n_0 (int): Current time index in the time grid.
            N_u (int): Number of steps to simulate.
            u_traj (list): Input trajectory of which the first `N_u` values are
                to be applied to the simulation.
        """
        assert len(u_traj) >= N_u
        # Apply back transform for u_opt
        # if hasattr(self.ocp.sys, "means_u"):
        #     means_u = self.ocp.sys.means_u
        #     variances_u = self.ocp.sys.variances_u
        #     u_opt = S(u_opt, means_u, variances_u)

        for n in range(n_0, n_0 + N_u):
            logger.info(f"Simulating step {n}/{len(self.t_grid)-1}")
            # simulate one step
            self.u_sim[n] = u_traj[n - n_0]
            sim_n = self.simulator(x0=self.x_sim[n],
                                   p=ca.vcat((self.u_sim[n],
                                              # self.cont.ocp.x_ref.T[n],
                                              # self.cont.ocp.u_ref.T[n]
                                              )))

            sim_n_xf = sim_n["xf"].full().squeeze()
            self.x_sim[n + 1] = sim_n_xf
            # self.L_sim[n] = sim_n['qf'].full()[0][0]
            # self.J_f += self.L_sim[n]

    def solve(self, cache_sol=True, force_solve=False):
        """
        Solves the specified optimal control problem.

        Keyword Args:
            cache_sol (bool): Whether to cash the determined solution.
            force_solve (bool): Whether to rerun the solver in spite of a cached
                solution.

        Returns:
            dict: Determined solution containing:
                - tgrid_sim (numpy.ndarray):
                    time grid used for simulation, i.e. ocp.t_grid
                - x_sim (numpy.ndarray):
                    state sequence as a result of applying the optimal
                    input sequence to the system specified via ocp.sys
                - u_sim (numpy.ndarray):
                    optimal input sequence determined by the MPCSolver
                    object, i.e. input sequence that was used to simulate
                    the system specified via ocp.sys
                - L_sim (numpy.ndarray):
                    stage cost sequence as a result of integrating
                    ocp.l_func with arguments x_sim and u_sim over each
                    time step
                - e_f (float):
                    error of final state, i.e. Euclidean distance between
                    final state and last element of state reference
                    specified via ocp.x_ref
                - J_f (float):
                    total cost at final time step, i.e. sum over L_sim
                - solve_times (list):
                    elapsed real time in
                    mpctools.solvers.ControlSolver.solve()
                    for each solved optimal control problem
                - alpha_a_sim (numpy.ndarray):
                    sequence of the estimated suboptimality degree alpha
                    via method a or one-step-method
                - alpha_b_sim (numpy.ndarray):
                    sequence of the estimated suboptimality degree alpha
                    via method b or two-step-method, i. e. implementation
                    of "a posteriori suboptimality estimate" described in
                    [Grüne and Pannek, Nonlinear Model Predictive Control,
                    2017, p. 318]
                - sol_status (str):
                    information about success of MPCSolver, i.e. last IPOPT
                    termination message
        """
        if 0:
            check_sol_hash = False
            try:
                cache = joblib_load('sols/sol_cache.dat')
                if self.data_hash in cache.keys():
                    sol = cache[self.data_hash]
                    if self.verbosity > 0:
                        print('Sol has already been cached.')
                    if force_solve:
                        sol.pop('solve_times')
                        sol_hash = joblib_hash(sol)
                        check_sol_hash = True
                    else:
                        self.sol = sol
                        if self.verbosity > 0:
                            print('sol_status: ' + sol['sol_status'])
                            print('e_f: {:.3e}'.format(sol['e_f']))
                            print('J_f: {:.3e}'.format(sol['J_f']))
                        if self.verbosity > 2:
                            self._plot()
                        return self.sol
            except FileNotFoundError:
                cache = {}

        self.status = "simulating"
        for n, t in enumerate(self.t_grid[:-1]):
            xd = self.sim_des_map(self.x_sim[n])
            try:
                xc, uc = self.cont.compute_feedback(t, xd)
            except SolverError as e:
                logger.exception(e)
                logger.error("Simulation stopped early at t={} (n={})"
                             .format(t, n))
                self.status = "failed"
                break
            self._simulate_steps(n, self.N_u, uc)
        else:
            self.status = "finished"

        # self.e_f = np.linalg.norm(self.x_sim[-1] - self.ocp_sim.x_ref[-1])

        # post processing
        self.sol = self._collect_solution_data()

        # if check_sol_hash:
        #     sol = self.sol.copy()
        #     sol.pop('solve_times')
        #     if joblib_hash(sol) != sol_hash:
        #         print('Warning: New sol is different from cached sol.')
        #
        # if cache_sol and self.sol_status != 'User_Requested_Stop':
        #     try:
        #         os.mkdir('sols/')
        #     except FileExistsError:
        #         pass
        #
        #     cache[self.data_hash] = self.sol
        #     joblib_dump(cache, 'sols/sol_cache.dat')

        return self.sol

    def _collect_solution_data(self):
        """
        Collect data from various endpoints and bundle in dictionary

        Returns: Dict, holding the solution
        """
        # TODO collect all data form controller, ocp, ivp etc.

        sol_keys = [
            "status",
            "t_grid",
            "x_sim",
            "u_sim",
            # "L_sim", "e_f", "J_f",
            # "solve_times",
            # "alpha_a_sim", "alpha_b_sim",
        ]
        sol = {key: getattr(self, key, None) for key in sol_keys}

        # if self.sol_status in ['Solve_Succeeded',
        #                        'Solved_To_Acceptable_Level']:
        #     self.sol.update({'x_opt_0': self.x_opts[0],
        #                      'u_opt_0': self.u_opts[0]})

        return sol
