# -*- coding: utf-8 -*-
'''
@author: Patrick Rüdiger

'''
import numpy as np

from mpctools.system import System
from mpctools.ocp import OptimalControlProblem

sys_names = {'1': 'double_int', '2': 'pend',
             '3': 'pend_cart_pl', '4': 'pend_cart',
             '5': 'dual_pend_cart_pl', '6': 'dual_pend_cart',
             '7': 'double_pend_cart_pl', '8': 'triple_pend_cart_pl'}

for name in sys_names.values():
    sys = System.load(name)
    T = 0.02
    x_ref = np.zeros((1, sys.n_x))
    u_ref = np.zeros((1, sys.n_u))

    if name == 'double_int':
        x_0 = np.array([1, 0])
        t_f = 1.5
        lb = {'x': np.array([-np.inf, -1]), 'u': -4, 'Du': -0.2}
        ub = {'x': np.array([np.inf, 1]), 'u': 4, 'Du': 0.2}

    elif name == 'pend':
        x_0 = np.array([np.pi, 0])
        t_f = 2
        lb = {}
        ub = {}

    elif name in ['pend_cart_pl', 'pend_cart']:
        x_0 = np.array([0, 0, np.pi, 0])
        t_f = 1
        lb = {'u': -10*9.81}
        ub = {'u': 10*9.81}

    elif name in ['dual_pend_cart_pl', 'dual_pend_cart']:
        x_0 = np.array([0, 0, np.pi, 0, np.pi, 0])
        t_f = 2
        lb = {}
        ub = {}

    elif name == 'double_pend_cart_pl':
        x_0 = np.array([0, 0, np.pi, 0, 0, 0])
        t_f = 3
        lb = {}
        ub = {}

    elif name == 'triple_pend_cart_pl':
        x_0 = np.array([0, 0, np.pi, 0, 0, 0, 0, 0])
        t_f = 3.5
        l_0 = 2.543
        bx = np.append(l_0/2, np.inf*np.ones(sys.n_x-1))
        lb = {'x': -bx, 'u': -4*9.81}
        ub = {'x': bx, 'u': 4*9.81}

    tgrid = np.linspace(0, t_f, int(t_f/T)+1)
    assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T

    for mode in ['xN', 'XNF', 'free']:
        ocp = OptimalControlProblem(sys, mode, x_0, tgrid, x_ref, u_ref,
                                    kwargs={'lb': lb, 'ub': ub})
        ocp.save(name+'_'+mode)
