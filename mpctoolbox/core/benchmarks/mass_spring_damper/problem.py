"""
OCP formulation for the mass-spring-damper system
"""
import numpy as np
import casadi as ca
import mpctoolbox as mt

from .system import ref_sys as sys

# Set initial system state and previous system input.
x_0_1d = np.array([1, -10])
x_0 = np.array([[1], [-10]])
u_prev = np.array([0])

# Specify time grid for simulation and discretization via time step size T
# and final time in seconds t_f.
T = 0.05
t_f = 1.0
t_grid = np.linspace(0, t_f, int(t_f / T) + 1)
assert t_grid[-1] == t_f and t_grid[1] - t_grid[0] == T

# Set state and input references
x_ref_const_1d = np.zeros(sys.n_x)
x_ref_const = np.zeros((sys.n_x, 1))
x_ref_var = np.zeros((sys.n_x, len(t_grid)))

u_ref_const_1d = np.ones(sys.n_u)
u_ref_const = np.ones((sys.n_u, 1))
u_ref_var = np.ones((sys.n_u, len(t_grid) - 1))


# Set weight matrices for deviation from reference state and input
q_mat = 1e3 * np.diag((10, 1))
r_mat = np.diag((1,))

# Stage costs for all but the last step
l_expr = 20 * (sys.x[0] - x_ref_const[0])**2 + 13.8 * sys.u[0]
l_func = ca.Function("l_func", [sys.x, sys.u], [l_expr], ["x", "u"], ["e"])

# Final costs the last step
lf_expr = 1e3 * (sys.x[1] - 2.38)**2 + sys.u[0]**2
lf_func = ca.Function("l_func", [sys.x, sys.u], [l_expr], ["x", "u"], ["e"])

# Set lower bounds for state 'x', input 'u' and change of input 'Du'.
lb = {
    "x": np.array([-5, -11]),
    "u": np.array([-30]),
    "Du": -20,
}
# Set upper bounds for state 'x', input 'u' and change of input 'Du'.
ub = {
    "x": np.array([5, 11]),
    "u": np.array([50]),
    "Du": 20,
}

# nonlinear constraints (state and input dependent)

# step constraints (all but the last step)
e_expr = ca.vertcat(*[0 - sys.x[0], ])  # stay positive in x[0]
e_func = ca.Function("e_func", [sys.x], [e_expr], ["x"], ["e"])

# final constraints (only last step)

# tolerance to reach
ef_x_tol = 1e-2

# explicit function
ef = ca.vertcat(*[(sys.x[0] + .05) ** 2, sys.x[1] ** 2])  # land in origin
ef_func = ca.Function("ef_func", [sys.x], [ef], ["x"], ["e"])


mode = mt.OCPMode.XN


def setup_ocps():
    args = dict(sys=sys,
                mode=mt.OCPMode.XN,
                x_0=x_0_1d,
                u_prev=u_prev,
                t_grid=t_grid,
                x_ref=x_ref_var,
                u_ref=u_ref_var,
                )

    extra_args = dict(lb=lb,
                      ub=ub,
                      ef_x_tol=ef_x_tol,
                      # e_func=e_func,
                      # ef_func=ef_func,
                      q_mat=q_mat,
                      r_mat=r_mat,
                      # l_func=l_func,
                      # lf_func=lf_func,
                      )
    xn_full = mt.OptimalControlProblem(**args,
                                       **extra_args)
    xn_red = mt.OptimalControlProblem(**args, q_mat=q_mat, r_mat=r_mat)
    return xn_red, xn_full


ocp_xn_red, ocp_xn_full = setup_ocps()
