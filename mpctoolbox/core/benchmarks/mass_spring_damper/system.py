"""
System formulation for the mass-spring-damper system
"""
import casadi as ca
import mpctoolbox as mt

x_dim = 2  # dimension of system state
u_dim = 1  # dimension of system input
x = ca.MX.sym("x", x_dim)  # symbolic variable for state
u = ca.MX.sym("u", u_dim)  # symbolic variable for input
mass = ca.MX.sym('m', 1)  # symbolic variable for mass
damp = ca.MX.sym('d', 1)  # symbolic variable for damping
stiff = ca.MX.sym('c', 1)  # symbolic variable for spring stiffness
sym_dict = dict(x=x, u=u)

# rhs
x_dot = [x[1], u[0] - x[1] - x[0]]
x_dot = ca.vertcat(*x_dot)

# rhs with parameters
x_dot_p = [x[1],
           (u[0]
            - damp * x[1]
            - stiff * x[0]
            ) / mass]
x_dot_p = ca.vertcat(*x_dot_p)

# parameters
p_dict = {
    mass: 1,  # [] = kg
    damp: 1,  # [] = ?
    stiff: 1,  # [] = N/m
}

n_dict = dict(
    x=([5, 2], [2.1, 1.2]),
    u=([-2], [1.5]),
)

# meta data
sxup_dict = dict(name="Test_System",
                 x_0="position",
                 x_1="velocity",
                 u_0="force",
                 m="mass",
                 d="damping",
                 c="stiffness",
                 )

# build reference system
ref_sys = mt.System(x_dot_p, sym_dict, p_dict, sxup_dict)

# build "real" system
p_sim = p_dict.copy()
p_sim[mass] *= 2
sim_sys = mt.System(x_dot_p, sym_dict, p_sim, sxup_dict)
