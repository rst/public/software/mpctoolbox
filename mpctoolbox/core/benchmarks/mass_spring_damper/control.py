"""
Controllers for the mass-spring-damper system
"""
import mpctoolbox as mt

from .problem import ocp_xn_red, ocp_xn_full


def get_c_dict():
    d = {m: mt.MPController(ocp_xn_full, hor_len=10, mode=m)
         for m in mt.MPCMode}
    return d
