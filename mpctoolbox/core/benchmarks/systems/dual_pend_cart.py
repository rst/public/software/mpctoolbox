# -*- coding: utf-8 -*-
"""
Dual pendulum on cart: Derivation of a (partially linearized) state space model

Based on examples by C. Knoll
https://nbviewer.jupyter.org/github/cknoll/beispiele/tree/master/

Author: Patrick Rüdiger
"""
import sympy as sp
import symbtools as st
import symbtools.modeltools as mt
from joblib import dump


# generalized passive (p) and active (q) coordinates
Np = 2
Nq = 1
n = Np + Nq
pp = st.symb_vector("p1:{0}".format(Np+1))
qq = st.symb_vector("q1:{0}".format(Nq+1))

# generalized coordinates and their time derivatives
ttheta = st.row_stack(pp, qq)
tthetad = st.time_deriv(ttheta, ttheta)
tthetadd = st.time_deriv(ttheta, ttheta, order=2)
st.make_global(ttheta, tthetad, tthetadd)

# parameters
params = sp.symbols('l1, l2, s1, s2, m1, m2, m0, g')
st.make_global(params)

# force on cart
tau1 = sp.Symbol("tau1")

# unit vectors
ex = sp.Matrix([1,0])
ey = sp.Matrix([0,1])

# centers of mass of cart and pendulums
S0 = ex*q1                  # center of mass of cart
G0 = S0                     # joint between cart and first pendulum
G1 = S0                     # joint between cart and second pendulum
S1 = G0 + mt.Rz(p1)*ey*s1   # center of mass of first pendulum
S2 = G1 + mt.Rz(p2)*ey*s2   # center of mass of second pendulum

# time derivatives of centers of mass
Sd0, Sd1, Sd2 = st.col_split(st.time_deriv(st.col_stack(S0, S1, S2), ttheta))

# kinetic energy
T_rot = 0
T_trans = (m0*Sd0.T*Sd0 + m1*Sd1.T*Sd1 + m2*Sd2.T*Sd2)/2
T = T_rot + T_trans[0]

# potential energy
V = m1*g*S1[1] + m2*g*S2[1]

# get state space model via Euler-Lagrange
mod = mt.generate_symbolic_model(T, V, ttheta, [0, 0, tau1])

# get rhs of state space model
mod.calc_state_eq(simplify=True)
x_dot = mod.f + mod.g*tau1

# get rhs of partially linearized state space model
mod.calc_coll_part_lin_state_eq(simplify=True)
x_dot_pl = mod.ff + mod.gg*qddot1

# change order of states
x_dot = [x_dot[2], x_dot[5], x_dot[0], x_dot[3], x_dot[1], x_dot[4]]
x_dot_pl = [x_dot_pl[2], x_dot_pl[5], x_dot_pl[0],
            x_dot_pl[3], x_dot_pl[1], x_dot_pl[4]]

# convert rhs to string that can later be evaluated via casadi imported as ca
replacements = {'sin':      'ca.sin',
                'cos':      'ca.cos',
                'q1':       'x[0]',
                'qdot1':    'x[1]',
                'qddot1':   'u[0]',
                'p1':       'x[2]',
                'pdot1':    'x[3]',
                'p2':       'x[4]',
                'pdot2':    'x[5]',
                'tau1':     'u[0]'}


def str_replace_all(string, replacements):
    for (key, val) in replacements.items():
        string = string.replace(key, val)
    return string


x_dot_str = str_replace_all(str(x_dot), replacements)
x_dot_pl_str = str_replace_all(str(x_dot_pl), replacements)

# save string, state and input dimensions
dump({'x_dot_str': x_dot_str, 'x_dim': len(x_dot), 'u_dim': 1},
     'examples/dual_pend_cart.str')
dump({'x_dot_str': x_dot_pl_str, 'x_dim': len(x_dot), 'u_dim': 1},
     'examples/dual_pend_cart_pl.str')
