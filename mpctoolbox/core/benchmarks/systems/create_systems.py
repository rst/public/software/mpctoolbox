# -*- coding: utf-8 -*-
'''
@author: Patrick Rüdiger

'''
import casadi as ca
from joblib import load

from mpctools.system import System

sys_names = {'1': 'double_int', '2': 'pend',
             '3': 'pend_cart_pl', '4': 'pend_cart',
             '5': 'dual_pend_cart_pl', '6': 'dual_pend_cart',
             '7': 'double_pend_cart_pl', '8': 'triple_pend_cart_pl'}

for name in sys_names.values():
    x_dim = load('systems/'+name+'.str')['x_dim']
    u_dim = load('systems/'+name+'.str')['u_dim']
    sym_dict = {'x': ca.MX.sym('x', x_dim), 'u': ca.MX.sym('u', u_dim)}
    sxup_dict = {'name': name}
    if name == 'double_int':
        p_val_dict = {}
        sxup_dict.update({'x_0': 's', 'x_1': 'v', 'u_0': 'a'})

    elif name == 'pend':
        p_val_dict = {'l': 0.5, 'g': 9.81}
        sxup_dict.update({'x_0': 'theta', 'x_1': 'omega', 'u_0': 'alpha'})

    elif name in ['pend_cart_pl', 'pend_cart']:
        p_val_dict = {'s1': 0.25, 'm1': 0.1, 'm0': 1.0, 'g': 9.81}
        sxup_dict.update({'x_0': 's', 'x_1': 'v',
                          'x_2': 'theta', 'x_3': 'omega'})
        if 'pl' in name:
            sxup_dict.update({'u_0': 'a'})
        else:
            sxup_dict.update({'u_0': 'F'})

    elif name in ['dual_pend_cart_pl', 'dual_pend_cart']:
        p_val_dict = {'s1': 0.7, 'm1': 0.7, 's2': 0.5, 'm2': 0.5,
                      'm0': 1.0, 'g': 9.81}
        sxup_dict.update({'x_0': 's', 'x_1': 'v',
                          'x_2': 'theta1', 'x_3': 'omega1',
                          'x_4': 'theta2', 'x_5': 'omega2'})
        if 'pl' in name:
            sxup_dict.update({'u_0': 'a'})
        else:
            sxup_dict.update({'u_0': 'F'})

    elif name == 'double_pend_cart_pl':
        p_val_dict = {'l1': 0.5, 'l2': 0.5, 's1': 0.5/2, 's2': 0.5/2,
                      'm1': 0.1, 'm2': 0.1,
                      'J1': 1/12*0.1*0.5**2, 'J2': 1/12*0.1*0.5**2,
                      'm0': 1.0, 'g': 9.81}
        sxup_dict.update({'x_0': 's', 'x_1': 'v',
                          'x_2': 'theta1', 'x_3': 'omega1',
                          'x_4': 'theta2', 'x_5': 'omega2',
                          'u_0': 'a'})

    elif name == 'triple_pend_cart_pl':
        p_val_dict = {'l1': 0.320, 'l2': 0.419, 'l3': 0.485,
                      's1': 0.21027, 's2': 0.27079, 's3': 0.22421,
                      'm1': 0.8512, 'm2': 0.8973, 'm3': 0.5519,
                      'J1': 0.01338, 'J2': 0.02352, 'J3': 0.01853,
                      'm0': 3.34, 'g': 9.81}

        sxup_dict.update({'x_0': 's', 'x_1': 'v',
                          'x_2': 'theta1', 'x_3': 'omega1',
                          'x_4': 'theta2', 'x_5': 'omega2',
                          'x_6': 'theta3', 'x_7': 'omega3',
                          'u_0': 'a'})

    sym_dict.update({key: ca.MX.sym(key, 1) for key in p_val_dict.keys()})
    x_dot_p = eval(load('systems/'+name+'.str')['x_dot_str'],
                   {'ca': ca}, sym_dict)
    x_dot_p = ca.vertcat(*x_dot_p)
    sys = System(x_dot_p, sym_dict, p_val_dict, sxup_dict)
    sys.save(name)
