# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 09:48:09 2019

@author: Patrick
"""

from joblib import dump

x_dot_str = '[x[1], u[0] + g/l*ca.sin(x[0])]'
dump({'x_dot_str': x_dot_str, 'x_dim': 2, 'u_dim': 1}, 'examples/pend.str')