# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ani import create_animation
from mpctools.ocp import OptimalControlProblem


np.random.seed(7)
u_guess = np.random.normal(loc=0.0, scale=4*9.81/3, size=(175, 1))
x_guess = np.random.normal(loc=0.0, scale=10/3, size=(176, 8))
solver_kwargs = {'N': 175, 'receding': 'unltd',
                 'u_guess': u_guess, 'x_guess': x_guess}
ocp = OptimalControlProblem.load('triple_pend_cart_pl_free_opt')
solver = MPCSolver(ocp, **solver_kwargs)
sol_0 = solver.solve()
u_opt_0 = sol_0['u_opt_0']
x_opt_0 = sol_0['x_opt_0']

solver_kwargs = {'N': 100, 'receding': 'unltd',
                 'u_guess': u_opt_0[:100], 'x_guess': x_opt_0[:101]}
solver = MPCSolver(ocp, **solver_kwargs)
sol = solver.solve()

name = 'triple_pend_cart_pl_free_opt_Init'
fstr1 = '{:<35} {:<10} {:<10} {:<10} {:<10}'
fstr2 = '{:<35} {:<10.3e} {:<10.1f} {:<10.3e} {:<10.3e}'
print(fstr1.format('', 'e_f', 'J_f', 'T_f', 'T_0/T_f'))
print(fstr2.format(name, sol['e_f'], sol['J_f'], sum(sol['solve_times']),
                   sol['solve_times'][0]/sum(sol['solve_times'])))
ani = create_animation('triple_pend_cart_pl', sol, 'Aufschwingen des DWPS',
                       file_path='examples/anis', filename=name)

xu_dict = {'x_0':  'x_0',
           'x_1':  'x_2',
           'x_2':  'x_4',
           'x_3':  'x_6',
           'x_4':  'x_1',
           'x_5':  'x_3',
           'x_6':  'x_5',
           'x_7':  'x_7',
           'u':    'u_0'}
ys_to_plot = [[['x_0'], ['x_4'], ['u'], ['L_sim'],
              ['alpha_a_sim', 'alpha_b_sim']],
              [['x_1'], ['x_5'], ['x_2'], ['x_6'], ['x_3'], ['x_7']]]
fig_size = [7.75/2.54, 15/2.54]
file_path = 'examples/plots'
filename = 'MPC_free_DWPS_Init_'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
colors = [[[tud_blue], [tud_blue], [tud_blue], [tud_blue],
           [tud_blue, tud_cyan]],
          [[tud_blue], [tud_blue], [tud_blue], [tud_blue], [tud_blue],
           [tud_blue]]]
drawstyles = [[['default'], ['default'], ['steps-post'], ['default'],
               ['default', 'default']],
              [['default'], ['default'], ['default'], ['default'],
               ['default'], ['default']]]
y_scales = [['linear', 'linear', 'linear', 'log', 'linear'],
            ['linear', 'linear', 'linear', 'linear', 'linear', 'linear']]
x_min = -0.07
x_max = 3.57
x_ticks = [0, 0.7, 1.4, 2.1, 2.8, 3.5]
x_tick_labels = None
y_ticks = [[[-0.75, -0.5, -0.25, 0, 0.25], [-2, 0, 2],
            [-10, 0, 10, 20], [1e-4, 1e-2, 1], [-6, -3, 1]],
           [[0, 90, 180], None, [-45, 0, 45], None,
            [-90, 0, 90, 180], None]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
alpha_a_str = r'$\hat{\alpha}_\mathrm{ES},'
alpha_b_str = r'\hat{\alpha}_\mathrm{ZS}$'
labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
          'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
          'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_3':    y_str_1 + r'$\widetilde{x}_3\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_4':    y_str_1 + r'$\widetilde{x}_4\mathrm{~in~\frac{m}{s}}$'
                    + y_str_2,
          'x_5':    y_str_1
                    + r'$\widetilde{x}_5\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'x_6':    y_str_1
                    + r'$\widetilde{x}_6\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'x_7':    y_str_1
                    + r'$\widetilde{x}_7\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2,
          'L_sim':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2,
          'alpha_a_simalpha_b_sim':     y_str_1 + alpha_a_str + alpha_b_str
                                        + y_str_2}

x_array = sol['tgrid_sim']
x_label = labels['t']
sxup_dict = ocp.sys.sxup_dict
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            if key in xu_dict.keys() and 'x' in key:
                y_arrays[i][k].append(sol['x_sim'][:,
                                                   int(xu_dict[key][-1])])
                if np.any(['theta' in sxup_dict[xu_dict[key]],
                           'omega' in sxup_dict[xu_dict[key]],
                           'alpha' in sxup_dict[xu_dict[key]]]):
                    y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
            elif key in xu_dict.keys() and 'u' in key:
                y_arrays[i][k].append(sol['u_sim'][:,
                                                   int(xu_dict[key][-1])])
            else:
                y_arrays[i][k].append(sol[key])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, fig_name=filename+str(i)+'.pdf',
                     file_path=file_path, show=show, usetex=usetex,
                     y_scales=y_scales[i], x_min=x_min, x_max=x_max,
                     x_ticks=x_ticks, x_tick_labels=x_tick_labels,
                     y_ticks=y_ticks[i], y_lpad=25)
