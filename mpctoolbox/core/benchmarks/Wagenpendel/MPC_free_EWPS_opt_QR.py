# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem
from mpctools.mpc import MPCSolver, optimize_QR


sol_dict = {'rhc_xN_ltd':       None,
            'rhc_free_ltd':     None,
            'rhc_free_ltd_2':   None}
solver_kwargs = {'N':           25,
                 'receding':    'ltd'}
xu_dict = {'x_0':  'x_0',
           'x_1':  'x_2',
           'x_2':  'x_1',
           'x_3':  'x_3',
           'u':    'u_0'}

ocp = OptimalControlProblem.load('pend_cart_pl_xN')
solver = MPCSolver(ocp, **solver_kwargs)
sol_dict.update({'rhc_xN_ltd': solver.solve()})

Q_opt, R_opt, L_opt, solver, sol = optimize_QR(ocp, solver_kwargs)

ocp = OptimalControlProblem.load('pend_cart_pl_free')
ocp.reconstruct(data={'Q': Q_opt, 'R': R_opt})
ocp.save('pend_cart_pl_free_opt')
solver = MPCSolver(ocp, **solver_kwargs)
sol_dict.update({'rhc_free_ltd': solver.solve()})

ocp.reconstruct(data={'Q': Q_opt, 'R': R_opt*5})
solver = MPCSolver(ocp, **solver_kwargs)
sol_dict.update({'rhc_free_ltd_2': solver.solve()})

ys_to_plot = [[['x_0'], ['x_2'], ['u']], [['x_1'], ['x_3'], ['L_sim']]]
fig_size = [7.75/2.54, 10/2.54]
file_path = 'examples/plots'
filename = 'MPC_free_EWPS_opt_QR_'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
tud_alert = '#ee7f00'
colors = [[[tud_cyan, tud_alert, tud_blue], [tud_cyan, tud_alert, tud_blue],
           [tud_cyan, tud_alert, tud_blue]],
          [[tud_cyan, tud_alert, tud_blue], [tud_cyan, tud_alert, tud_blue],
           [tud_cyan, tud_alert, tud_blue]]]
drawstyles = [[['default', 'default', 'default'],
               ['default', 'default', 'default'],
               ['steps-post', 'steps-post', 'steps-post']],
              [['default', 'default', 'default'],
               ['default', 'default', 'default'],
               ['default', 'default', 'default']]]
y_scales = [['linear', 'linear', 'linear'], ['linear', 'linear', 'log']]
x_min = -0.02
x_max = 1.02
y_mins = [[None, None, None], [None, None, 1e-3]]
y_maxs = [[None, None, None], [None, None, 1e5]]
y_ticks = [[None, None, None], [[0, 90, 180], None, None]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
          'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
          'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~\frac{m}{s}}$'
                    + y_str_2,
          'x_3':    y_str_1
                    + r'$\widetilde{x}_3\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2,
          'L_sim':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2,
          'L_opt':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2}

x_array = sol_dict['rhc_xN_ltd']['tgrid_sim']
x_label = labels['t']
sxup_dict = ocp.sys.sxup_dict
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            for (sol_key, sol) in sol_dict.items():
                if key in xu_dict.keys() and 'x' in key:
                    y_arrays[i][k].append(sol['x_sim'][:,
                                                       int(xu_dict[key][-1])])
                    if sxup_dict[xu_dict[key]] in ['theta', 'omega', 'alpha']:
                        y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
                elif key in xu_dict.keys() and 'u' in key:
                    y_arrays[i][k].append(sol['u_sim'][:,
                                                       int(xu_dict[key][-1])])
                elif key == 'L_sim' and 'rhc_free_ltd' in sol_key:
                    y_arrays[i][k].append(sol[key])
                elif key == 'L_sim' and 'rhc_xN_ltd' in sol_key:
                    y_arrays[i][k].append(L_opt)
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, filename+str(i)+'.pdf', file_path=file_path,
                     show=show, usetex=usetex, y_scales=y_scales[i],
                     y_mins=y_mins[i], y_maxs=y_maxs[i], x_min=x_min,
                     x_max=x_max, y_ticks=y_ticks[i])

print('{:<15} {:<10}'.format('', 'e_f'))
for (key, sol) in sol_dict.items():
    print('{:<15} {:<10.1e}'.format(key, sol['e_f']))
