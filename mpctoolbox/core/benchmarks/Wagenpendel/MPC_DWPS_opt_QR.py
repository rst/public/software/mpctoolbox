# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np
import casadi as ca
from joblib import load as joblib_load

from mpctools.system import System
from mpctools.plotter import Plotter
from mpctools.ani import create_animation
from mpctools.ocp import OptimalControlProblem
from mpctools.mpc import MPCSolver, optimize_QR


def create_system():
    name = 'triple_pend_cart_pl'

    try:
        sys = System.load(name)
        return sys
    except KeyError:
        pass

    x_dim = joblib_load('systems/'+name+'.str')['x_dim']
    u_dim = joblib_load('systems/'+name+'.str')['u_dim']
    sym_dict = {'x': ca.MX.sym('x', x_dim), 'u': ca.MX.sym('u', u_dim)}
    sxup_dict = {'name': name, 'x_0': 's', 'x_1': 'v',
                 'x_2': 'theta1', 'x_3': 'omega1',
                 'x_4': 'theta2', 'x_5': 'omega2',
                 'x_6': 'theta3', 'x_7': 'omega3',
                 'u_0': 'a'}

    p_val_dict = {'l1': 0.320, 'l2': 0.419, 'l3': 0.485,
                  's1': 0.21027, 's2': 0.27079, 's3': 0.22421,
                  'm1': 0.8512, 'm2': 0.8973, 'm3': 0.5519,
                  'J1': 0.01338, 'J2': 0.02352, 'J3': 0.01853,
                  'm0': 3.34, 'g': 9.81}

    sym_dict.update({key: ca.MX.sym(key, 1) for key in p_val_dict.keys()})
    x_dot_p = eval(joblib_load('systems/'+name+'.str')['x_dot_str'],
                   {'ca': ca}, sym_dict)
    x_dot_p = ca.vertcat(*x_dot_p)
    sys = System(x_dot_p, sym_dict, p_val_dict, sxup_dict)
    sys.save(name)

    return sys


def create_ocps():
    T = 0.02
    x_ref = np.zeros((1, sys.n_x))
    u_ref = np.zeros((1, sys.n_u))

    x_0 = np.array([0, 0, np.pi, 0, 0, 0, 0, 0])
    t_f = 3.5
    l_0 = 2.543
    bx = np.append(l_0/2, np.inf*np.ones(sys.n_x-1))
    lb = {'x': -bx, 'u': -4*9.81}
    ub = {'x': bx, 'u': 4*9.81}

    tgrid = np.linspace(0, t_f, int(t_f/T)+1)
    assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T

    ocp_dict = {}
    for mode in ['xN', 'XNF', 'free']:
        name = sys.sxup_dict['name'] + '_' + mode
        try:
            ocp_dict[name] = OptimalControlProblem.load(name)
        except KeyError:
            kwargs = {'lb': lb, 'ub': ub}
            if mode == 'XNF':
                kwargs['sigma'] = 1e6
            ocp_dict[name] = OptimalControlProblem(sys, mode, x_0, tgrid,
                                                   x_ref, u_ref, kwargs)
            ocp_dict[name].save(name)

    return ocp_dict


def create_solvers():
    np.random.seed(7)
    u_guess = np.random.normal(loc=0.0, scale=4*9.81/3, size=(175, 1))
    x_guess = np.random.normal(loc=0.0, scale=10/3, size=(176, 8))
    solver_kwargs = {'N': 175, 'u_guess': u_guess, 'x_guess': x_guess,
                     'verbosity': 2}
    solver_kwargs_updates = {'xN_unltd':    {'receding': 'unltd'},
                             'xN_global':   {'receding': 'global'},
                             'XNF_unltd':   {'receding': 'unltd'}}

    solver_dict = {}
    for key in solver_kwargs_updates.keys():
        solver_kwargs.update(solver_kwargs_updates[key])
        solver_dict[key] = MPCSolver(ocp_dict[solver_ocp_dict[key]],
                                     **solver_kwargs)

    return solver_dict


def run_solvers():
    sol_dict = {}

    for (key, solver) in solver_dict.items():
        sol_dict[key] = solver.solve()

    return sol_dict


def create_opt_ocps():
    solver_name = 'xN_unltd'
    solver_data = solver_dict[solver_name].data
    solver_kwargs = {'N': solver_data['N'], 'u_guess': solver_data['u_guess'],
                     'x_guess': solver_data['x_guess'],
                     'receding': solver_data['receding']}
    ocp = ocp_dict[solver_ocp_dict[solver_name]]
    Q_opt, R_opt, L_opt, solver, sol = optimize_QR(ocp, solver_kwargs)

    for key in ocp_dict.keys():
        ocp = OptimalControlProblem.load(key)
        ocp.reconstruct(data={'Q': Q_opt, 'R': R_opt})
        ocp.save(key+'_opt')

    return Q_opt, R_opt


def print_results():
    fstr1 = '{:<15} {:<10} {:<10} {:<10} {:<10}'
    fstr2 = '{:<15} {:<10.3e} {:<10.1f} {:<10.3e} {:<10.3e}'
    print(fstr1.format('', 'e_f', 'J_f', 'T_f', 'T_0/T_f'))
    for (key, val) in sol_dict.items():
        print(fstr2.format(key, val['e_f'], val['J_f'],
                           sum(val['solve_times']),
                           val['solve_times'][0]/sum(val['solve_times'])))


def plot_results():
    xu_dict = {'x_0':  'x_0',
               'x_1':  'x_2',
               'x_2':  'x_4',
               'x_3':  'x_6',
               'x_4':  'x_1',
               'x_5':  'x_3',
               'x_6':  'x_5',
               'x_7':  'x_7',
               'u':    'u_0'}

    to_plot = [[['x_0'], ['x_4'], ['u'], ['L_sim']],
               [['x_1'], ['x_5'], ['x_2'], ['x_6'], ['x_3'], ['x_7']]]
    fig_size = [7.75/2.54, 15/2.54]
    file_path = 'examples/plots'
    filename = 'MPC_DWPS_opt_QR_'
    show = False
    usetex = True

    tud_alert = '#ee7f00'
    tud_blue = '#00305d'
    tud_cyan = '#009de0'
    colors = [[[tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan]],
              [[tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan],
               [tud_alert, tud_blue, tud_cyan]]]
    drawstyles = [[['default', 'default', 'default'],
                   ['default', 'default', 'default'],
                   ['steps-post', 'steps-post', 'steps-post'],
                   ['default', 'default', 'default']],
                  [['default', 'default', 'default'],
                   ['default', 'default', 'default'],
                   ['default', 'default', 'default'],
                   ['default', 'default', 'default'],
                   ['default', 'default', 'default'],
                   ['default', 'default', 'default']]]
    y_scales = [['linear', 'linear', 'linear', 'log'],
                ['linear', 'linear', 'linear', 'linear', 'linear', 'linear']]
    x_min = -0.07
    x_max = 3.57
    x_ticks = [0, 0.7, 1.4, 2.1, 2.8, 3.5]
    x_tick_labels = None
    y_ticks = [[[-0.5, 0, 0.5, 1], [-2, -1, 0, 1, 2, 3],
                [-10, 0, 10, 20], [1e-2, 1e-1, 1, 10]],
               [[0, 90, 180, 270], [-1e3, 0], [-90, 0, 90], [0, 1e3],
                [-180, 0, 180, 360], [-1e3, 0, 1e3]]]
    y_tick_labels = [[None, None, None, None],
                     [None, [r'$-10^3$', r'$0$'], None, [r'$0$', r'$10^3$'],
                      None, [r'$-10^3$', r'$0$', r'$10^3$']]]

    y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
    y_str_2 = r'\end{center}'
    labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
              'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
              'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                        + y_str_2,
              'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~{}^\circ}$'
                        + y_str_2,
              'x_3':    y_str_1 + r'$\widetilde{x}_3\mathrm{~in~{}^\circ}$'
                        + y_str_2,
              'x_4':    y_str_1 + r'$\widetilde{x}_4\mathrm{~in~\frac{m}{s}}$'
                        + y_str_2,
              'x_5':    y_str_1
                        + r'$\widetilde{x}_5\mathrm{~in~\frac{{}^\circ}{s}}$'
                        + y_str_2,
              'x_6':    y_str_1
                        + r'$\widetilde{x}_6\mathrm{~in~\frac{{}^\circ}{s}}$'
                        + y_str_2,
              'x_7':    y_str_1
                        + r'$\widetilde{x}_7\mathrm{~in~\frac{{}^\circ}{s}}$'
                        + y_str_2,
              'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2,
              'L_sim':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2}

    x_array = sol_dict['xN_global']['tgrid_sim']
    x_label = labels['t']
    sxup_dict = sys.sxup_dict
    y_arrays = []
    y_labels = []
    for i in range(len(to_plot)):
        pltr = Plotter(x_label, x_array)
        y_arrays.append([])
        for k in range(len(to_plot[i])):
            y_arrays[i].append([])
            y_labels.append('')
            for j in range(len(to_plot[i][k])):
                key = to_plot[i][k][j]
                for sol in sol_dict.values():
                    if key in xu_dict.keys() and 'x' in key:
                        x_sim_idx = int(xu_dict[key][-1])
                        y_arrays[i][k].append(sol['x_sim'][:, x_sim_idx])
                        if np.any(['theta' in sxup_dict[xu_dict[key]],
                                   'omega' in sxup_dict[xu_dict[key]],
                                   'alpha' in sxup_dict[xu_dict[key]]]):
                            y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
                    elif key in xu_dict.keys() and 'u' in key:
                        u_sim_idx = int(xu_dict[key][-1])
                        y_arrays[i][k].append(sol['u_sim'][:, u_sim_idx])
                    else:
                        y_arrays[i][k].append(sol[key])
                y_labels[-1] += key
            y_labels[-1] = labels[y_labels[-1]]
            pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                             drawstyles=drawstyles[i][k])
        pltr.create_plot(fig_size, filename+str(i)+'.pdf', file_path=file_path,
                         show=show, usetex=usetex, y_scales=y_scales[i],
                         x_min=x_min, x_max=x_max, x_ticks=x_ticks,
                         x_tick_labels=x_tick_labels, y_ticks=y_ticks[i],
                         y_tick_labels=y_tick_labels[i])


def animate_results():
    for (key, val) in sol_dict.items():
        ani = create_animation('triple_pend_cart_pl', val,
                               'Aufschwingen des DWPS',
                               file_path='examples/anis',
                               filename='triple_pend_cart_pl_'+key)


if __name__ == '__main__':
    sys = create_system()
    solver_ocp_dict = {'xN_unltd':    sys.sxup_dict['name'] + '_xN',
                       'xN_global':   sys.sxup_dict['name'] + '_xN',
                       'XNF_unltd':   sys.sxup_dict['name'] + '_XNF'}
    ocp_dict = create_ocps()
    solver_dict = create_solvers()
    sol_dict = run_solvers()
    Q_opt, R_opt = create_opt_ocps()
    print_results()
    plot_results()
    animate_results()
