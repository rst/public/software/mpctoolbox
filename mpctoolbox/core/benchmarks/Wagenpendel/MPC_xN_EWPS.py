# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem


sol_dict = {'rhc_ltd':           None,
            'rhc_global':        None,
            'rhc_unltd':         None,
            'rhc_semiglobal':    None}
solver_kwargs = {'N':   25}
solver_kwargs_updates = {'rhc_ltd':           {'receding': 'ltd'},
                         'rhc_global':        {'receding': 'global'},
                         'rhc_unltd':         {'receding': 'unltd'},
                         'rhc_semiglobal':    {'receding': 'semiglobal'}}

ocp = OptimalControlProblem.load('pend_cart_pl_xN')
xu_dict = {'x_0':  'x_0',
           'x_1':  'x_2',
           'x_2':  'x_1',
           'x_3':  'x_3',
           'u':    'u_0'}

for key in sol_dict.keys():
    solver_kwargs.update(solver_kwargs_updates[key])
    solver = MPCSolver(ocp, **solver_kwargs)
    sol_dict.update({key: solver.solve()})

ys_to_plot = [[['x_0'], ['x_2'], ['u']], [['x_1'], ['x_3']]]
fig_size = [7.75/2.54, 10/2.54]
file_path = 'examples/plots'
filename = 'MPC_xN_EWPS_'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
colors = [[[tud_blue, tud_cyan], [tud_blue, tud_cyan], [tud_blue, tud_cyan]],
          [[tud_blue, tud_cyan], [tud_blue, tud_cyan]]]
drawstyles = [[['default', 'default'], ['default', 'default'],
              ['steps-post', 'steps-post']],
              [['default', 'default'], ['default', 'default']]]
x_min = -0.02
x_max = 1.02
y_ticks = [[None, None, None], [[0, 90, 180], None]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
          'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
          'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~\frac{m}{s}}$'
                    + y_str_2,
          'x_3':    y_str_1
                    + r'$\widetilde{x}_3\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2}

x_array = sol_dict['rhc_ltd']['tgrid_sim']
x_label = labels['t']
sxup_dict = ocp.sys.sxup_dict
y_arrays = []
y_labels = []
sols_to_plot = []

print('{:<15} {:<10} {:<10}'.format('', 'J_f', 'e_f'))
for (key, sol) in sol_dict.items():
    print('{:<15} {:<10.1f} {:<10.1e}'.format(key, sol['J_f'], sol['e_f']))
    if 'rhc_ltd' in key or 'rhc_global' in key:
        sols_to_plot.append(sol_dict[key])

for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            for sol in sols_to_plot:
                if key in xu_dict.keys() and 'x' in key:
                    y_arrays[i][k].append(sol['x_sim'][:,
                                                       int(xu_dict[key][-1])])
                    if sxup_dict[xu_dict[key]] in ['theta', 'omega', 'alpha']:
                        y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
                elif key in xu_dict.keys() and 'u' in key:
                    y_arrays[i][k].append(sol['u_sim'][:,
                                                       int(xu_dict[key][-1])])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, filename+str(i)+'.pdf', file_path=file_path,
                     show=show, usetex=usetex, x_min=x_min, x_max=x_max,
                     y_ticks=y_ticks[i])
