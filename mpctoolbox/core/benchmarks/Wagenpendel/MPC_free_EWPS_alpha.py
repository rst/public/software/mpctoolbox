# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem


sol_dict = {'rhc_ltd':           None,
            'rhc_unltd':         None}
solver_kwargs = {'N':   25}
solver_kwargs_updates = {'rhc_ltd':     {'receding': 'ltd'},
                         'rhc_unltd':   {'receding': 'unltd'}}

ocp = OptimalControlProblem.load('pend_cart_pl_free_opt')
T = 0.02
x_ref = np.zeros((1, ocp.sys.n_x))
u_ref = np.zeros((1, ocp.sys.n_u))
t_f = 6
tgrid = np.linspace(0, t_f, int(t_f/T)+1)
assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T
ocp.reconstruct(data={'t_grid': tgrid, 'x_ref': x_ref, 'u_ref': u_ref})
for key in sol_dict.keys():
    solver_kwargs.update(solver_kwargs_updates[key])
    solver = MPCSolver(ocp, **solver_kwargs)
    sol_dict.update({key: solver.solve()})

ys_to_plot = [[['alpha_a_sim', 'alpha_b_sim'], ['L_sim']]]
fig_size = [15/2.54, 10/2.54]
file_path = 'examples/plots'
filename = 'MPC_free_EWPS_alpha.pdf'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
tud_lgreen = '#69af22'
tud_dgreen = '#007d3f'
colors = [[[tud_blue, tud_cyan, tud_dgreen, tud_lgreen], [tud_blue, tud_cyan]]]
drawstyles = [[['default', 'default', 'default', 'default'],
               ['default', 'default']]]
y_scales = [['linear', 'log']]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
alpha_a_str = r'$\hat{\alpha}_\mathrm{ES},'
alpha_b_str = r'\hat{\alpha}_\mathrm{ZS}$'
labels = {'t':                          r'$t\mathrm{~in~s}\longrightarrow$',
          'alpha_a_simalpha_b_sim':     (y_str_1 + alpha_a_str + alpha_b_str
                                         + y_str_2),
          'L_sim':      y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2}

x_array = sol_dict['rhc_ltd']['tgrid_sim']
x_label = labels['t']
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            for (sol_key, sol) in sol_dict.items():
                y_arrays[i][k].append(sol[key])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, filename, file_path=file_path, show=show,
                     usetex=usetex, y_scales=y_scales[i])

print('{:<15} {:<10}'.format('', 'e_f'))
for (key, sol) in sol_dict.items():
    print('{:<15} {:<10.1e}'.format(key, sol['e_f']))
