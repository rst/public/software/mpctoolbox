# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.system import System
from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem


sigmas = np.array([10**0.9**e for e in np.arange(81)])
sigmas = np.append(np.logspace(6, 1, 40)[:-1], sigmas)
ocp_name_list = ['pend_cart_pl_XNF_sigma_'
                 + str(i).zfill(3) for i in range(len(sigmas))]
solver_kwargs = {'N':           25,
                 'receding':    'ltd'}

sys = System.load('pend_cart_pl')
T = 0.02
x_ref = np.zeros((1, sys.n_x))
u_ref = np.zeros((1, sys.n_u))
x_0 = np.array([0, 0, np.pi, 0])
t_f = 1
tgrid = np.linspace(0, t_f, int(t_f/T)+1)
assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T
lb = {'u': -10*9.81}
ub = {'u': 10*9.81}

delta_0 = 1
for n in range(len(sigmas)):
    try:
        ocp = OptimalControlProblem.load(ocp_name_list[n])
    except KeyError:
        ocp = OptimalControlProblem(sys, 'XNF', x_0, tgrid, x_ref, u_ref,
                                    kwargs={'lb': lb, 'ub': ub,
                                            'sigma': sigmas[n],
                                            'delta_0': delta_0})
        delta_0 = ocp.delta
        ocp.save(ocp_name_list[n])

deltas = []
sol_dict = {}
for n in range(len(sigmas)):
    ocp = OptimalControlProblem.load(ocp_name_list[n])
    deltas.append(ocp.delta)
    solver = MPCSolver(ocp, **solver_kwargs)
    sol_dict.update({str(n): solver.solve()})

ys_to_plot = [[['delta', 'e_f'], ['J_f']]]
fig_size = [15.5/2.54, 10/2.54]
file_path = 'examples/plots'
filename = 'MPC_XNF_EWPS.pdf'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
colors = [[[tud_blue, tud_cyan], [tud_blue]]]
linestyles = [[['', ''], ['']]]
markers = [[['.', '.'], ['.']]]
markersizes = [[[8, 3], [3]]]
y_mins = [None, None]
y_maxs = [None, None]
y_scales = ['log', 'linear']
x_scale = 'log'

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
labels = {'sigma':     r'$\sigma\longrightarrow$',
          'deltae_f':  y_str_1 + r'$\delta, e_\mathrm{f}$' + y_str_2,
          'J_f':       y_str_1 + r'$J_\mathrm{f}$' + y_str_2}

x_array = sigmas
x_label = labels['sigma']
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            y_arrays[i][k].append([])
            for (sol_key, sol) in sol_dict.items():
                if key == 'delta':
                    y_arrays[i][k][j].append(deltas[int(sol_key)])
                else:
                    y_arrays[i][k][j].append(sol[key])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         linestyles=linestyles[i][k], markers=markers[i][k],
                         markersizes=markersizes[i][k])
    pltr.create_plot(fig_size, filename, file_path=file_path, show=show,
                     usetex=usetex, y_mins=y_mins, y_maxs=y_maxs,
                     x_scale=x_scale, y_scales=y_scales, y_lpad=15)

Jfs = np.array([sol['J_f'] for sol in sol_dict.values()])
efs = np.array([sol['e_f'] for sol in sol_dict.values()])
Jfs_argmin = np.argmin(Jfs)
Jfs_argmax = np.argmax(Jfs)
efs_argmin = np.argmin(efs)
efs_argmax = np.argmax(efs)

fstr1 = '{:<10} {:<10} {:<10} {:<10} {:<10}'
fstr2 = '{:<10} {:<10.1f} {:<10.3e} {:<10.4e} {:<10.3e}'
print(fstr1.format('', 'J_f', 'e_f', 'sigma', 'delta'))
print(fstr2.format('min sigma', Jfs[-1], efs[-1], sigmas[-1], deltas[-1]))
print(fstr2.format('max sigma', Jfs[0], efs[0], sigmas[0], deltas[0]))
print(fstr2.format('min e_f', Jfs[efs_argmin], efs[efs_argmin],
                   sigmas[efs_argmin], deltas[efs_argmin]))
print(fstr2.format('max e_f', Jfs[efs_argmax], efs[efs_argmax],
                   sigmas[efs_argmax], deltas[efs_argmax]))
print(fstr2.format('min J_f', Jfs[Jfs_argmin], efs[Jfs_argmin],
                   sigmas[Jfs_argmin], deltas[Jfs_argmin]))
print(fstr2.format('max J_f', Jfs[Jfs_argmax], efs[Jfs_argmax],
                   sigmas[Jfs_argmax], deltas[Jfs_argmax]))
