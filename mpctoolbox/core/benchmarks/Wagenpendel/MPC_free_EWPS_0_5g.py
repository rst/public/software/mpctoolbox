# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ani import create_animation
from mpctools.ocp import OptimalControlProblem


solver_kwargs = {'N':           50,
                 'receding':    'unltd'}
xu_dict = {'x_0':  'x_0',
           'x_1':  'x_2',
           'x_2':  'x_1',
           'x_3':  'x_3',
           'u':    'u_0'}

ocp = OptimalControlProblem.load('pend_cart_pl_free_opt')
T = 0.02
x_ref = np.zeros((1, ocp.sys.n_x))
u_ref = np.zeros((1, ocp.sys.n_u))
x_0 = np.array([0, 0, np.pi, 0])
x_0 = np.reshape(x_0, (ocp.sys.n_x, 1))
t_f = 7
tgrid = np.linspace(0, t_f, int(t_f/T)+1)
assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T
ocp.reconstruct(data={'lb': {'u': -0.5*9.81}, 'ub': {'u': 0.5*9.81},
                      't_grid': tgrid, 'x_ref': x_ref, 'u_ref': u_ref})
solver = MPCSolver(ocp, **solver_kwargs)
sol = solver.solve()

ys_to_plot = [[['x_0'], ['x_2'], ['x_1'], ['x_3']],
              [['u'], ['L_sim'], ['alpha_a_sim', 'alpha_b_sim']]]
fig_size = [7.75/2.54, 15/2.54]
file_path = 'examples/plots'
filename = 'MPC_free_EWPS_0_5g_'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
colors = [[[tud_blue], [tud_blue], [tud_blue], [tud_blue]],
          [[tud_blue], [tud_blue], [tud_blue, tud_cyan]]]
drawstyles = [[['default'], ['default'], ['default'], ['default']],
              [['steps-post'], ['default'], ['default', 'default']]]
y_scales = [['linear', 'linear', 'linear', 'linear'],
            ['linear', 'log', 'linear']]
x_min = -0.14
x_max = 7.14
x_ticks = [0, 1, 2, 3, 4, 5, 6, 7]
y_ticks = [[None, [-1, 0, 1, 2], [0, 90, 180, 270], None],
           [None, [1e-5, 1e-3, 1e-1, 10, 1e3],
            [-35, -30, -25, -20, -15, -10, -5, 1]]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
alpha_a_str = r'$\hat{\alpha}_\mathrm{ES},'
alpha_b_str = r'\hat{\alpha}_\mathrm{ZS}$'
labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
          'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
          'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~\frac{m}{s}}$'
                    + y_str_2,
          'x_3':    y_str_1
                    + r'$\widetilde{x}_3\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2,
          'L_sim':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2,
          'alpha_a_simalpha_b_sim':     y_str_1 + alpha_a_str + alpha_b_str
                                        + y_str_2}

print('{:<20} {:<10} {:<10}'.format('', 'J_f', 'e_f'))
print('{:<20} {:<10.1f} {:<10.1e}'.format('rhc_free_unltd_N_50',
                                          sol['J_f'], sol['e_f']))
ani = create_animation('pend_cart_pl', sol, 'Aufschwingen des EWPS',
                       file_path='examples/anis', filename='MPC_free_EWPS_0,5g')

x_array = sol['tgrid_sim']
x_label = labels['t']
sxup_dict = ocp.sys.sxup_dict
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            if key in xu_dict.keys() and 'x' in key:
                y_arrays[i][k].append(sol['x_sim'][:, int(xu_dict[key][-1])])
                if sxup_dict[xu_dict[key]] in ['theta', 'omega', 'alpha']:
                    y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
            elif key in xu_dict.keys() and 'u' in key:
                y_arrays[i][k].append(sol['u_sim'][:, int(xu_dict[key][-1])])
            else:
                y_arrays[i][k].append(sol[key])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, filename+str(i)+'.pdf', file_path=file_path,
                     show=show, usetex=usetex, x_min=x_min, x_max=x_max,
                     y_scales=y_scales[i], x_ticks=x_ticks, y_ticks=y_ticks[i])
