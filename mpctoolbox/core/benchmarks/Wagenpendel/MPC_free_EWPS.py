# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.mpc import MPCSolver
from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem


sol_dict = {'rhc_free_ltd_N_107':   None,
            'rhc_free_ltd_N_25':    None,
            'rhc_xN_ltd_N_25':      None}
solver_kwargs = {'N':           107,
                 'receding':    'ltd'}
solver_kwargs_updates = {'rhc_free_ltd_N_107':   {},
                         'rhc_free_ltd_N_25':    {'N': 25},
                         'rhc_xN_ltd_N_25':      {}}
ocp_name_dict = {'rhc_free_ltd_N_107':   'pend_cart_pl_free',
                 'rhc_free_ltd_N_25':    'pend_cart_pl_free',
                 'rhc_xN_ltd_N_25':      'pend_cart_pl_xN'}
T = 0.02
t_f_1 = 1
t_f_3 = 3
tgrid_1 = np.linspace(0, t_f_1, int(t_f_1/T)+1)
assert tgrid_1[-1] == t_f_1 and tgrid_1[1] - tgrid_1[0] == T
tgrid_3 = np.linspace(0, t_f_3, int(t_f_3/T)+1)
assert tgrid_3[-1] == t_f_3 and tgrid_3[1] - tgrid_3[0] == T
x_ref = np.zeros((1, 4))
u_ref = np.zeros((1, 1))
ocp_params_dict = {'pend_cart_pl_xN':   {'t_grid': tgrid_1,
                                         'x_ref': x_ref, 'u_ref': u_ref},
                   'pend_cart_pl_free': {'t_grid': tgrid_3,
                                         'x_ref': x_ref, 'u_ref': u_ref}}
xu_dict = {'x_0':  'x_0',
           'x_1':  'x_2',
           'x_2':  'x_1',
           'x_3':  'x_3',
           'u':    'u_0'}

for key in sol_dict.keys():
    ocp = OptimalControlProblem.load(ocp_name_dict[key])
    ocp.reconstruct(data=ocp_params_dict[ocp_name_dict[key]])
    solver_kwargs.update(solver_kwargs_updates[key])
    solver = MPCSolver(ocp, **solver_kwargs)
    sol_dict.update({key: solver.solve()})

ys_to_plot = [[['x_0'], ['x_2'], ['u']], [['x_1'], ['x_3'], ['L_sim']]]
fig_size = [7.75/2.54, 10/2.54]
file_path = 'examples/plots'
filename = 'MPC_free_EWPS_'
show = False
usetex = True

tud_blue = '#00305d'
tud_cyan = '#009de0'
tud_alert = '#ee7f00'
colors = [[[tud_blue, tud_alert, tud_cyan], [tud_blue, tud_alert, tud_cyan],
           [tud_blue, tud_alert, tud_cyan]],
          [[tud_blue, tud_alert, tud_cyan], [tud_blue, tud_alert, tud_cyan],
           [tud_blue, tud_alert, tud_cyan]]]
drawstyles = [[['default', 'default', 'default'],
               ['default', 'default', 'default'],
               ['steps-post', 'steps-post', 'steps-post']],
              [['default', 'default', 'default'],
               ['default', 'default', 'default'],
               ['default', 'default', 'default']]]
y_scales = [['linear', 'linear', 'linear'], ['linear', 'linear', 'log']]
x_min = -0.06
x_max = 3.06
y_mins = [[None, None, None], [None, None, 1e-3]]
y_maxs = [[None, None, None], [None, None, None]]
x_ticks = [0, 1, 2, 3]
y_ticks = [[None, None, None], [[0, 90, 180], None, [1e-3, 1, 1e2]]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
labels = {'t':      r'$t\mathrm{~in~s}\longrightarrow$',
          'x_0':    y_str_1 + r'$\widetilde{x}_0\mathrm{~in~m}$' + y_str_2,
          'x_1':    y_str_1 + r'$\widetilde{x}_1\mathrm{~in~{}^\circ}$'
                    + y_str_2,
          'x_2':    y_str_1 + r'$\widetilde{x}_2\mathrm{~in~\frac{m}{s}}$'
                    + y_str_2,
          'x_3':    y_str_1
                    + r'$\widetilde{x}_3\mathrm{~in~\frac{{}^\circ}{s}}$'
                    + y_str_2,
          'u':      y_str_1 + r'$u\mathrm{~in~\frac{m}{s^2}}$' + y_str_2,
          'L_sim':  y_str_1 + r'$l(\boldsymbol{x}, u)$' + y_str_2}

print('{:<20} {:<10} {:<10}'.format('', 'J_f', 'e_f'))
for (key, sol) in sol_dict.items():
    print('{:<20} {:<10.1f} {:<10.1e}'.format(key, sol['J_f'], sol['e_f']))

x_array = sol_dict['rhc_free_ltd_N_107']['tgrid_sim']
x_label = labels['t']
sxup_dict = ocp.sys.sxup_dict
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        y_labels.append('')
        for j in range(len(ys_to_plot[i][k])):
            key = ys_to_plot[i][k][j]
            for sol in sol_dict.values():
                if key in xu_dict.keys() and 'x' in key:
                    y_arrays[i][k].append(sol['x_sim'][:,
                                                       int(xu_dict[key][-1])])
                    if sxup_dict[xu_dict[key]] in ['theta', 'omega', 'alpha']:
                        y_arrays[i][k][-1] = y_arrays[i][k][-1]/np.pi*180
                elif key in xu_dict.keys() and 'u' in key:
                    y_arrays[i][k].append(sol['u_sim'][:,
                                                       int(xu_dict[key][-1])])
                else:
                    y_arrays[i][k].append(sol[key])
            y_labels[-1] += key
        y_labels[-1] = labels[y_labels[-1]]
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         drawstyles=drawstyles[i][k])
    pltr.create_plot(fig_size, filename+str(i)+'.pdf', file_path=file_path,
                     show=show, usetex=usetex, x_min=x_min, x_max=x_max,
                     y_scales=y_scales[i], y_mins=y_mins[i], y_maxs=y_maxs[i],
                     x_ticks=x_ticks, y_ticks=y_ticks[i])
