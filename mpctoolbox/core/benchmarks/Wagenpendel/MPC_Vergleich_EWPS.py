# -*- coding: utf-8 -*-
""" Author: Patrick Rüdiger """
import numpy as np

from mpctools.plotter import Plotter
from mpctools.ocp import OptimalControlProblem
from mpctools.mpc import MPCSolver, optimize_QR


sigmas = np.array([10, 3e3, 1e6])
ocp_name_list = []
solver_kwargs = {'N':           25,
                 'receding':    'unltd'}

ocp = OptimalControlProblem.load('pend_cart_pl_xN')
Q_opt, R_opt, L_opt, solver, sol = optimize_QR(ocp, {'N': 25, 'receding': 'ltd'})

for mode in ['xN', 'free']:
    ocp.reconstruct(data={'mode': mode, 'Q': Q_opt, 'R': R_opt})
    ocp_name_list.append('pend_cart_pl_'+mode+'_opt')
    ocp.save(ocp_name_list[-1])
for n in range(len(sigmas)):
    ocp.reconstruct(data={'mode': 'XNF', 'Q': Q_opt, 'R': R_opt,
                          'sigma': sigmas[n], 'delta_0': 1})
    ocp_name_list.append('pend_cart_pl_XNF_'+str(n)+'_opt')
    ocp.save(ocp_name_list[-1])

sol_dict = {}
N_dict = {}
for ocp_name in ocp_name_list:
    ocp = OptimalControlProblem.load(ocp_name)
    for k in range(2, 11):
        lb = {'u': -k*9.81}
        ub = {'u': k*9.81}
        if 'XNF' in ocp_name:
            ocp.reconstruct(data={'lb': lb, 'ub': ub, 'sigma': ocp.sigma,
                                  'delta': ocp.delta})
        else:
            ocp.reconstruct(data={'lb': lb, 'ub': ub})
        for N in range(10, 101):
            solver_kwargs.update({'N': N})
            solver = MPCSolver(ocp, **solver_kwargs)
            sol = solver.solve()
            if sol['sol_status'] not in ['Solve_Succeeded',
                                         'Solved_To_Acceptable_Level']:
                continue
            if 'free' in ocp_name:
                if min(sol['alpha_b_sim']) <= 0:
                    continue
            break
        sol_dict.update({ocp_name+'_'+str(k): sol})
        N_dict.update({ocp_name+'_'+str(k): N})
        print(ocp_name+'_'+str(k)+': '+str(N))

fstr1 = '{:<30} {:<10} {:<10} {:<10} {:<10} {:<10}'
fstr2 = '{:<30} {:<10.3e} {:<10.3f} {:<10.3f} {:<10.3f} {:<10.3f}'
fstr3 = '{:<30} {:<10.3e} {:<10.0f} {:<10.3f} {:<10.3f} {:<10.3f}'
print(fstr1.format('', 'e_f', 'J_f', 'T_f/t_f', 'T_0/T_f', 'N_min/n_f'))
for ocp_name in ['xN', 'free', 'XNF_0', 'XNF_1', 'XNF_2']:
    e_fs = []
    J_fs = []
    T_fs = []
    T_0s = []
    Ns = []
    for (key, val) in sol_dict.items():
        if ocp_name in key:
            print(fstr2.format(key, val['e_f'], val['J_f'],
                               sum(val['solve_times']) / ocp.t_grid[-1],
                               val['solve_times'][0] / sum(val['solve_times']),
                               np.array(N_dict[key]) / int(ocp.t_grid[-1] / ocp.T)))
            e_fs.append(val['e_f'])
            J_fs.append(val['J_f'])
            T_fs.append(sum(val['solve_times']))
            T_0s.append(val['solve_times'][0])
            Ns.append(N_dict[key])
    print(fstr3.format(ocp_name +'_mean', np.mean(e_fs), np.mean(J_fs),
                       np.mean(T_fs) / ocp.t_grid[-1],
                       np.mean(T_0s) / np.mean(T_fs),
                       np.mean(Ns) / int(ocp.t_grid[-1] / ocp.T)))

ys_to_plot = [[['e_f'], ['J_f'], ['T_f'], ['N']]]
plt_types = ['bar', 'bar', 'bar', 'bar']
fig_size = [15/2.54, 15/2.54]
bar_width = 0.15
file_path = 'examples/plots'
filename = 'MPC_Vergleich_EWPS.pdf'
show = False
usetex = True

tud_alert = '#ee7f00'
tud_blue = '#00305d'
tud_cyan = '#009de0'
tud_dgreen = '#007d3f'
tud_lgreen = '#69af22'
colors = [[[tud_alert, tud_blue, tud_cyan, tud_dgreen, tud_lgreen],
           [tud_alert, tud_blue, tud_cyan, tud_dgreen, tud_lgreen],
           [tud_alert, tud_blue, tud_cyan, tud_dgreen, tud_lgreen],
           [tud_alert, tud_blue, tud_cyan, tud_dgreen, tud_lgreen]]]
y_scales = [['log', 'linear', 'linear', 'linear']]
y_mins = [1e-5, None, None, None]
y_maxs = [1, None, None, None]
y_ticks = [[1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1], None,
           [1, 5, 10, 15], [0, 0.25, 0.5, 0.75, 1]]

y_str_1 = r'\begin{center} $\big\uparrow$\\[4pt]'
y_str_2 = r'\end{center}'
labels = {'u_b':    r'$u_b\longrightarrow$',
          'e_f':    y_str_1 + r'$e_\mathrm{f}$' + y_str_2,
          'J_f':    y_str_1 + r'$J_\mathrm{f}$' + y_str_2,
          'T_f':    y_str_1 + r'$\frac{T_\mathrm{f}}{t_\mathrm{f}}$' + y_str_2,
          'N':      y_str_1 + r'$\frac{N_\mathrm{min}}{n_\mathrm{f}}$'+y_str_2}

x_label = labels['u_b']
x_array = np.arange(2, 11)
x_tick_labels = [r'$' + str(e) + r'g$' for e in x_array]
y_arrays = []
y_labels = []
for i in range(len(ys_to_plot)):
    pltr = Plotter(x_label, x_array)
    y_arrays.append([])
    for k in range(len(ys_to_plot[i])):
        y_arrays[i].append([])
        key = ys_to_plot[i][k][0]
        y_labels.append(labels[key])
        for j in range(len(ocp_name_list)):
            y_arrays[i][k].append([])
            for ub in range(2, 11):
                if key == 'N':
                    N = N_dict[ocp_name_list[j] + '_'+str(ub)]
                    y_arrays[i][k][j].append(N / int(ocp.t_grid[-1] / ocp.T))
                elif key == 'T_f':
                    T_f = sum(sol_dict[ocp_name_list[j] + '_'
                                       + str(ub)]['solve_times'])
                    y_arrays[i][k][j].append(T_f / ocp.t_grid[-1])
                else:
                    y_arrays[i][k][j].append(sol_dict[ocp_name_list[j]
                                                      + '_'+str(ub)][key])
        pltr.add_subplot(y_arrays[i][k], y_labels[-1], colors=colors[i][k],
                         plt_types=plt_types[i], bar_widths=bar_width)
    pltr.create_plot(fig_size, filename, file_path=file_path, show=show,
                     usetex=usetex, y_lpad=10, y_scales=y_scales[i],
                     y_mins=y_mins, y_maxs=y_maxs, y_ticks=y_ticks,
                     x_ticks=x_array, x_tick_labels=x_tick_labels)
