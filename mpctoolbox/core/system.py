import logging
import os
from joblib import dump as joblib_dump
from joblib import load as joblib_load
from joblib import hash as joblib_hash

import numpy as np
import casadi as ca

logger = logging.getLogger(__name__)


class System:
    """
    Class for user-defined continuous state space systems build on Casadi
    expressions.

    This class is the foundation to model all dynamic behaviour.

    The right hand-side :math:`\\dot x = f(x, u, p)` may either be given as
    ``SX`` or ``MX`` expression and will be converted to ``SX``.

    Args:
        x_dot_p (cs.Matrix): State-space representation (rhs) of the system
            using the symbolic variables and including parameter symbols.
        sym_dict (dict): Map between string identifiers and symbols (SX or MX)
            in the rhs. *Must* contain keys "x" (state) and "u" (input) and
            *may* contain a parameter vector "p".

    Keyword Args:
        p_val_dict (dict): Mapping of symbolic parameters and their values.
        sxup_dict (dict): Descriptions for system name, state and input members.
            (To describe member two of the state vector use "x_2".)
        norm_dict (dict): Dictionary holding the normalization data. Holds tuple
            of vectors (mean, var) the components of each variable.

    Note:
        Providing a time variant system is currently not supported.
    """
    mandatory_symbols = "x", "u"

    def __init__(self, x_dot_p, sym_dict,
                 p_val_dict=None, sxup_dict=None):
        # self._collect_data(p_val_dict, sxup_dict, sym_dict, x_dot_p)

        # get symbol information
        for val in self.mandatory_symbols:
            if val not in sym_dict:
                msg = "Missing mandatory entry '{}' in 'sym_dict'".format(val)
                logger.error(msg)
                raise KeyError(msg)

        x = sym_dict["x"]
        u = sym_dict["u"]
        self.n_x = x.size1()
        self.n_u = u.size1()

        # substitute parameters
        if p_val_dict is None:
            x_dot = x_dot_p
        else:
            p_syms = ca.vertcat(*p_val_dict.keys())
            p_vals = ca.vertcat(*p_val_dict.values())
            x_dot = ca.substitute(x_dot_p, p_syms, p_vals)

        x_dot_fun = ca.Function('x_dot_fun', [x, u], [x_dot],
                                ['x', 'u'], ['x_dot'])

        # convert to SX
        self.x_dot_fun = x_dot_fun.expand()
        self.x = ca.SX.sym('x', self.n_x)
        self.u = ca.SX.sym('u', self.n_u)
        self.x_dot = self.x_dot_fun(self.x, self.u)

        # copy descriptions
        self.sxup_dict = {} if sxup_dict is None else sxup_dict

        self._build_discrete_sys()

    def _build_discrete_sys(self):
        # build euler-forward discretised approximation
        self.T = ca.SX.sym('T')
        self.x_dot_d = self.x + self.x_dot * self.T
        self.x_dot_d_fun = ca.Function('x_dot_d_fun', [self.x, self.u, self.T],
                                       [self.x_dot_d],
                                       ['x', 'u', 'T'], ['x_dot_d'])
        self.A_d_fun = ca.Function('A_d_fun', [self.x, self.u, self.T],
                                   [ca.jacobian(self.x_dot_d, self.x)],
                                   ['x', 'u', 'T'], ['A_d'])
        self.B_d_fun = ca.Function('B_d_fun', [self.x, self.u, self.T],
                                   [ca.jacobian(self.x_dot_d, self.u)],
                                   ['x', 'u', 'T'], ['B_d'])

    def get_state_map(self, other):
        """
        Construct a mapping that projects the state of this system to the state
        of another system.

        Args:
            other (System): System whose state is to be computed.

        Returns: Callable, state mapping.

        """
        def _identity_map(_x):
            return _x

        if other == self:
            return _identity_map

        return NotImplemented

    def _collect_data(self, p_val_dict, sxup_dict, sym_dict, x_dot_p):
        # collect invariant data
        self.data = {}
        self.x_dot_p_str = self.data.setdefault('x_dot_p_str', str(x_dot_p))
        shape_dict = {k: v.shape for (k, v) in sym_dict.items()}
        self.shape_dict = self.data.setdefault('shape_dict', shape_dict)
        self.p_val_dict = self.data.setdefault('p_val_dict', p_val_dict)
        self.sxup_dict = self.data.setdefault('sxup_dict', sxup_dict)
        # take hash
        self.data_hash = joblib_hash(self.data)
        self.data['data_hash'] = self.data_hash

    def save(self, filename):
        """
        Saves system to "systems/sys_cache.dat", assigns the provided filename

        Arguments:
            filename (str):
                name of the system to save
        """
        raise NotImplementedError
        try:
            os.mkdir('benchmarks/')
        except FileExistsError:
            pass

        try:
            cache = joblib_load('benchmarks/sys_cache.dat')
        except FileNotFoundError:
            cache = {}
            pass

        if self.data_hash in cache.keys() and filename in cache.keys():
            if self.data_hash == cache[filename]['data_hash']:
                print('System "' + filename + '" has already been cached.')
                return
        if self.data_hash in cache.keys():
            cache.pop(cache[self.data_hash])
            print('System has already been cached. New filename "'
                  + filename + '" assigned.')
        if filename in cache.keys():
            cache.pop(cache[filename]['data_hash'])
            print('Filename "' + filename + '" has already been assigned. '
                  + 'Associated system has been replaced.')

        cache[filename] = self.data
        cache[self.data_hash] = filename

        joblib_dump(cache, 'benchmarks/sys_cache.dat')

        fn_file = open('benchmarks/sys_filenames.txt', 'w')
        for key in cache.keys():
            if key in cache.values():
                fn_file.write(key+os.linesep)
        fn_file.close()

    def set_p_vals(self, p_val_dict):
        """
        Allows changing the parameter values without manually recreating the
        system.

        Arguments:
            p_val_dict (dict):
                Assigns parameter values to all additional keys in sym_dict.
        """
        raise NotImplementedError
        self.data.pop('data_hash')
        self.__init__(self.x_dot_p, self.sym_dict, p_val_dict=p_val_dict,
                      sxup_dict=self.sxup_dict)

    @classmethod
    def load(cls, filename):
        """
        Loads the system specified via filename from "systems/sys_cache.dat"

        Arguments:
            filename (str):
                name of the system to load

        Returns:
            sys (System):
                loaded system

        Exceptions:
            AssertionError:
                data hash of loaded system is different from saved data hash
        """
        raise NotImplementedError

        cache = joblib_load('benchmarks/sys_cache.dat')
        data = cache[filename]

        shape_dict = data['shape_dict']
        sym_dict = {k: ca.MX.sym(k, v) for (k, v) in shape_dict.items()}
        loc_dict = sym_dict.copy()
        loc_dict['sq'] = lambda x: x**2
        x_dot_p = eval(data['x_dot_p_str'], ca.__dict__, loc_dict)
        sys = cls(x_dot_p, sym_dict, data['p_val_dict'], data['sxup_dict'])
        assert sys.data_hash == data['data_hash']
        return sys
