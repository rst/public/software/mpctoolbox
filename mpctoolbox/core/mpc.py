"""
This module contains the main functionality of the toolbox, the model predictive
controller.
"""
import os
from enum import IntEnum, auto
import logging
from joblib import dump as joblib_dump
from joblib import load as joblib_load
from joblib import hash as joblib_hash

import numpy as np
import casadi as ca
import mpctools  # as mpc

import matplotlib.pyplot as plt

from .ocp import OCPMode


logger = logging.getLogger(__name__)


class MPCMode(IntEnum):
    """
    Enum holding options for the handling of the optimisation horizon
    """
    UNLIMITED = auto()
    """
    Classic MPC: The optimization horizon recedes for every step and will not
    be limited by the actual number of time steps left in the simulation.
    """

    LIMITED = auto()
    """
    The optimization horizon recedes until its end reaches the simulation domain
    and will be decremented by 1 for every time step until N = 1.
    """

    HYBRID = auto()
    """
    The optimization horizon recedes until its end reaches the simulation domain
    after which the control is deactivated and the last optimal input sequence 
    is applied for the remaining steps.
    """

    OPEN_LOOP = auto()
    """
    The optimization horizon includes the complete the simulation domain and the
    computed optimal input sequence is applied for all steps. This is a pure
    optimisation based feedforward.
    """

# def T(x, means, variances):
#     z = np.empty(x.shape)
#     for i in range(x.size):
#         z[i] = (x[i]-means[i])/variances[i]
#     return z
#
#
# def S(z, means, variances):
#     x = np.empty(z.shape)
#     for i in range(z.size):
#         x[i] = (z[i]*variances[i])+means[i]
#     return x


class NlpCallback(ca.Callback):
    """
    Callback to monitor the current progress of the CasADi nlp solver.
    """
    def __init__(self, name, n_x, n_g, n_p, verbosity, ocp_n_x, x_f, opts=None):
        ca.Callback.__init__(self)

        self.nx = n_x
        self.ng = n_g
        self.np = n_p
        self.verbosity = verbosity
        self.ocp_n_x = ocp_n_x
        self.x_f = x_f

        if opts is None:
            opts = {}

        self.x_opts = []
        self.e_0s = []
        self.e_Ns = []
        self.g_norms = []
        self.J_opt = 0

        self.construct(name, opts)

        # volatile data
        self.k = 0

    def reset(self):
        self.k = 0

    def get_n_in(self): return ca.nlpsol_n_out()
    def get_n_out(self): return 1
    def get_name_in(self, i): return ca.nlpsol_out(i)
    def get_name_out(self, i): return 'ret'

    def get_sparsity_in(self, i):
        n = ca.nlpsol_out(i)
        if n == 'f':
            return ca.Sparsity.scalar()
        elif n in ('x', 'lam_x'):
            return ca.Sparsity.dense(self.nx)
        elif n in ('g', 'lam_g'):
            return ca.Sparsity.dense(self.ng)
        elif n in ('p', 'lam_p'):
            return ca.Sparsity.dense(self.np)
        else:
            return ca.Sparsity(0, 0)

    def eval(self, arg):
        sol = {}
        for (i, s) in enumerate(ca.nlpsol_out()):
            sol[s] = arg[i]

        x_sol = sol['x'].full().squeeze()
        self.x_opts.append(x_sol)

        e_0 = np.linalg.norm(x_sol[:self.ocp_n_x] - self.x_f)
        self.e_0s.append(e_0)

        e_N = np.linalg.norm(x_sol[-self.ocp_n_x:] - self.x_f)
        self.e_Ns.append(e_N)

        self.g_norms.append(np.linalg.norm(sol['g'].full().flatten()))
        self.J_opt = sol['f'].full()[0][0]

        logger.debug("e_0: {:.3e}".format(self.e_0s[-1]))
        logger.debug("e_N: {:.3e}".format(self.e_Ns[-1]))
        logger.debug("g_norm: {:.3e}".format(self.g_norms[-1]))

        self.k += 1

        return [0]


def optimize_qr(ocp, solver_kwargs, lbq=0, ubq=np.inf, r_0=0.1,
                lbr=0, ubr=np.inf):
    """
    Make quadratic stage cost as monotonically decreasing as possible for
    a specific sequence of states/inputs via optimization using IPOPT.

    Solves provided ocp with MPCSolver using the provided solver_kwargs. The
    resulting sequence of states sol['x_sim'] and inputs sol['u_sim'] is used
    to optimize the diagonal elements of the matrices Q and R of the quadratic
    stage cost L to make its sequence for those states/inputs as monotonically
    decreasing as possible.

    Args:
        ocp (OptimalControlProblem): Optimal control problem to solve.
        solver_kwargs (dict): Options for MPCSolver.

    Keyword Args:
        lbq (float or numpy.ndarray): Lower bounds for diagonal elements of Q.
        ubq (float or numpy.ndarray): Upper bounds for diagonal elements of Q.
        r_0 (float): First diagonal element of R.
        lbr (float or numpy.ndarray): Lower bounds for diagonal elements of R.
        ubr (float or numpy.ndarray): Upper bounds for diagonal elements of R.

    Returns:
        Tuple: A tuple containing:
            - Q_opt (casadi.DM): Optimal Q.
            - R_opt (casadi.DM): Optimal R.
            - L_opt (numpy.ndarray): Optimal stage cost.
            - solver (MPCSolver): MPCSolver object.
            - sol (dict): Solution determined with MPCSolver.

    Raises:
        AssertionError:
            optimization not successful or Q_opt not positive definite
            (try adjusting bounds)
    """
    solver = MPController(ocp, **solver_kwargs)
    sol = solver.solve(force_solve=False)

    q = ca.SX.sym('Q', ocp.sys.n_x)
    Q = ca.diag(q)
    r = ca.SX.sym('R', ocp.sys.n_u)
    R = ca.diag(r)

    e_x = ocp.sys.x - ocp.x_sp
    e_u = ocp.sys.u - ocp.u_sp
    L = e_x.T @ Q @ e_x + e_u.T @ R @ e_u
    L_fun = ca.Function('L_fun',
                        [ocp.sys.x, ocp.sys.u, ocp.x_sp, ocp.u_sp, q, r], [L])

    L_sim = []
    for n in range(len(sol['u_sim'][:, 0])):
        L_sim.append(L_fun(sol['x_sim'][n], sol['u_sim'][n],
                           ocp.x_ref[n], ocp.u_ref[n], q, r))

    J = 0
    for n in range(len(L_sim)-1):
        J += (((L_sim[n+1]-L_sim[n])/L_sim[n])
              * (ca.sign(L_sim[n+1]-L_sim[n])+1))

    if np.all(lbq == (lbq*np.ones(ocp.sys.n_x))[0]):
        lbq = lbq*np.ones(ocp.sys.n_x)
    if np.all(ubq == (ubq*np.ones(ocp.sys.n_x))[0]):
        ubq = ubq*np.ones(ocp.sys.n_x)
    if np.all(lbr == (lbr*np.ones(ocp.sys.n_u))[0]):
        lbr = lbr*np.ones(ocp.sys.n_u)
    if np.all(ubr == (ubr*np.ones(ocp.sys.n_u))[0]):
        ubr = ubr*np.ones(ocp.sys.n_u)
    lbr[0], ubr[0] = [r_0, r_0]
    lbx = np.append(lbq, lbr)
    ubx = np.append(ubq, ubr)

    nlp = {'f': J, 'x': ca.vertcat(q, r)}
    opts = {'ipopt': solver.solver_kwargs}

    nlp_solver = ca.nlpsol('solver', 'ipopt', nlp, opts)

    nlp_sol = nlp_solver(x0=[], lbx=lbx, ubx=ubx, lbg=[], ubg=[])
    assert nlp_solver.stats()['success']
    assert np.all(np.squeeze(nlp_sol['x'][:ocp.sys.n_x].full()) > 0)

    q_opt = np.squeeze(nlp_sol['x'][:ocp.sys.n_x].full())
    r_opt = np.squeeze(nlp_sol['x'][ocp.sys.n_x:].full())
    Q_opt = ca.diag(q_opt)
    R_opt = ca.diag(r_opt)
    L_opt = np.zeros(len(sol['u_sim'][:, 0]))
    for n in range(len(sol['u_sim'][:, 0])):
        L_opt[n] = L_fun(sol['x_sim'][n], sol['u_sim'][n],
                         ocp.x_ref[n], ocp.u_ref[n], q_opt, r_opt).full()[0][0]

    if solver.verbosity > 1:
        plt.figure('L_sim')
        legend = ['L']
        plt.plot(sol['tgrid_sim'][:-1], L_opt, '-')
        legend.append('L opt')
        plt.legend(legend)
        plt.xlabel('t')
        plt.show()

    return [Q_opt, R_opt, L_opt, solver, sol]


class SolverError(BaseException):
    pass


class MPController:
    """
    A model predictive controller implemented with mpctools-casadi.

    In detail, after the system is discretised using collocation, a nonlinear
    optimisation problem is formulated stored.
    
    Everytime `get_control` is called, the problem will be solved starting from
    the current state using IPOPT.
    
    
    For every step, the output is applied to a simulation system which ios solved
    using CVODES.

    Args:
        ocp (OptimalControlProblem): Optimal control problem for which a
            control is to be designed.
        mode (MPCMode): Operation mode of the controller.

    Keyword Args:
        hor_len (int): Length of optimization horizon, must be greater 1.
        u_guess (numpy.ndarray): Initial guess for the input trajectory,
            defaults to ``ocp.u_ref[:N]``.
        x_guess (numpy.ndarray): Initial guess for the state trajectory,
            defaults to ``ocp.x_ref[:N]``.
        n_coll_pts (int): Number of collocation points to use for discretisation
            of the continuous system dynamics. Casadi MPCTools implements an
            orthogonal collocation method using the zeros of a shifted Legendre
            polynomial as interpolation points. Defaults to 4.
        solver_kwargs (dict): Options for the solver, see below for details.
            TODO add content
        show_step (bool): If True, computed trajectories are shown after each
            optimisation step.
        allow_relaxation (bool): If True, a solution for the ocp is tried with
            relaxed constraints tolerances in case of local infeasibility.
        continue_on_failure (bool): If True, the controller continues even if no
            solution could be found for a single step.
        verbosity (int) in range(0, 12):
            print level for MPCTools, IPOPT, and this class; basic plots
            will be displayed for verbosity > 2

    Warnings:
        steps_receding > 1 is not implemented yet

    Raises:
        AssertionError: If invalid keyword arguments are provided.
        NotImplementedError: If ``steps_receding != 1``.
    """
    def __init__(self, ocp, mode,
                 hor_len=None,
                 x_guess=None,
                 u_guess=None,
                 n_coll_pts=4,
                 solver_kwargs=None,
                 verbosity=0,
                 show_step=False,
                 allow_relaxation=False,
                 continue_on_failure=False,
                 ):
        # the control problem
        self.ocp = ocp

        # discretisation options
        self.n_coll_pts = n_coll_pts

        # solver options
        if solver_kwargs is None:
            self.solver_kwargs = dict(max_iter=1e4, time_limit=3600)
        else:
            self.solver_kwargs = solver_kwargs
        self.time_limit = self.solver_kwargs.pop("time_limit", 3600)

        # operation mode
        assert mode in MPCMode
        self.mode = mode

        # optimisation horizon
        self.n_f = len(self.ocp.t_grid) - 1
        if self.mode == MPCMode.OPEN_LOOP:
            if hor_len is not None:
                logger.warning("Global planning requested, ignoring given "
                               "horizon length.")
            self.N = self.n_f
        else:
            if hor_len is None:
                e = ValueError("Horizon length must be provided.")
                logger.exception(e)
                raise e
            if hor_len > self.n_f:
                logger.warning("Provided horizon length is longer than whole "
                               "simulation domain, limiting to domain.")
            self.N = min(hor_len, self.n_f)
        self.N_0 = self.N

        # guesses
        self.guess = {}
        if x_guess is not None:
            assert x_guess.shape == (self.ocp.sys.n_x, self.N + 1)
            self.guess["x"] = x_guess.T
        else:
            self.guess["x"] = self.ocp.x_ref[:, min(self.n_f - 1, self.N + 1)].T
        if u_guess is not None:
            assert u_guess.shape == (self.ocp.sys.n_u, self.N)
            self.guess["u"] = u_guess.T
        else:
            self.guess["u"] = self.ocp.u_ref[:, min(self.n_f - 1, self.N)].T

        self.verbosity = verbosity
        self.show_step = show_step
        self.allow_relaxation = allow_relaxation
        self.cont_on_failure = continue_on_failure

        # setup internal variables
        self.t_grid = self.ocp.t_grid
        self.solve_times = []
        self.solve_iterations = []
        self.x_opts = []
        self.u_opts = []
        self.J_opts = []
        self.L_opts = []
        self.J_f = 0
        self.L_sim = np.zeros(self.n_f)
        self.alpha_a_sim = []
        self.alpha_b_sim = []

        self._n_solver = None
        self.nmpc_solver = None
        self._setup_solver()
        if self.mode == MPCMode.OPEN_LOOP:
            logger.info("Computing global planning")
            if not self._run_solver(self.ocp.x_0, 0):
                raise SolverError("Solution failed.")

        # self.data = {'ocp_hash': self.ocp.data_hash}
        # data_keys = ['receding', 'N_u', 'u_guess', 'x_guess', 'n_coll_pts',
        #              'n_f', 'N', 'int_tol', 'ipopt_opts', 'opt_time_limit']
        # for (key, val) in self.__dict__.items():
        #     if key in data_keys:
        #         self.data[key] = val
        # self.data_hash = joblib_hash(self.data)
        # self.data['data_hash'] = self.data_hash

    def _plot_current(self, n, show=True):
        f, axs = plt.subplots(2, sharex=True)
        axs[0].set_title("Results for step {}".format(n))

        # state trajectories
        x_vals = np.vstack((self.x_opts[-1],
                            np.nan * np.ones((1, self.ocp.sys.n_x))))
        x_steps = list(range(len(x_vals)))
        for i in range(self.ocp.sys.n_x):
            axs[0].step(x_steps,
                        x_vals[:, i],
                        label="x_{0}".format(i),
                        where="post",
                        )
        axs[0].set_ylabel("states")
        axs[0].grid()
        axs[0].legend()

        # input trajectories
        u_vals = np.vstack((self.u_opts[-1],
                            np.nan * np.ones((1, self.ocp.sys.n_u))))
        u_steps = list(range(len(u_vals)))
        if n == 0 and self.ocp.u_prev is not None:
            u_steps.insert(0, -1)
            u_vals = np.vstack((np.atleast_2d(self.ocp.u_prev),
                                u_vals))
        for i in range(self.ocp.sys.n_u):
            axs[1].step(u_steps,
                        u_vals[:, i],
                        label="u_{0}".format(i),
                        where="post",
                        )
        axs[1].set_xlim(min(u_steps), self.N_0)
        axs[1].set_xlabel("n")
        axs[1].set_ylabel("inputs")
        axs[1].grid()
        axs[1].legend()

        if show:
            f.show()
        else:
            return f

        # plt.figure('L_sim')
        # plt.plot(self.sol['tgrid_sim'][:-1], self.sol['L_sim'], '-')
        # plt.legend('L')
        # plt.xlabel('t')
        # plt.grid()
        # plt.show()
        #
        # if self.ocp.mode == 'free':
        #     plt.figure('alpha_a_sim and alpha_b_sim')
        #     plt.plot(self.sol['tgrid_sim'][:len(self.sol['alpha_a_sim'])],
        #              self.sol['alpha_a_sim'], '-')
        #     plt.plot(self.sol['tgrid_sim'][:len(self.sol['alpha_b_sim'])],
        #              self.sol['alpha_b_sim'], '-')
        #     plt.xlabel('t')
        #     plt.legend(['alpha_a_sim', 'alpha_b_sim'])
        #     plt.grid()
        #     plt.show()

    def _estimate_alphas(self):
        L_inf_0 = self.ocp.T * self.ocp.l_func(self.x_opts[-1][:, 0],
                                               self.u_opts[-1][:, 0],
                                               self.ocp.x_ref[:, 0],
                                               self.ocp.u_ref[:, 0]
                                               ).full().squeeze()
        try:
            c = np.zeros(self.N)
            for n in range(self.N):
                c[n] = self.L_opts[-1][n] / L_inf_0

            g = c[0]
            g_prod_0 = 1
            g_prod_1 = 1
            for N in range(2, self.N+1):
                g += c[N-1]
                g_prod_0 *= g
                g_prod_1 *= (g - 1)
            alpha_a_N = 1 - ((g - 1) * g_prod_1) / (g_prod_0 - g_prod_1)
            self.alpha_a_sim.append(alpha_a_N)
        except Exception:
            self.alpha_a_sim.append(np.nan)

        if self.verbosity > 1:
            print('alpha_a_sim: {:.3f}'.format(self.alpha_a_sim[-1]))

        if len(self.L_opts) >= 2:
            J_opt_N = np.sum(self.L_opts[-1])
            J_opt_N_prev = np.sum(self.L_opts[-2][:self.N])
            alpha_b_N = (J_opt_N_prev - J_opt_N)/self.L_opts[-2][0]
            self.alpha_b_sim.append(alpha_b_N)

            if self.verbosity > 1:
                print('alpha_b_sim: {:.3f}'.format(self.alpha_b_sim[-1]))

    def _setup_solver(self, x0=None, n=None, relax=False):
        """
        Configures and creates the underlying problem solver
        """
        if x0 is None:
            x0 = self.ocp.x_0.squeeze()
        if n is None:
            n = 0

        # handle last input
        u_prev = self.ocp.u_prev
        if n > 0 and u_prev is not None:
            # re-init: Take input from last run
            u_prev = self.nmpc_solver.var['u', 0]

        dims = dict(t=self.N,
                    x=self.ocp.sys.n_x,
                    u=self.ocp.sys.n_u,
                    c=self.n_coll_pts)
        if self.ocp.e_func is not None:
            dims["e"] = np.prod(self.ocp.e_func.sx_out()[0].shape)

        # create solver object
        self.nmpc_solver = mpctools.nmpc(
            N=dims,
            x0=x0,
            f=self.ocp.sys.x_dot_fun,
            Delta=self.ocp.T,
            sp=dict(x=self.ocp.x_ref.T[n:n+self.N+1],
                    u=self.ocp.u_ref.T[n:n+self.N]),
            guess=self.guess,
            l=self.ocp.l_func,
            Pf=self.ocp.lf_func,
            e=self.ocp.e_func,
            ef=self.ocp.ef_func,
            lb=self.ocp.lb,
            ub=self.ocp.ub,
            timelimit=self.time_limit,
            funcargs=self.ocp.func_args,
            uprev=u_prev,
            verbosity=self.verbosity,
            solver="ipopt",
            discretel=False
        )

        # add a callback to observe the solver status
        n_g = self.nmpc_solver._ControlSolver__solver.size_out('g')[0]
        n_x = self.nmpc_solver._ControlSolver__solver.size_out('x')[0]
        n_p = self.nmpc_solver._ControlSolver__solver.size_in('p')[0]
        self.nlpsol_cb = NlpCallback("nlpsol_cb", n_x, n_g, n_p,
                                     self.verbosity, self.ocp.sys.n_x,
                                     self.ocp.x_ref[:, -1])
        ca_opts = {
            "iteration_callback": self.nlpsol_cb,
            "warn_initial_bounds": True,
        }
        so_opts = self.solver_kwargs.copy()
        if relax:
            # increase the solution tolerance
            # so_opts["acceptable_tol"] = 1e0
            so_opts["acceptable_constr_viol_tol"] = 1

        # configure solver
        self.nmpc_solver.initialize(casadioptions=ca_opts,
                                    solveroptions=so_opts)

    def _run_solver(self, x_0, n):
        if n == self._n_solver:
            logger.debug("Skipping re-computation, result already known.")
            return True

        # if self.ocp_sim_x_dict is not None:
        #     x_0_sim = x_0
        #     x_0 = np.zeros(self.ocp.sys.n_x)
        #     for (k, v) in self.ocp_sim_x_dict.items():
        #         x_0[v] = x_0_sim[k]

        # if hasattr(self.ocp.sys,"means_x"):
        #     means_x = self.ocp.sys.means_x
        #     variances_x = self.ocp.sys.variances_x
        #     x_0 = T(x_0,means_x,variances_x)

        # update initial state
        self.nmpc_solver.fixvar('x', 0, x_0)

        # run solver
        self.nmpc_solver.solve()

        # store variables for the next run
        if self.ocp.u_prev is not None:
            self.nmpc_solver.par["u_prev", 0] = self.nmpc_solver.var['u', 0]
        self.nmpc_solver.saveguess()

        # post process results
        self.sol_status = self.nmpc_solver.stats["status"]
        self.solve_times.append(self.nmpc_solver.stats["time"])
        self.solve_iterations.append(self.nlpsol_cb.k)
        self.nlpsol_cb.reset()

        x_opt = np.vstack([e.full().squeeze()
                           for e in self.nmpc_solver.var["x"]])
        self.x_opts.append(x_opt)

        u_opt = np.vstack([e.full().squeeze()
                           for e in self.nmpc_solver.var["u"]])
        self.u_opts.append(u_opt)

        self.J_opts.append(self.nlpsol_cb.J_opt)

        L_opt = np.zeros(self.N)
        for k in range(self.N):
            x_ref = self.ocp.x_ref[:, min(n+k, self.ocp.x_ref.shape[1] - 1)]
            u_ref = self.ocp.u_ref[:, min(n+k, self.ocp.u_ref.shape[1] - 2)]
            self.nmpc_solver.par['x_sp', k] = x_ref
            self.nmpc_solver.par['u_sp', k] = u_ref
            l_args = [x_opt[k], u_opt[k]]
            if "x_sp" in self.ocp.func_args["l"]:
                l_args.append(x_ref)
            if "u_sp" in self.ocp.func_args["l"]:
                l_args.append(u_ref)
            L_opt[k] = self.ocp.l_func(*l_args).full()[0][0] * self.ocp.T
        self.L_opts.append(L_opt)

        if self.mode in (MPCMode.LIMITED, MPCMode.HYBRID) \
                and n + self.N > self.n_f:
            logger.warning("Shortening the horizon")
            # horizon reached end of simulation domain -> shorten
            self.N -= 1

            # save current guesses
            g = self.nmpc_solver.guess

            # update them so they fit the shortened horizon
            xguess = np.reshape([e.full() for e in g["x"][1:]],
                                (self.N + 1, self.ocp.sys.n_x))
            uguess = np.reshape([e.full() for e in g["u"][1:]],
                                (self.N, self.ocp.sys.n_u))
            self.guess.update(dict(u=uguess, x=xguess))

            # re-init solver with shorter horizon
            self._setup_solver(x_opt[1], n)

        # logger.info("e_f: {:.3e}".format(self.e_f))
        logger.debug("J_f: {:.3e}".format(self.J_f))
        logger.info("Solving took total of {:.3f}s for {} iterations"
                    "".format(self.solve_times[-1],
                              self.solve_iterations[-1]))
        if self.show_step:
            self._plot_current(n)

        # check integrity of solution
        if self.sol_status not in ["Solve_Succeeded",
                                   "Solved_To_Acceptable_Level"]:
            logger.error(f"Solver returned with '{self.sol_status}'")
            if "Infeasible" in self.sol_status:
                self.constraint_info()
            return False

        # update current solver state
        self._n_solver = n

        return True

    def compute_feedback(self, t, x):
        if t >= self.t_grid[:-1].max():
            logger.warning("Provided time '{:.2f}'s outside of planning domain"
                           "[{:.2f}, {:.2f}]. "
                           "Repeating last known values."
                           .format(t, self.t_grid[0], self.t_grid[-1]))
            return self.x_opts[-1][-2:], self.u_opts[-1][-1:]

        n = np.where((self.t_grid - t) <= 0)[0][-1]
        if t not in self.t_grid:
            logger.debug("Provided time '{:.2f}'s not found in provided time "
                         "grid. Using step with t={:.2f} instead."
                         .format(t, self.t_grid[n]))

        # special cases that require no new solutions
        if self.mode == MPCMode.OPEN_LOOP:
            return self.x_opts[0][n:], self.u_opts[0][n:]
        elif self.mode == MPCMode.HYBRID and n + self.N > self.n_f:
            nr = n % self.N
            return self.x_opts[-1][nr:], self.u_opts[-1][nr:]

        # solve the control problem for the current state
        ret = self._run_solver(x, n)
        if not ret and self.sol_status == "Restoration_Failed":
            # try to fix the issue by resetting the solver
            logger.error("Solver failed")
            logger.warning("Performing re-init")
            self._setup_solver(x, n)
            ret = self._run_solver(x, n)

        if not ret \
                and self.sol_status == "Infeasible_Problem_Detected"\
                and self.allow_relaxation:
            logger.error("Solution failed to reach constraints")
            logger.warning("Restarting step with relaxed constraints")
            v = self.verbosity
            self.verbosity = 5
            self._setup_solver(x, n, relax=True)
            ret = self._run_solver(x, n)
            self.verbosity = v

        if not ret and not self.cont_on_failure:
            self._plot_current(n)
            raise SolverError("Solution failed for n={}".format(n))

        # TODO is this the right place?
        # if self.ocp.mode == OCPMode.FREE:
        #     self._estimate_alphas()

        # return optimal state and input trajectories
        return self.x_opts[-1], self.u_opts[-1]

    def constraint_info(self, tol=1e-10):
        sol = self.nmpc_solver._ControlSolver__sol
        con = self.nmpc_solver._ControlSolver__con
        sg = sol.get("g", None)
        if sg is None:
            logger.error("No constraint information available")
            return
        for idx in range(sg.shape[0]):
            lb = self.nmpc_solver.conlb[idx]
            ub = self.nmpc_solver.conub[idx]
            g = sg[idx]
            err = max(g - ub, lb - g)
            if err > tol:
                logger.error(f"Constraint {idx}/{sg.shape[0]}:\t"
                             f"{lb}\t<= {g}\t<= {ub} violated in "
                             f"expression: {con[idx]}")
