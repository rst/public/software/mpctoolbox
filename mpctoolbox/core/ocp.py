import os
import logging
from ast import literal_eval
from enum import IntEnum, auto
from joblib import dump as joblib_dump
from joblib import load as joblib_load
from joblib import hash as joblib_hash

import numpy as np
import casadi as ca
from scipy.linalg import solve_discrete_are as solve_dare

from .system import System as System

logger = logging.getLogger(__name__)


class OCPMode(IntEnum):
    """
    Enum holding options for the handling of terminal conditions

    For details concerning the definitions please refer to [GP17]_ .

    .. [GP17] Grüne and Pannek, Nonlinear Model Predictive Control,
        Springer 2017
    """

    XN = auto()
    """
    MPC with stabilizing terminal conditions with
    (equilibrium) endpoint constraint described in
    [GP17]_ pp. 92ff.
    """

    XNF = auto()
    """
    MPC with stabilizing terminal conditions with
    Lyapunov function terminal cost described in
    [GP17]_ pp. 99ff.
    """

    FREE = auto()
    """
    MPC without stabilizing terminal conditions described
    in [GP17]_ pp. 121ff.
    """

    CUSTOM = auto()
    """
    User-defined ocp mode, i.e. no restrictions on terminal
    state and terminal cost.
    """


def transform(x, means, variances):
    shape = x.shape
    x = np.reshape(x, (x.size,))
    z = np.empty(shape).reshape((x.size,))
    for i in range(x.size):
        z[i] = (x[i]-means[i])/variances[i]
    z = np.reshape(z, shape)
    return z


class OptimalControlProblem:
    """
    Class for user-defined optimal control problem (OCP)

    If no explicit cost function is provided, quadratic costs with weights
    matrices *Q* and *R* are used. If *Q* and *R* are missing, too unit matrices
    will be used.

    Args:
        sys (System): System to control.
        mode (OCPMode): Handling of terminal conditions, choose between
        x_0 (numpy.ndarray): Initial state of the system.
        t_grid (numpy.ndarray): Equidistant time grid for simulation implying a
            certain step size, used for both discretization and simulation.
        x_ref (numpy.ndarray): Reference state for every step with shape
            (n_x, n_t) or for all steps with shape (n_x,).
        u_ref (numpy.ndarray): Reference input for every step with shape
            (n_x, n_t) or for all steps with shape (n_x,).

    Keyword Args:
        lb (dict): Dict holding the lower bounds for state ('x'), input ('u')
            and input derivative ('Du').
        ub (dict): Dict holding the upper bounds for state ('x'), input ('u')
            and input derivative ('Du').
        q_mat (numpy.ndarray): Weight matrix for state in quadratic stage cost,
            defaults to unit matrix.
        r_mat (numpy.ndarray): Weight matrix for input in quadratic stage cost,
            defaults to unit matrix.
        ef_x_tol (numpy.ndarray): Tolerance for endpoint constraint in ocp mode
            with stabilizing terminal constraints ('xN').
        sigma (float): Value for sigma in ocp mode 'XNF' (must be greater one),
            defaults to 10.
        delta_0 (float): Initial guess for delta in ocp mode 'XNF',
            defaults to 1e3.
        delta (float): Value for delta in ocp mode 'XNF'.
        l_func (casadi.Function): Stage cost function.
        lf_func (casadi.Function): Terminal cost function.
        e_func (casadi.Function): General constraint function, admissible region
            is  given by :math:`e \\le 0`.
        ef_func (casadi.Function): Terminal constraint function, admissible
            region is  given by :math:`e_f \\le 0`.

    Warnings:
        If weight matrices and a step cost function are provided, the weight
        matrices will be ignored.

    Exceptions:
        AssertionError:
            mode not in OCPModes, invalid reference shapes,
            sigma <= 1

    """
    def __init__(self, sys, mode, x_0, t_grid, x_ref, u_ref,
                 lb=None, ub=None, u_prev=None,
                 q_mat=None, r_mat=None, l_func=None, lf_func=None,
                 e_func=None, ef_func=None, ef_x_tol=None,
                 sigma=None, delta_0=None, delta=None,
                 ):
        self.sys = sys
        self.mode = mode

        self.x_0 = x_0.squeeze()
        if sys.n_x != self.x_0.size:
            e = ValueError("Initial state does not match system definition")
            logger.exception(e)
            raise e

        self.t_grid = t_grid
        self.T = self.t_grid[1] - self.t_grid[0]

        self.x_ref = x_ref
        if self.x_ref.ndim == 1:  # allow 1d arrays
            self.x_ref = self.x_ref[:, None]
        if sys.n_x != self.x_ref.shape[0]:
            e = ValueError("Reference state does not match system definition")
            logger.exception(e)
            raise e
        if self.x_ref.shape[1] == 1:
            self.x_ref = self.x_ref.repeat(len(self.t_grid), axis=1)
        if self.x_ref.shape[1] != len(self.t_grid):
            e = ValueError("Provided reference states do not match the system "
                           "dimension and time grid. Required {}, Provided: {}"
                           "".format((self.sys.n_x, len(t_grid)),
                                     self.x_ref.shape)
                           )
            logger.exception(e)
            raise e

        self.u_ref = u_ref
        if self.u_ref.ndim == 1:  # allow 1d arrays
            self.u_ref = self.u_ref[:, None]
        if sys.n_u != self.u_ref.shape[0]:
            e = ValueError("Reference input does not match system definition")
            logger.exception(e)
            raise e
        if self.u_ref.shape[1] == 1:
            self.u_ref = self.u_ref.repeat(len(self.t_grid) - 1, axis=1)
        if self.u_ref.shape[1] != len(self.t_grid) - 1:
            e = ValueError("Provided reference inputs do not match the system "
                           "dimension and time grid. Required {}, Provided: {}."
                           "Remember that no reference input can be given for "
                           "the last time step."
                           "".format((self.t_grid.shape[0], self.sys.n_u),
                                     self.u_ref.shape)
                           )
            logger.exception(e)
            raise e

        self.lb = lb if lb is not None else {}
        self.ub = ub if ub is not None else {}

        if u_prev is None:
            if "Du" in self.lb or "Du" in self.ub:
                logger.warning("Boundaries for input derivatives stated "
                               "but no previous input 'u_prev' provided. "
                               "Ignoring constraints")
                self.lb.pop("Du", None)
                self.ub.pop("Du", None)
            self.u_prev = None
        else:
            self.u_prev = u_prev.squeeze()
            if self.u_prev.size != self.sys.n_u:
                err = ValueError(f"Shape of 'u_prev' {u_prev.shape} does not "
                                 f"match system dimension {sys.n_u}.")
                logger.exception(err)
                raise err

        if 0:
            # TODO handle means
            # self.uprev = self.data.setdefault('uprev', None)
            # if 'Du' in self.lb.keys() or 'Du' in self.ub.keys():
            #     self.uprev = self.data.setdefault('uprev', np.zeros(self.sys.n_u))
            # transform in MPC Area if transformation data is in metadata
            if hasattr(sys, "means_x"):
                x_ref = transform(x_ref, sys.means_x, sys.variances_x)
                x_0 = transform(x_0, sys.means_x, sys.variances_x)
                self.lb['x'] = transform(self.lb['x'], sys.means_x, sys.variances_x)
                self.ub['x'] = transform(self.ub['x'], sys.means_x, sys.variances_x)

            if hasattr(sys, "means_u"):
                u_ref = transform(u_ref, sys.means_u, sys.variances_u)
                self.lb['u'] = transform(self.lb['u'], sys.means_u, sys.variances_u)
                self.ub['u'] = transform(self.ub['u'], sys.means_u, sys.variances_u)
                self.lb['Du'] = transform(self.lb['Du'], sys.means_u, sys.variances_u)
                self.ub['Du'] = transform(self.ub['Du'], sys.means_u, sys.variances_u)

            #uprev = transform(uprev, sys.means_u, sys.variances_u)
            #TODO ub, lb, uprev transformieren

        # assemble setpoint values and tracking errors
        self.x_sp = ca.SX.sym('x_sp', self.sys.n_x)
        self.u_sp = ca.SX.sym('u_sp', self.sys.n_u)
        e_x = self.sys.x - self.x_sp
        e_u = self.sys.u - self.u_sp

        # state weight matrix
        req_q_shape = (self.sys.n_x, self.sys.n_x)
        if q_mat is not None and q_mat.shape != req_q_shape:
            e = ValueError("State weight matrix Q does not match system "
                           "definition. Provided: {}, Required: {}"
                           "".format(q_mat.shape, req_q_shape))
            logger.exception(e)
            raise e
        self.q_mat = q_mat

        # input weight matrix
        req_r_shape = (self.sys.n_u, self.sys.n_u)
        if r_mat is not None and r_mat.shape != req_r_shape:
            e = ValueError("Input weight matrix R does not match system "
                           "definition. Provided: {}, Required: {}"
                           "".format(r_mat.shape, req_r_shape))
            logger.exception(e)
            raise e
        self.r_mat = r_mat

        # build stage costs L()
        l_arg_symbols = [self.sys.x, self.sys.u, self.x_sp, self.u_sp]
        l_arg_names = ['x', 'u', 'x_sp', 'u_sp']
        if l_func is None:
            logger.info("No stage cost given, using quadratic cost as fallback")
            if self.q_mat is None:
                logger.info("No state weight matrix given, using unit matrix")
                self.q_mat = np.eye(self.sys.n_x)
            if self.r_mat is None:
                logger.info("No input weight matrix given, using unit matrix")
                self.r_mat = np.eye(self.sys.n_u)

            self.l_expr = e_x.T @ self.q_mat @ e_x + e_u.T @ self.r_mat @ e_u
            self.l_func = ca.Function('L_func', l_arg_symbols, [self.l_expr],
                                      l_arg_names, ['L'])
        else:
            # stage cost given, use that
            if q_mat is not None or r_mat is not None:
                logger.warning("Explicit stage cost and weight matrices "
                               "provided, ignoring weights in favor of given "
                               "cost function.")
            self.l_func = l_func
            l_arg_symbols_user = [l_arg_symbols[l_arg_names.index(n)]
                                  for n in self.l_func.name_in()]
            self.l_expr = self.l_func(*l_arg_symbols_user)

        # build stage and final constraints
        self.e_func = e_func
        self.ef_x_tol = ef_x_tol
        self.ef_func = ef_func

        # handle the different operation modes
        if self.mode == OCPMode.XN:
            # fixed terminal state
            if self.ef_func is None:
                logger.info("No explicit terminal constraints given, "
                            "using reference as fallback")
                if self.ef_x_tol is None:
                    ef_expr = e_x**2
                else:
                    ef_expr = e_x**2 - self.ef_x_tol**2
                self.ef_func = ca.Function('ef_func', [self.sys.x, self.x_sp],
                                           [ef_expr], ['x', 'x_sp'], ['ef'])
            if lf_func is not None:
                logger.warning("Terminal costs are given but are ignored for "
                               "fixed end state.")
            self.lf_func = None

        elif self.mode == OCPMode.XNF:
            # fixed terminal area
            self.x_f = self.x_ref[:, -1]
            self.u_f = self.u_ref[:, -1]
            if ~np.all(self.x_f[:, None] == self.x_ref) or\
                    ~np.all(self.u_f[:, None] == self.u_ref):
                logger.warning("Time variant reference not supported in ocp "
                               "mode 'XNF'. Last element of reference will be "
                               "used to estimate terminal cost and region.")

            # compute covariance matrix for stage costs
            self.p_mat = self._compute_p_mat()

            if sigma is None or sigma <= 1:
                raise ValueError("A value for 'sigma' (>0) must be provided"
                                 "in {}-mode.".format(self.mode))
            self.sigma = sigma
            self.V_inf = self.sigma * (e_x.T @ self.p_mat @ e_x)

            # terminal costs
            if lf_func is not None:
                logger.warning("Terminal area mode requires special terminal"
                               "costs but custom terminal costs "
                               "where provided by he user."
                               "Ignoring given the cost function.")
            self.lf_expr = self.V_inf
            self.lf_func = ca.Function('Lf_func', [self.sys.x, self.x_sp],
                                       [self.lf_expr], ['x', 'x_sp'], ['Lf'])

            err = ValueError("Either 'delta' OR 'delta_0' must be provided"
                             "in {}-mode.".format(self.mode))
            if delta is None:
                if delta_0 is None:
                    raise err
                self.delta = self._estimate_delta(delta_0)
            else:
                if delta_0 is not None:
                    raise err
                self.delta = delta

            # terminal constraints
            if self.ef_func is not None:
                logger.warning("Terminal area mode requires special terminal"
                               "constraints but custom terminal constraints "
                               "where provided."
                               "Ignoring given the constraint function.")
            self.ef = e_x.T @ e_x - self.delta**2
            self.ef_func = ca.Function('ef_func', [self.sys.x, self.x_sp],
                                       [self.ef], ['x', 'x_sp'], ['ef'])

        elif self.mode == OCPMode.FREE:
            # no terminal constraints or costs
            if lf_func is not None:
                logger.warning("End costs 'lf_func' provided but "
                               "free end state is chosen. Ignoring costs.")
            self.lf_func = None

            if self.ef_func is not None:
                logger.warning("End constraints 'ef_func' provided but free "
                               "end state is chosen. Ignoring constraints.")
            self.ef_func = None

        elif self.mode == OCPMode.CUSTOM:
            if lf_func is None:
                logger.warning("No end costs provided, re-using stage cost.")
                self.lf_func = self.l_func
            else:
                self.lf_func = lf_func
        else:
            e = ValueError("Unknown operation mode '{}'".format(mode))
            logger.exception(e)
            raise e

        self.func_args = {"l": self.l_func.name_in()}
        if self.lf_func is not None:
            self.func_args["Pf"] = self.lf_func.name_in()
        if self.e_func is not None:
            self.func_args["e"] = self.e_func.name_in()
        if self.ef_func is not None:
            self.func_args["ef"] = self.ef_func.name_in()

        if 0:
            self.data = {'sys_hash': self.sys.data_hash}
            self.data.update(kwargs)
            self.data['x_ref'] = self.x_ref
            self.data['u_ref'] = self.u_ref
            ca_fun_dict = self.data.get("ca_fun_dict", {})
            if ca_fun_dict:
                ca_fun_str_dict = {}
                for (key, fun) in ca_fun_dict.items():
                    ca_fun_str_dict[key] = str(fun(*fun.sx_in()))
                    ca_fun_dict[key] = fun.expand()
                self.data['ca_fun_str'] = ca_fun_str_dict
            self.data_hash = joblib_hash(self.data)
            self.data['data_hash'] = self.data_hash

    def _compute_p_mat(self):
        self.Q_d = self.T * self.q_mat
        self.R_d = self.T * self.r_mat

        self.A_d = np.reshape(
            self.sys.A_d_fun(self.x_f, self.u_f, self.T).full(),
            (self.sys.n_x, self.sys.n_x)
        )
        self.B_d = np.reshape(
            self.sys.B_d_fun(self.x_f, self.u_f, self.T).full(),
            (self.sys.n_x, self.sys.n_u)
        )
        p_mat = solve_dare(self.A_d, self.B_d, self.Q_d, self.R_d)

        return p_mat

    def _estimate_delta(self, delta_0=1e3):
        """
        TODO

        Args:
            delta_0 (float): Initial guess for delta.

        Returns:
            int: The estimated delta.
        """
        logger.info("Estimating delta")
        self.K = (ca.inv(self.R_d + self.B_d.T @ self.p_mat @ self.B_d)
                  @ self.B_d.T @ self.p_mat @ self.A_d)
        u = -self.K @ (self.sys.x - self.x_f)
        x_dot_d = self.sys.x_dot_d_fun(self.sys.x, u, self.T)
        V_inf_fun = ca.Function('V_inf_fun', [self.sys.x, self.x_sp],
                                [self.V_inf], ['x', 'x_sp'], ['V_inf'])

        L_d = ((self.sys.x - self.x_f).T @ self.Q_d @ (self.sys.x - self.x_f)
               + u.T @ self.R_d @ u)
        F = V_inf_fun(self.sys.x, self.x_f)
        F_p = V_inf_fun(x_dot_d, self.x_f)

        h = F - F_p - L_d
        h_fun = ca.Function('h_fun', [self.sys.x], [h], ['x'], ['h'])

        x_rnds = []
        np.random.seed(7)
        K = 5000*2**(self.sys.n_x - 1)
        for k in range(K):
            v = np.random.normal(loc=0, scale=1, size=self.sys.n_x)
            d = np.linalg.norm(v)
            x_rnds.append(v/d)

        delta = delta_0
        k_cs = np.arange(K)
        s = 0.5
        rel_tol = 1e-4
        while True:
            k_cs_new = []
            for k in k_cs:
                x_rnd = x_rnds[k]*delta
                h = h_fun(x_rnd + self.x_f).full()[0][0]
                if h < 0:
                    if k > 0.1*K and s < 0.1:
                        k_cs_new.append(k)
                    else:
                        delta *= 1 - s
                        break
            if len(k_cs_new) > 0:
                k_cs = k_cs_new
                delta *= 1 - s
                continue
            if h >= 0:
                if s > rel_tol:
                    if delta_0 > delta:
                        delta /= 1 - s
                        s /= 10
                        delta *= 1 - s
                    else:
                        delta /= 1 - s
                        delta_0 = delta
                else:
                    break
            if delta > 1e20:
                delta = 1e20
                logger.warning("Could not find upper bound for delta. "
                               "Value has been set to 1e20. "
                               "The system might be linear.")
                break
            if delta < 1e-20:
                delta = 1e-20
                logger.warning("Could not find lower bound for delta. "
                               "Value has been set to 1e-20. "
                               "The system might be linear.")
                break

        p = abs(int(np.floor(np.log10(rel_tol)) + 1))
        e = int(np.floor(np.log10(delta))) - p
        delta = int(delta*10**(-e))*10**e
        return literal_eval(np.format_float_scientific(delta, precision=p))

    def reconstruct(self, data=(), ca_fun_dict=None):
        """
        Allows changing the properties of the ocp specified in data dict and
        ca_fun_dict without manually recreating the ocp.

        Keyword arguments:
            data (dict):
                keys in ['lb', 'ub', 'uprev', 'mode', 'x_0', 't_grid', 'x_ref',
                         'u_ref', 'Q', 'R', 'data_hash', 'e_x_tol', 'sigma',
                         'delta_0', 'delta']
            ca_fun_dict (dict):
                dict of custom CasADi function objects
        """
        self.data.update(data)
        self.data.pop('data_hash')
        if 'delta' in self.data.keys() and 'delta' not in data.keys():
            self.data.pop('delta')
        self.__init__(self.sys, self.mode, self.x_0, self.t_grid, self.x_ref,
                      self.u_ref, kwargs=self.data, ca_fun_dict=ca_fun_dict)

    def save(self, filename):
        """
        Saves ocp to "ocps/ocp_cache.dat", assigns the provided filename

        Arguments:
            filename (str):
                name of the ocp to save
        """
        try:
            os.mkdir('benchmarks/ocps/')
        except FileExistsError:
            pass

        try:
            cache = joblib_load('ocps/ocp_cache.dat')
        except FileNotFoundError:
            cache = {}
            pass

        if self.data_hash in cache.keys() and filename in cache.keys():
            if self.data_hash == cache[filename]['data_hash']:
                print('Ocp "' + filename + '" has already been cached.')
                return
        if self.data_hash in cache.keys():
            cache.pop(cache[self.data_hash])
            print('Ocp has already been cached. New filename "'
                  + filename + '" assigned.')
        if filename in cache.keys():
            cache.pop(cache[filename]['data_hash'])
            print('Filename "' + filename + '" has already been assigned. '
                  + 'Associated ocp has been replaced.')

        cache[filename] = self.data
        cache[self.data_hash] = filename

        joblib_dump(cache, 'ocps/ocp_cache.dat')

        fn_file = open('ocps/ocp_filenames.txt', 'w')
        for key in cache.keys():
            if key in cache.values():
                fn_file.write(key+os.linesep)
        fn_file.close()

    @classmethod
    def load(cls, filename):
        """
        Loads the ocp specified via filename from "ocps/ocp_cache.dat"

        Arguments:
            filename (str):
                name of the ocp to load

        Returns:
            ocp (OptimalControlProblem):
                loaded ocp

        Exceptions:
            AssertionError:
                data hash of loaded system is different from saved data hash
        """
        ocp_cache = joblib_load('ocps/ocp_cache.dat')
        sys_cache = joblib_load('benchmarks/sys_cache.dat')
        ocp_data = ocp_cache[filename]
        sys_filename = sys_cache[ocp_data['sys_hash']]

        sys = System.load(sys_filename)
        assert ocp_data['sys_hash'] == sys.data_hash

        if 'ca_fun_str_dict' in ocp_data.keys():
            x_sp = ca.MX.sym('x_sp', sys.n_x)
            u_sp = ca.MX.sym('u_sp', sys.n_u)
            loc_dict = {'x': sys.x, 'u': sys.u, 'x_sp': x_sp, 'u_sp': u_sp}
            loc_dict['sq'] = lambda x: x**2
            ca_fun_dict = {}
            for (key, val) in ocp_data['ca_fun_str_dict']:
                ca_fun_dict[key] = eval(val, ca.__dict__, loc_dict)
        else:
            ca_fun_dict = None

        ocp_data_hash = ocp_data.pop('data_hash')
        ocp = cls(sys, ocp_data['mode'], ocp_data['x_0'], ocp_data['t_grid'],
                  ocp_data['x_ref'], ocp_data['u_ref'], kwargs=ocp_data,
                  ca_fun_dict=ca_fun_dict)
        assert ocp.data_hash == ocp_data_hash
        return ocp
