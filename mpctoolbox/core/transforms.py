"""
This module contains several transforms that may be applied to map the
user-defined variables into the ones used in the model.

Currently, the supported transformations include:
    * Unity transformation (for default behavior)
    * Standardisation

"""
import sys
import logging
import inspect

import numpy as np
import casadi as ca


logger = logging.getLogger(__name__)


class Transform:
    """
    Base class for all transformations.

    A transformations must describe the forward as well as the inverse
    mapping between two spaces and also specify the meta data required
    to perform the transformations.
    """
    required_fields = tuple()

    def __init__(self, *args):
        assert len(self.required_fields) == len(args)
        self._meta_data = {k: v for k, v in zip(self.required_fields, args)}

    def forward(self, values):
        """
        Forward transform.

        Args:
            values: Values to map into the new space.

        Returns: Transformed values.
        """
        return self._apply_transform(self._fwd, values)

    def backward(self, values):
        """
        Backward transform.

        Args:
            values: Values to map into original space.

        Returns: Transformed values.
        """
        return self._apply_transform(self._bwd, values)

    def _apply_transform(self, trafo, values):
        if isinstance(values, ca.SX):
            # apply transform for usage in casadi expression
            return trafo(values)
        elif values.ndim == 2:
            # apply transform for every time step (rows)
            return np.apply_along_axis(trafo, 0, values)
        elif values.ndim == 1:
            # apply transform for given values
            return trafo(values)

    def _fwd(self, values):
        """
        The internal forward transformation routine.

        Overwrite this method with the actual transformation.

        The types and dimensions of the mapped values should stay the same
        """
        raise NotImplementedError()

    def _bwd(self, values):
        """
        The internal backward transformation routine.

        Overwrite this method with the actual transformation.

        The types and dimensions of the mapped values should stay the same
        """
        raise NotImplementedError()

    def __eq__(self, other):
        """
        Implementation of equality check.

        To transforms are regarde equal if the are of the same type and use
        the same meta data.
        Args:
            other (Transform): Transform to compare against.

        Returns: True if equality holds, False otherwise.

        """
        if not isinstance(other, Transform):
            return NotImplemented
        if self.__class__ != other.__class__:
            return False
        if self._meta_data != other._meta_data:
            return False

        return True


class UnitTransform(Transform):
    """
    Unit transformation
    """
    required_fields = tuple()

    def _fwd(self, values):
        return values

    def _bwd(self, values):
        return values


class StandardizationTransform(Transform):
    """
    Standardization transformation

    This transform will subtract the mean and divide by the variance to arrive
    at mean free data with variance one.
    """
    required_fields = ("mean", "variance")

    def _fwd(self, values):
        return (values - self._meta_data["mean"]) / self._meta_data["variance"]

    def _bwd(self, values):
        return values * self._meta_data["variance"] + self._meta_data["mean"]


def get_required_data(trafo_type):
    """
    Return the required metadata to instantiate a certain transformation object.

    Args:
        trafo_type(str): Type of transformation.

    Returns: Tuple of str, required meta data fields.

    """
    cls = _get_trafo_class(trafo_type)
    return cls.required_fields


def build_transform(trafo_type, meta_data):
    """
    Construct a transformation object.

    Args:
        trafo_type(str): Type of transformation.
        meta_data: Meta data to use.

    Returns: Transformation object.

    """
    cls = _get_trafo_class(trafo_type)
    trafo = cls(*meta_data)
    return trafo


def _get_trafo_class(trafo_type):
    cls = _trafo_map.get(trafo_type, None)
    if cls is None:
        raise ValueError("Requested transform '{}' does not exist, kown types "
                         "are: {}"
                         "".format(trafo_type, _trafo_map.keys()))
    return cls


def _build_transformation_map():
    """
    Build a map of Transformation names and their required meta data
    """
    trafo_map = {}
    cls_members = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for name, cls in cls_members:
        if issubclass(cls, Transform):
            trafo_map[cls.__name__] = cls
    return trafo_map


_trafo_map = _build_transformation_map()
