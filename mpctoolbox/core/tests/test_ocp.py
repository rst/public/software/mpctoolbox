import unittest
import logging
import numpy as np
from numpy.testing import assert_array_equal

from mpctoolbox.core.ocp import OptimalControlProblem, OCPMode
from mpctoolbox.core.benchmarks.mass_spring_damper import (
    system as s, problem as p
)


logging.basicConfig()


class OCPTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.args = dict(sys=s.ref_sys,
                         mode=OCPMode.XN,
                         x_0=p.x_0,
                         t_grid=p.t_grid,
                         x_ref=p.x_ref_var,
                         u_ref=p.u_ref_var,
                         )

    def test_defaults(self):
        o = OptimalControlProblem(**self.args)
        assert o.e_func is None
        assert o.ef_x_tol is None
        assert o.lf_func is None

    def test_inputs_dims(self):
        # given values should match the provided system
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(s.ref_sys,
                                      p.mode,
                                      p.x_0[:1],
                                      p.t_grid,
                                      p.x_ref_const_1d,
                                      p.u_ref_const_1d,
                                      )
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(s.ref_sys,
                                      p.mode,
                                      p.x_0,
                                      p.t_grid,
                                      p.x_ref_const[:1],
                                      p.u_ref_const_1d,
                                      )
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(s.ref_sys,
                                      p.mode,
                                      p.x_0,
                                      p.t_grid,
                                      p.x_ref_const,
                                      p.u_ref_const[:0],
                                      )

        # scalar values for initial state and references should work
        o = OptimalControlProblem(s.ref_sys,
                                  p.mode,
                                  p.x_0_1d,
                                  p.t_grid,
                                  p.x_ref_const_1d,
                                  p.u_ref_const_1d,
                                  )
        # and automatic conversion to 1d should take place
        assert_array_equal(o.x_0, p.x_0_1d)
        assert_array_equal(o.x_ref, p.x_ref_var)
        assert_array_equal(o.u_ref, p.u_ref_var)

        # (1, N) shaped values for initial state and references are also fine
        o = OptimalControlProblem(s.ref_sys,
                                  p.mode,
                                  p.x_0,
                                  p.t_grid,
                                  p.x_ref_const,
                                  p.u_ref_const,
                                  )
        # and automatic conversion to 1d should take place
        assert_array_equal(o.x_0, p.x_0_1d)
        assert_array_equal(o.x_ref, p.x_ref_var)
        assert_array_equal(o.u_ref, p.u_ref_var)

        # 2d values for initial state and references are the default
        o = OptimalControlProblem(s.ref_sys,
                                  p.mode,
                                  p.x_0,
                                  p.t_grid,
                                  p.x_ref_var,
                                  p.u_ref_var,
                                  )
        assert_array_equal(o.x_0, p.x_0_1d)
        assert_array_equal(o.x_ref, p.x_ref_var)
        assert_array_equal(o.u_ref, p.u_ref_var)

    def test_mode_choices(self):
        # mode should be in OCPMode
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(s.ref_sys,
                                      "FAST",
                                      p.x_0,
                                      p.t_grid,
                                      p.x_ref_var,
                                      p.u_ref_var,
                                      )

    def test_weight_matrices(self):
        args = (s.ref_sys, OCPMode.XN, p.x_0, p.t_grid, p.x_ref_var, p.u_ref_var)

        # no functions, no weights, unit matrices should be used
        o = OptimalControlProblem(*args)
        assert_array_equal(o.q_mat, np.eye(s.ref_sys.n_x))
        assert_array_equal(o.r_mat, np.eye(s.ref_sys.n_u))

        # if, provided dimensions must match
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(*args, q_mat=np.eye(6))
        with self.assertRaises(ValueError):
            o = OptimalControlProblem(*args, r_mat=np.eye(3))

        o = OptimalControlProblem(*args, q_mat=p.q_mat, r_mat=p.r_mat)
        assert_array_equal(o.q_mat, p.q_mat)
        assert_array_equal(o.r_mat, p.r_mat)

    def test_stage_costs(self):
        x_d = p.x_0 - p.x_ref_const
        u_d = p.u_prev - p.u_ref_const

        # no weights -> quadratic costs wih unit weights
        o = OptimalControlProblem(**self.args)
        l_ocp = o.l_func(p.x_0_1d, p.u_prev, p.x_ref_const_1d, p.u_ref_const_1d)
        l_test = x_d.T @ x_d + u_d.T @ u_d
        self.assertAlmostEqual(l_ocp, l_test)

        # provided weights -> quadratic costs with those
        o = OptimalControlProblem(**self.args, q_mat=p.q_mat, r_mat=p.r_mat)
        l_ocp = o.l_func(p.x_0_1d, p.u_prev, p.x_ref_const_1d, p.u_ref_const_1d)
        l_test = x_d.T @ p.q_mat @ x_d + u_d.T @ p.r_mat @ u_d
        self.assertAlmostEqual(l_ocp, l_test)

        # provided stage costs -> use those (only makes sense in FREE mode)
        o = OptimalControlProblem(**self.args, l_func=p.l_func)
        l_ocp = o.l_func(p.x_0_1d, p.u_prev)
        l_test = p.l_func(p.x_0_1d, p.u_prev)
        self.assertAlmostEqual(l_ocp, l_test)

    def test_final_costs(self):
        x_d = p.x_0 - p.x_ref_const
        u_d = p.u_prev - p.u_ref_const

        # final costs only makes sense in CUSTOM mode since XN and FREE do not
        # allow any and XNF computes its own
        args = self.args.copy()
        args["mode"] = OCPMode.CUSTOM

        # no weights -> quadratic costs with unit weights based on references
        o = OptimalControlProblem(**args)
        lf_ocp = o.lf_func(p.x_0_1d, p.u_prev, p.x_ref_const_1d, p.u_ref_const_1d)
        lf_test = x_d.T @ x_d + u_d.T @ u_d
        self.assertAlmostEqual(lf_ocp, lf_test)

        # provided weights -> quadratic costs with those
        o = OptimalControlProblem(**args, q_mat=p.q_mat, r_mat=p.r_mat)
        lf_ocp = o.lf_func(p.x_0_1d, p.u_prev, p.x_ref_const_1d, p.u_ref_const_1d)
        lf_test = x_d.T @ p.q_mat @ x_d + u_d.T @ p.r_mat @ u_d
        self.assertAlmostEqual(lf_ocp, lf_test)

        # provided final costs -> use those
        o = OptimalControlProblem(**args, lf_func=p.lf_func)
        lf_ocp = o.lf_func(p.x_0_1d, p.u_prev)
        lf_test = p.lf_func(p.x_0_1d, p.u_prev)
        self.assertAlmostEqual(lf_ocp, lf_test)

        for mode in (OCPMode.XN, OCPMode.FREE):
            args["mode"] = mode
            o = OptimalControlProblem(**args)
            assert o.lf_func is None
            o = OptimalControlProblem(**args, q_mat=p.q_mat, r_mat=p.r_mat)
            assert o.lf_func is None
            o = OptimalControlProblem(**args, lf_func=p.lf_func)
            assert o.lf_func is None

        args["mode"] = OCPMode.XNF
        with self.assertRaises(ValueError):
            # sigma is needed for lf
            o = OptimalControlProblem(**args)

        args["sigma"] = 5
        with self.assertRaises(ValueError):
            # at least delta_0 is needed for ef
            o = OptimalControlProblem(**args)

        args["delta"] = 10
        with self.assertRaises(ValueError):
            # either precomputed delta or delta0 may be given, but not both
            o = OptimalControlProblem(**args, delta_0=123)

        o = OptimalControlProblem(**args)
        lf_ocp = o.lf_func(p.x_0_1d, p.x_ref_const_1d)
        lf_test = o.sigma * x_d.T @ o.p_mat @ x_d
        self.assertAlmostEqual(lf_ocp, lf_test)

    def test_step_constraints(self):
        o = OptimalControlProblem(**self.args, e_func=p.e_func)
        self.assertEqual(o.e_func, p.e_func)

    def test_boundary_constraints(self):
        o = OptimalControlProblem(**self.args, lb=p.lb)
        self.assertEqual(o.lb, p.lb)

        # if Du is provided but 'u_prev' is not -> warn and ignore Du
        o = OptimalControlProblem(**self.args, ub=p.ub)
        assert "Du" not in o.ub

        o = OptimalControlProblem(**self.args, ub=p.ub, u_prev=p.u_prev)
        assert o.ub == p.ub
        assert o.u_prev == p.u_prev

    def test_final_constraints(self):
        args = self.args.copy()
        xf = p.x_ref_var[:, -1]

        rand_states = np.random.rand(s.ref_sys.n_x, 1000)
        rand_states[:, 500] = xf
        r = rand_states - xf[:, None]
        r2 = r ** 2

        # XN -> use last entry in references as constraints
        args["mode"] = OCPMode.XN

        # without tol, constraints may only be fulfilled in xf
        o = OptimalControlProblem(**args)
        for idx, state in enumerate(rand_states.T):
            c = o.ef_func(state, xf)
            if idx == 500:
                assert np.allclose(c, 0)
            assert_array_equal(c, r2[:, idx:idx+1])

        # with tol, constraints may only be fulfilled in xf - tol
        rt = r2 - p.ef_x_tol ** 2
        args["ef_x_tol"] = p.ef_x_tol
        o = OptimalControlProblem(**args)
        for idx, state in enumerate(rand_states.T):
            c = o.ef_func(state, xf)
            assert_array_equal(c, rt[:, idx:idx+1])

        # XNF -> constraints are ignored in favour of special area constraints
        delta = 10
        a = r[:, None, :].T
        b = r[None].T
        c = (a @ b).squeeze()
        rf = c - delta ** 2
        args["mode"] = OCPMode.XNF
        args["sigma"] = 5
        args["delta"] = delta
        o = OptimalControlProblem(**args)
        for idx, state in enumerate(rand_states.T):
            c = o.ef_func(state, xf)
            assert_array_equal(c, rf[idx:idx + 1, None])

    def test_compute_p(self):
        assert False

    def test_compute_delta(self):
        assert False


if __name__ == '__main__':
    unittest.main()
