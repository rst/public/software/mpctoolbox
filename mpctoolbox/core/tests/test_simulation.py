import logging
import unittest
import numpy as np
from numpy.testing import assert_array_equal

from matplotlib import pyplot as plt
from mpctoolbox.core.simulation import InitialValueProblem, Simulator
from mpctoolbox.core.benchmarks.mass_spring_damper import (
    system as s, problem as p, control as c
)
from mpctoolbox import identity_map, OCPMode, MPCMode

t_short = np.linspace(0, .5)
t_ref = np.linspace(0, 1, num=1000)
t_long = np.linspace(0, 2)

x0_ref = p.x_0_1d
x0_sim = x0_ref + 3


class IVPTestCase(unittest.TestCase):

    def test_init(self):
        p = InitialValueProblem(s.sim_sys, x0_sim, t_short)
        assert p.sys == s.sim_sys
        assert all(p.x0 == x0_sim)
        assert all(p.t_grid == t_short)


class SimulatorTestCase(unittest.TestCase):

    show_plots = False
    # show_plots = True

    ivp_r = InitialValueProblem(s.ref_sys, x0_ref, t_ref)
    ivp_rs = InitialValueProblem(s.ref_sys, x0_ref, t_short)
    ivp_rl = InitialValueProblem(s.ref_sys, x0_ref, t_long)

    ivp_x_err = InitialValueProblem(s.ref_sys, x0_sim, t_ref)
    ivp_m_err = InitialValueProblem(s.sim_sys, x0_ref, t_ref)
    ivp_mx_err = InitialValueProblem(s.sim_sys, x0_sim, t_ref)

    def _plot_sol(self, sol, c=None):
        if not self.show_plots:
            return

        f, ax = plt.subplots()
        ax.plot(sol["t_grid"], sol["x_sim"], label="x")
        ax.step(sol["t_grid"], sol["u_sim"], label="u", where="post")
        ax.legend()
        ax.grid()
        if c is not None:
            ax.set_title(c.mode)
        f.show()

    def _plot_cont_states(self, cont):
        if not self.show_plots:
            return

        f, ax = plt.subplots()
        for idx, x_opt in enumerate(cont.x_opts):
            steps = range(idx, len(x_opt) + idx)
            ax.plot(steps, x_opt)
        f.show()

    def setUp(self) -> None:
        logging.basicConfig()
        l = logging.getLogger("mpctoolbox.core.mpc")
        # l.setLevel(logging.INFO)
        self.l = logging.getLogger(__name__)
        # self.l .setLevel(logging.DEBUG)

        self.cd = c.get_c_dict()
        self.c_xn_o = self.cd[MPCMode.OPEN_LOOP]

    def test_init(self):
        # if system ref == sim then deduction of the state map should work
        sim = Simulator(self.ivp_r, self.c_xn_o)

        # if they differ, however deduction fails
        with self.assertRaises(ValueError):
            sim = Simulator(self.ivp_m_err, self.c_xn_o)

        # if the map is provided, everything should be fine
        sim = Simulator(self.ivp_x_err, self.c_xn_o, sim_des_map=identity_map)
        assert all(sim.x_sim[0] == self.ivp_x_err.x0)

    def test_ref_sim(self):
        for mode, cont in self.cd.items():
            # self.l.info(f"Running {mode}")
            # cont.show_step = True
            sim = Simulator(self.ivp_r, cont)
            res = sim.solve()

            self._plot_cont_states(cont)
            self._plot_sol(res, cont)
            assert res["status"] == "finished"

    def test_ref_sim_short(self):
        sim = Simulator(self.ivp_rs, self.c_xn_o)
        res = sim.solve()
        assert res["status"] == "finished"
        self._plot_sol(res, self.c_xn_o)

    def test_ref_sim_long(self):
        sim = Simulator(self.ivp_rl, self.c_xn_o)
        res = sim.solve()
        assert res["status"] == "finished"
        self._plot_sol(res, self.c_xn_o)

    def test_x_err_sim(self):
        for mode, cont in self.cd.items():
            self.l.info(f"Running {mode}")
            sim = Simulator(self.ivp_x_err, cont)
            res = sim.solve()
            self._plot_cont_states(cont)
            self._plot_sol(res, cont)
            assert res["status"] == "finished"

    def test_m_err_sim(self):
        for mode, cont in self.cd.items():
            self.l.info(f"Running {mode}")
            sim = Simulator(self.ivp_m_err, cont, sim_des_map=identity_map)
            res = sim.solve()
            self._plot_cont_states(cont)
            self._plot_sol(res, cont)
            assert res["status"] == "finished"

    def test_mx_err_sim(self):
        for cont in self.cd.values():
            sim = Simulator(self.ivp_mx_err, cont, sim_des_map=identity_map)
            res = sim.solve()
            assert res["status"] == "finished"
            self._plot_sol(res, cont)
