import unittest

import numpy as np
from numpy.testing import assert_array_equal, assert_array_almost_equal
import casadi as ca

from mpctoolbox.core.transforms import (
    Transform, UnitTransform, StandardizationTransform,
    get_required_data, build_transform
)


class TransformTestCase(unittest.TestCase):

    def test_defaults(self):
        assert Transform.required_fields == tuple()

    def test_init(self):
        t = Transform()
        assert t._meta_data == dict()

    def test_calls(self):
        t = Transform()
        with self.assertRaises(NotImplementedError):
            t.forward(np.ones(5))

        with self.assertRaises(NotImplementedError):
            t.backward(np.ones(5))


class UnitTransformTestCase(unittest.TestCase):

    def test_defaults(self):
        assert UnitTransform.required_fields == tuple()

    def test_init(self):
        t = UnitTransform()
        assert t._meta_data == dict()

    def test_calls(self):
        t = UnitTransform()

        src_values = np.random.random(10)
        dst_values = src_values

        assert_array_equal(dst_values, t._fwd(src_values))
        assert_array_equal(src_values, t._bwd(dst_values))

    def test_apply_operator(self):
        t = UnitTransform()

        # numpy 1d
        src_values = np.random.random(10)
        dst_values = src_values
        assert (dst_values == t._apply_transform(t._fwd, src_values)).all()
        assert (src_values == t._apply_transform(t._bwd, dst_values)).all()

        # numpy 2d
        src_values = np.random.random((10, 100))
        dst_values = src_values
        assert (dst_values == t._apply_transform(t._fwd, src_values)).all()
        assert (src_values == t._apply_transform(t._bwd, dst_values)).all()

        # casadi
        src_values = ca.SX.sym("x", 10)
        dst_values = ca.SX.eye(10) @ src_values
        assert (dst_values - t._apply_transform(t._fwd, src_values)).is_zero()
        assert (src_values - t._apply_transform(t._bwd, dst_values)).is_zero()


class StandardTransformTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.src_data_2d = np.random.random((8, 100))
        self.src_data_1d = self.src_data_2d[:, 0]
        self.src_data_ca = ca.SX.sym("x", 8)

        self.mean = np.mean(self.src_data_2d, axis=1)
        self.var = np.var(self.src_data_2d, axis=1)

        self.dst_data_1d = (self.src_data_1d - self.mean) / self.var
        self.dst_data_2d = (self.src_data_2d - self.mean[:, None]) / self.var[:, None]
        self.dst_data_ca = (self.src_data_ca - self.mean) / self.var

    def test_defaults(self):
        assert StandardizationTransform.required_fields == ("mean", "variance")

    def test_init(self):
        # wrong arg count
        with self.assertRaises(AssertionError):
            t = StandardizationTransform()
        with self.assertRaises(AssertionError):
            t = StandardizationTransform()

        t = StandardizationTransform(self.mean, self.var)
        assert_array_equal(t._meta_data["mean"], self.mean)
        assert_array_equal(t._meta_data["variance"], self.var)

    def test_forward(self):
        t = StandardizationTransform(self.mean, self.var)
        assert_array_almost_equal(t.forward(self.src_data_1d), self.dst_data_1d)
        assert_array_almost_equal(t.forward(self.src_data_2d), self.dst_data_2d)
        (t.forward(self.src_data_ca) - self.dst_data_ca).is_zero()

    def test_backward(self):
        t = StandardizationTransform(self.mean, self.var)
        assert_array_almost_equal(t.backward(self.dst_data_1d), self.src_data_1d)
        assert_array_almost_equal(t.backward(self.dst_data_2d), self.src_data_2d)
        (t.backward(self.dst_data_ca) - self.src_data_ca).is_zero()


class UtilitiesTestCase(unittest.TestCase):

    def test_get_required_data(self):
        with self.assertRaises(ValueError):
            get_required_data("UnknownTransform")

        fields = get_required_data("Transform")
        assert fields == tuple()

        fields = get_required_data("StandardizationTransform")
        assert fields == ("mean", "variance")

    def test_build_transform(self):
        t1 = StandardizationTransform(5, 12)
        t2 = build_transform("StandardizationTransform", (5, 12))
        assert t1._meta_data == t2._meta_data


if __name__ == '__main__':
    unittest.main()
