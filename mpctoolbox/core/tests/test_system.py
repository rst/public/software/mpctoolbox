import unittest

import numpy as np
from numpy.testing import assert_array_equal
import casadi as ca

from mpctoolbox.core.system import System
from mpctoolbox.core.benchmarks.mass_spring_damper import system as sys


class SystemTestCase(unittest.TestCase):

    def test_init(self):
        # mandatory symbols x and/or u are missing in sym dict
        with self.assertRaises(KeyError):
            System(sys.x_dot, sym_dict=dict())
        with self.assertRaises(KeyError):
            System(sys.x_dot, sym_dict=dict(x=sys.x))
        with self.assertRaises(KeyError):
            System(sys.x_dot, sym_dict=dict(u=sys.u))
        System(sys.x_dot, sys.sym_dict)

        # rhs contains parameters that are not specified in p_values
        with self.assertRaises(RuntimeError):
            System(sys.x_dot_p, sys.sym_dict)
        s = System(sys.x_dot_p, sys.sym_dict,
                   p_val_dict=sys.p_dict, sxup_dict=sys.sxup_dict)

        # test if args are actually taken
        self.assertEqual(s.x.size1(), sys.x.size1())
        self.assertEqual(s.u.size1(), sys.u.size1())
        self.assertEqual(s.sxup_dict, sys.sxup_dict)

        # test correct sizes
        self.assertEqual(s.n_x, sys.x_dim)
        self.assertEqual(s.n_u, sys.u_dim)

    def test_state_map(self):
        s = System(sys.x_dot_p, sys.sym_dict,
                   p_val_dict=sys.p_dict, sxup_dict=sys.sxup_dict)

        # state mapping from self to self is the identity
        m = s.get_state_map(s)
        x = np.random.random(s.n_x)
        np.allclose(x, m(x))

        s2 = System(sys.x_dot_p, sys.sym_dict,
                    p_val_dict=sys.p_dict, sxup_dict=sys.sxup_dict)

        # mapping to some other system is unknown
        m = s.get_state_map(s2)
        assert m == NotImplemented


if __name__ == '__main__':
    unittest.main()
