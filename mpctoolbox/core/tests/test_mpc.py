import unittest
import logging
import numpy as np
from numpy.testing import assert_array_equal

from matplotlib import pyplot as plt

from mpctoolbox.core.mpc import MPController, MPCMode
from mpctoolbox.core.benchmarks.mass_spring_damper import (
    problem as p
)


class MPCSolverTestCase(unittest.TestCase):

    show_plots = False
    # show_plots = True

    def test_global(self):
        ocp = p.ocp_xn_full
        c = MPController(ocp, mode=MPCMode.OPEN_LOOP)
        self._fake_sim(c, self.show_plots)

    def test_semiglobal(self):
        ocp = p.ocp_xn_full
        c = MPController(ocp, hor_len=10, mode=MPCMode.HYBRID)
        self._fake_sim(c, self.show_plots)

    def test_limited(self):
        ocp = p.ocp_xn_full
        c = MPController(ocp, hor_len=10, mode=MPCMode.LIMITED)
        self._fake_sim(c, self.show_plots)

    def test_unlimited(self):
        ocp = p.ocp_xn_full
        c = MPController(ocp, hor_len=10, mode=MPCMode.UNLIMITED)
        self._fake_sim(c, self.show_plots)

    def _fake_sim(self, c, plot=False):
        xt = [c.ocp.x_0]
        ut = []
        grid = c.ocp.t_grid
        for t in grid[:-1]:
            x, u = c.compute_feedback(t, xt[-1])
            # assert_array_equal(x[0], xt[-1])
            xt.append(x[1])
            ut.append(u[0])
        ut.append(np.nan * ut[-1])

        ut = np.array(ut).T
        xt = np.array(xt).T

        # plot results
        if plot:
            f, ax = plt.subplots()
            ax.plot(grid, xt.T, label="state")
            ax.plot(grid, ut.T, label="input")
            ax.grid()
            ax.legend()
            f.show()

        self._test_constraints(c.ocp, xt, ut[:, :-1])

    def _test_constraints(self, ocp, x, u):
        eps = 1e-6
        if ocp.u_prev is not None:
            ud = u - np.hstack((np.atleast_1d(ocp.u_prev)[None],
                                u[..., :-1]))
        else:
            ud = None
        for bound, var in zip(["u", "x", "Du"], [u, x, ud]):
            if bound in ocp.lb:
                diff = var - np.atleast_2d(ocp.lb[bound]).T
                violations = diff[diff < 0]
                np.testing.assert_allclose(violations,
                                           np.zeros_like(violations),
                                           atol=1e-6)
            if bound in ocp.ub:
                diff = var - np.atleast_2d(ocp.ub[bound]).T
                violations = diff[diff > 0]
                np.testing.assert_allclose(violations,
                                           np.zeros_like(violations),
                                           atol=1e-6)


if __name__ == "__main__":
    logging.basicConfig()
    l = logging.getLogger("mpctoolbox.core.mpc")
    # l.setLevel(logging.DEBUG)
