import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.animation import FuncAnimation


__all__ = ["create_animation"]


def create_animation(sys_name, sol, title, file_path=None, filename=None,
                     show=False):

    if file_path is not None:
        try:
            os.mkdir(file_path)
        except FileExistsError:
            pass

    d = 1
    h_ani = (sol['tgrid_sim'][1] - sol['tgrid_sim'][0])*d

    def animate(t):
        if sys_name in ['pend_cart_pl', 'pend_cart']:
            x_pend = [x_cart[t], x1[t]]
            y_pend = [0, y1[t]]

        elif sys_name == 'triple_pend_cart_pl':
            x_pend = [x_cart[t], x1[t], x2[t], x3[t]]
            y_pend = [0, y1[t], y2[t], y3[t]]

        pole.set_data(x_pend, y_pend)
        cart.set_xy([x_cart[t]-0.1, -0.05])
        time_text.set_text(time_template % (t*h_ani))

        return pole, cart, time_text

    if sys_name in ['pend_cart_pl', 'pend_cart']:
        x_cart = sol['x_sim'].T[0, 0::d]
        s1 = 0.25
        x1 = -s1*np.sin(sol['x_sim'].T[2, 0::d]) + sol['x_sim'].T[0, 0::d]
        y1 = s1*np.cos(sol['x_sim'].T[2, 0::d])

    elif sys_name == 'triple_pend_cart_pl':
        x_cart = sol['x_sim'].T[0, 0::d]
        l1 = 0.320
        l2 = 0.419
        l3 = 0.485
        x1 = -l1*np.sin(sol['x_sim'].T[2, 0::d]) + sol['x_sim'].T[0, 0::d]
        y1 = l1*np.cos(sol['x_sim'].T[2, 0::d])
        x2 = -l2*np.sin(sol['x_sim'].T[2, 0::d] + sol['x_sim'].T[4, 0::d]) + x1
        y2 = l2*np.cos(sol['x_sim'].T[2, 0::d] + sol['x_sim'].T[4, 0::d]) + y1
        x3 = -l3*np.sin(sol['x_sim'].T[2, 0::d] + sol['x_sim'].T[4, 0::d]
                        + sol['x_sim'].T[6, 0::d]) + x2
        y3 = l3*np.cos(sol['x_sim'].T[2, 0::d] + sol['x_sim'].T[4, 0::d]
                       + sol['x_sim'].T[6, 0::d]) + y2

    plt.rcdefaults()
    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    ax.set(xlabel=r'$s$ in m', ylabel=r'$h$ in m')
    if sys_name in ['pend_cart_pl', 'pend_cart']:
        plt.ylim((-0.3, 0.3))
    elif sys_name == 'triple_pend_cart_pl':
        plt.ylim((-1.5, 1.5))
        plt.xlim((-1.5, 1.5))
    plt.title(title)
    time_template = '$t$ = %.2f s'
    time_text = ax.text(-0.15, 1.1, '', transform=ax.transAxes, zorder=3)
    if sys_name in ['pend_cart_pl', 'pend_cart']:
        rail, = ax.plot([min(x_cart) - 0.2, max(x_cart) + 0.2],
                        [0, 0], 'ks-', zorder=0)
    elif sys_name in ['dual_pend_cart_pl', 'dual_pend_cart']:
        rail, = ax.plot([min(min(x_cart) - 0.2, min(x1), min(x2)),
                         max(max(x_cart) + 0.2, max(x1), max(x2))],
                        [0, 0], 'ks-', zorder=0)
    elif sys_name == 'triple_pend_cart_pl':
        rail, = ax.plot([-1.2715, 1.2715], [0, 0], 'ks-', zorder=0)
    cart = patches.Rectangle((-0.1, -0.05), 0.2, 0.1, fc='k', zorder=1)
    pole, = ax.plot([], [], 'b.-', lw=2, zorder=2)
    ax.add_artist(cart)
    ani = FuncAnimation(fig, animate,
                        np.arange(0, int(len(sol['tgrid_sim'])/d)),
                        interval=0.5/h_ani, blit=True)

    if file_path is not None and filename is not None:
        ani.save(file_path + '/' + filename + '.mp4', dpi=200)

    if show:
        plt.show()
    else:
        plt.close()

    return ani
