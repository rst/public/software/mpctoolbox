"""
Various functions and helpers for easier usage of the toolbox
"""


__all__ = ["identity_map"]


def identity_map(x):
    return x