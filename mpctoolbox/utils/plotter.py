"""
Various visualisation routines
"""
import os
import locale
import numpy as np
from matplotlib import colors
from matplotlib import pyplot as plt


__all__ = ["Plotter"]


class Plotter(object):
    def __init__(self, x_label, x_array):
        self.x_label = x_label
        self.x_array = x_array

        self.subplots = []

    def add_subplot(self, y_arrays, y_label, plt_types='line', labels=None,
                    colors=None, linestyles='-', drawstyles='default',
                    linewidths=1, markers=None, markersizes=1,
                    bar_widths=None, colormap=None, cm_levels=None,
                    cm_center=None, cbar_label=None, z_mesh=None, alphas=1):
        subplot = {'plt_types': plt_types, 'y_arrays': y_arrays,
                   'y_label': y_label, 'labels': labels,
                   'colors': colors, 'linestyles': linestyles,
                   'drawstyles': drawstyles, 'linewidths': linewidths,
                   'markers': markers, 'markersizes': markersizes,
                   'bar_widths': bar_widths, 'colormap': colormap,
                   'cm_levels': cm_levels, 'cm_center': cm_center,
                   'cbar_label': cbar_label, 'z_mesh': z_mesh,
                   'alphas': alphas}

        for (key, val) in subplot.items():
            if key not in ['y_arrays', 'y_label', 'colormap', 'cm_levels',
                           'cm_center', 'cbar_label', 'z_mesh']:
                if type(val) != type(y_arrays):
                    subplot[key] = [val for yarray in y_arrays]

        self.subplots.append(subplot)

    def _initialize(self, fig_size, font_size, use_tex, file_path):
        locale.setlocale(locale.LC_NUMERIC, "")

        p_entries = [
            r'\usepackage{amsmath}',
            r'\usepackage[utf8]{inputenc}',
            r'\usepackage[ngerman]{babel}',
            r'\usepackage{lmodern}',
            r'\usepackage[T1]{fontenc}',
            r'\usepackage[babel=true]{microtype}',
        ]
        preamble = "\n".join(p_entries)

        params = {'axes.formatter.use_locale':  True,
                  'backend':                    'Qt5Agg',
                  'figure.figsize':             fig_size,
                  'axes.labelsize':             font_size,
                  'font.size':                  font_size,
                  'legend.fontsize':            font_size,
                  'xtick.labelsize':            font_size,
                  'ytick.labelsize':            font_size,
                  'text.usetex':                use_tex,
                  'text.latex.preamble':        preamble}

        plt.rcParams.update(params)

        if file_path is not None:
            try:
                os.mkdir(file_path)
            except FileExistsError:
                pass

    def create_plot(self, fig_size, fig_name, font_size=12, file_path=None,
                    show=False, usetex=False, grid_on=True, y_lpad=20,
                    x_min=None, x_max=None, y_mins=None, y_maxs=None,
                    x_ticks=None, x_tick_labels=None, y_ticks=None,
                    y_tick_labels=None, x_scale='linear', y_scales=None):

        self._initialize(fig_size, font_size, usetex, file_path)

        plt.close(fig_name)
        plt.ioff()
        fig = plt.figure(fig_name)
        axs = []
        try:
            for i in range(len(self.subplots)):
                axs.append(plt.subplot(int(str(len(self.subplots)) + '1'
                                           + str(i+1))))
                for j in range(len(self.subplots[i]['y_arrays'])):

                    if self.subplots[i]['plt_types'][j] == 'line':
                        y_len = len(self.subplots[i]['y_arrays'][j])
                        plt.plot(self.x_array[:y_len],
                                 self.subplots[i]['y_arrays'][j],
                                 label=self.subplots[i]['labels'][j],
                                 color=self.subplots[i]['colors'][j],
                                 linestyle=self.subplots[i]['linestyles'][j],
                                 drawstyle=self.subplots[i]['drawstyles'][j],
                                 linewidth=self.subplots[i]['linewidths'][j],
                                 marker=self.subplots[i]['markers'][j],
                                 markersize=self.subplots[i]['markersizes'][j],
                                 mew=self.subplots[i]['markersizes'][j]/10,
                                 alpha=self.subplots[i]['alphas'][j])

                    elif self.subplots[i]['plt_types'][j] == 'bar':
                        x = self.x_array
                        bar_num = len(self.subplots[i]['y_arrays'])
                        bar_width = self.subplots[i]['bar_widths'][j]
                        bar_pos = x + (j-bar_num/2)*bar_width + bar_width/2
                        plt.bar(bar_pos, self.subplots[i]['y_arrays'][j],
                                width=bar_width,
                                label=self.subplots[i]['labels'][j],
                                color=self.subplots[i]['colors'][j],
                                alpha=self.subplots[i]['alphas'][j])

                    elif self.subplots[i]['plt_types'][j] == 'contourf':
                        vmin = self.subplots[i]['cm_levels'][0]
                        vmax = self.subplots[i]['cm_levels'][-1]
                        vcenter = self.subplots[i]['cm_center']
                        divnorm = colors.DivergingNorm(vmin=vmin, vmax=vmax,
                                                       vcenter=vcenter)
                        y_array = self.subplots[i]['y_arrays'][j]
                        x_mesh, y_mesh = np.meshgrid(self.x_array, y_array)
                        ctf = plt.contourf(x_mesh, y_mesh,
                                           self.subplots[i]['z_mesh'],
                                           self.subplots[i]['cm_levels'],
                                           cmap=self.subplots[i]['colormap'],
                                           norm=divnorm,
                                           alpha=self.subplots[i]['alphas'][j])
                        for c in ctf.collections:
                            c.set_edgecolor('face')
                        cbar = fig.colorbar(ctf)
                        cbar.ax.set_ylabel(self.subplots[i]['cbar_label'],
                                           rotation=0, labelpad=y_lpad,
                                           verticalalignment='top', y=1.0)

                if self.subplots[i]['labels'][j] is not None:
                    plt.legend(loc=0)
                plt.ylabel(self.subplots[i]['y_label'], rotation=0,
                           labelpad=y_lpad,
                           verticalalignment='top', y=1.0)
                plt.margins(x=0.02)
                plt.grid(grid_on)
                if y_scales is not None:
                    plt.yscale(y_scales[i])
                plt.xscale(x_scale)
                if y_mins is not None:
                    plt.ylim(bottom=y_mins[i])
                if y_maxs is not None:
                    plt.ylim(top=y_maxs[i])
                if x_min is not None:
                    plt.xlim(left=x_min)
                if x_max is not None:
                    plt.xlim(right=x_max)
                if x_ticks is not None:
                    plt.xticks(ticks=x_ticks, labels=x_tick_labels)
                plt.setp(axs[i].get_xticklabels(),
                         visible=(i == len(self.subplots)-1))
                if y_ticks is not None:
                    if y_tick_labels is None:
                        plt.yticks(ticks=y_ticks[i])
                    else:
                        plt.yticks(ticks=y_ticks[i], labels=y_tick_labels[i])
        except Exception:
            plt.close(fig_name)
            raise

        plt.xlabel(self.x_label, horizontalalignment='right', x=1.0)
        plt.tight_layout()
        fig.align_ylabels()
        if file_path is not None and fig_name is not None:
            path = os.path.join(file_path, fig_name)
            plt.savefig(path)
        if show:
            plt.show()
        else:
            plt.close(fig_name)
