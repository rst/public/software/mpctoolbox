"""
Routines to check relations of the continuous space system (as graph of
onnx-file) and its metadata (part of the onnx-file)
"""
import logging

from .parser import get_input_names, get_output_names, get_meta_dict


logger = logging.getLogger(__name__)


def has_valid_metadata(model):
    input_names = get_input_names(model)
    output_names = get_output_names(model)
    meta_dict = get_meta_dict(model)
    input_tuples = eval(meta_dict["input_tuple"])
    output_tuples = eval(meta_dict["output_tuple"])

    x_count = input_names.count("x")
    u_count = input_names.count("u")
    p_count = input_names.count("p")
    t_count = input_names.count("t")
    x_dot_count = output_names.count("x_dot")

    if x_count != 1:
        logger.error('One x in metadata required. You got %s ', x_count)
        raise ValueError()
    if u_count != 1:
        logger.error('One u in metadata required. You got %s ', u_count)
        raise ValueError()
    if p_count > 1:
        logger.error('max. one p in metadata required. You got %s ', p_count)
        raise ValueError()
    if t_count > 1:
        logger.error('max. one t in metadata required. You got %s ', t_count)
        raise ValueError()
    if x_dot_count != 1:
        logger.error('One x_dot in metadata required. You got %s ', x_dot_count)
        raise ValueError()

    for idx, input_name in enumerate(input_names):
        if input_name not in ["x", "u", "p", "t"]:
            logger.error('%s input_name %s ', "input name", " not allowed.")
        if input_name == "x":
            x_dim = input_tuples[idx][1]

    for idx, output_name in enumerate(output_names):
        if output_name == "x_dot":
            x_dot_dim = output_tuples[idx][1]

    if x_dim != x_dot_dim:
        # logger.warning("dim(x)=",x_dim,"!=dim(x_dot)",x_dot_dim)
        logger.warning("dim(x)!=dim(x_dot)")


def is_adaptable(model):
    # ONNX Model is adaptable, if there is only one input and one output and
    # the sum of sizes of model inputs/outputs in metadata equals the size of the model input/output

    if (len(model.graph.input) != 1) or (len(model.graph.output) != 1):
        return False

    model_input_dim = model.graph.input[0].type.tensor_type.shape.dim[
        1].dim_value
    model_output_dim = model.graph.output[0].type.tensor_type.shape.dim[
        1].dim_value
    meta_dict = get_meta_dict(model)
    input_tuples = eval(meta_dict["input_tuple"])
    output_tuples = eval(meta_dict["output_tuple"])
    in_dim = 0
    out_dim = 0

    for input_tuple in input_tuples:
        in_dim = input_tuple[1] + in_dim
    for output_tuple in output_tuples:
        out_dim = output_tuple[1] + out_dim
    if (in_dim == model_input_dim) and (out_dim == model_output_dim):
        is_adaptable = True
    else:
        is_adaptable = False

    return is_adaptable


def is_consistend_to_metadata(model):
    metadata = model.metadata_props
    metadata_keys = [metadata[i].key for i in range(len(metadata))]
    mandatory_keys = ['input_tuple', 'output_tuple', 'input_transform',
                      'output_transform']
    for key in mandatory_keys:
        if key not in metadata_keys:
            raise KeyError("metadata_props not completed: ", key,
                           "is missing! Mandatory Keys:", mandatory_keys)
    meta_dict = get_meta_dict(model)
    metadata_input_tuples = eval(meta_dict["input_tuple"])
    model_inputs = model.graph.input
    metadata_output_tuples = eval(meta_dict["output_tuple"])
    model_outputs = model.graph.output

    is_consistend = is_equal(model_inputs, metadata_input_tuples) and is_equal(
        model_outputs, metadata_output_tuples)
    return is_consistend


def is_equal(model_io, metadata_tuples):
    is_consistend = True
    if (len(model_io) == len(metadata_tuples)):
        for i in range(0, len(model_io)):
            if ((model_io[i].name != metadata_tuples[i][0]) or
                    (model_io[i].type.tensor_type.shape.dim[1].dim_value !=
                     metadata_tuples[i][1])):
                is_consistend = False

                # logging.error("")
                print(i, "th. variable have false dim or name")
                print("dim",
                      model_io[i].type.tensor_type.shape.dim[1].dim_value)
                print(metadata_tuples[i][0])
                print("name", model_io[i].name)
                print(metadata_tuples[i][1])
    else:
        is_consistend = False
    return is_consistend


def check_metadata_var_names(model):
    input_names = get_input_names(model)
    output_names = get_output_names(model)
    if "x" not in input_names:
        raise ValueError("Name of state variable in onnx-model must be x")
    if "u" not in input_names:
        raise ValueError("Name of control variable in onnx-model must be u")
    if "x_dot" != output_names[0]:
        raise ValueError("First output of onnx-model must be x_dot")
    for input_name in input_names:
        if input_name not in ["x", "u", "t", "p"]:
            raise ValueError('''
                                only states("x"), controls("u"), time("t") and parameters("p")
                                allowed as inputs.
                            ''')
