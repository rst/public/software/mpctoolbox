"""
Routines for export and import to the open neuronal network exchange (ONNX)
format.
"""
import sys
import logging
import inspect
from ast import literal_eval

import casadi as ca
import numpy as np

from onnx.checker import check_model
from onnx.mapping import TENSOR_TYPE_TO_NP_TYPE

from mpctoolbox.core.transforms import get_required_data, build_transform

logger = logging.getLogger(__name__)


def get_meta_dict(model):
    metadata = model.metadata_props
    meta_dict = {m.key: m.value for m in metadata}

    for i in range(len(metadata)):
        new_key = metadata[i].key
        new_value = metadata[i].value
        meta_dict[new_key] = new_value
    return meta_dict


def get_tuple_names(model, field):
    meta_dict = get_meta_dict(model)
    tuples = literal_eval(meta_dict[field])
    names = [t[0] for t in tuples]
    return names


def get_input_names(model):
    return get_tuple_names(model, "input_tuple")


def get_output_names(model):
    return get_tuple_names(model, "output_tuple")


def get_transformations(model):
    means_in, variances_in = get_input_transformation(model)
    means_out, variances_out = get_output_transformation(model)
    return means_in, variances_in, means_out, variances_out


def get_input_transformation(model):
    """
    returns means, variances 
    if input_transform = std, else none,none 
    """
    return _get_transformation(model, "input")


def get_output_transformation(model):
    """
    returns means, variances 
    if input_transform = std, else none,none 
    """
    return _get_transformation(model, "output")


def _get_transformation(model, prefix=""):
    """
    Create a transformation object using the given meta data of a model.

    Args:
        model_path (str): Path to .onnx model.
        prefix (str): Optional prefix to select a specific data set,
            defaults to "".
    """
    meta_dict = get_meta_dict(model)
    t_name = "_".join((prefix, "transform"))
    trafo_type = meta_dict.get(t_name, "")
    req_data = get_required_data(trafo_type)

    args = []
    for entry in req_data:
        lbl = "".join((prefix, entry))
        value = meta_dict.get(lbl, None)
        if value is None:
            raise ValueError("Required field '{}' not present in meta data."
                             "".format(lbl))
        args.append(literal_eval(value))

    trafo = build_transform(trafo_type, args)
    return trafo


def ca_max(x):
    expr = x[0]
    for element in x:
        expr = ca.fmax(element, expr)
    return expr


def ca_mean(x):
    expr = 0
    for element in x:
        expr = expr + element
    expr = expr / len(x)
    return expr


def ca_elem_sum(x):
    expr = 0
    for element in x:
        expr = expr + element
    return expr


def import_from_onnx(onnx_model):
    """
    Build a casadi expressions for input and output from an onnx file.

    The constructor assumes that all computation nodes are provided in the
    correct order, so that the expression can be iteratively build.

    Args:
        onnx_model(str): Path to the .onnx file.

    Returns:
        Tuple of rhs, state and input vectors as casadi graph objects

    """
    check_model(onnx_model, full_check=True)

    graph = onnx_model.graph
    graph_input_names = [out.name for out in graph.input]
    graph_output_names = [out.name for out in graph.output]

    all_inputs = collect_input_data(graph)
    nodes = collect_node_data(graph)

    # build expression tree
    # output_counter = 0
    for name, node in nodes.items():
        node_inputs = [all_inputs[inp] for inp in node["inputs"]]
        expr = _build_expr(node["op"], node_inputs, node["args"])
        all_inputs[name] = expr

        # logger.debug(name)
        # logger.debug(node)
        # logger.debug(expr)
        # if isinstance(expr, list):
        #     all_inputs[name] = expr[output_counter]
        #     output_counter = output_counter + 1
        # else:
        #     all_inputs[name] = expr
        #     output_counter = 0

    # collect inputs and outputs
    casadi_inputs = [all_inputs[v] for v in graph_input_names]
    casadi_outputs = [all_inputs[v] for v in graph_output_names]

    return casadi_inputs, casadi_outputs


def collect_node_data(graph):
    nodes = {}
    for op_idx, op in enumerate(graph.node):
        logger.debug("Analyzing node {}:\n {}".format(op_idx, op))
        for output in op.output:
            assert output not in nodes  # no cyclic graphs
            nodes[output] = {"inputs": tuple(op.input),
                             "op": op.op_type,
                             "args": op.attribute
                             }
    return nodes


def get_entries(graph):
    model_output_names = []
    for graph_output in graph.output:
        model_output_names.append(graph_output.name)
    return model_output_names


def _build_expr(op, inputs, args):
    cs_op = onnx_casadi_map.get(op, None)
    if cs_op is None:
        raise NotImplementedError("ONNX Operation '{}' not (yet) implemented"
                                  "".format(op))
    expr = cs_op(inputs, args)
    return expr


def collect_input_data(graph):
    """
    Retrieve and create casadi input values for graph computation

    Args:
        graph (onnx.graph): Expression graph to work on.

    Returns: Dict mapping from input names to their data.
    """
    input_map = {}

    for initializer in graph.initializer:
        name = initializer.name
        const = extract_constant(initializer)
        input_map[name] = const
        logger.debug(const)

    for inp in graph.input:
        name = inp.name
        shape = _get_shape(inp)
        var = ca.SX.sym(name, *shape)
        input_map[name] = var
        logger.debug(var)

        # if name in input_names:
        #     if len(shape) > 2:
        #         raise NotImplementedError("Check this code")
        #         ca_shape = [shape[-2], shape[-1]]
        #         var = ca.SX.sym(name, *ca_shape)
        #         var = [ca.SX.sym("1", *ca_shape) for i in range(shape[0])]
        #         # only for shape 1 x ? x ? x ?
        #         var = [[ca.SX.sym(str(i), *ca_shape) for i in range(shape[1])]]
        #     else:
        #         var = ca.SX.sym(name, *shape)
        #     input_variables[name] = var
        #     logger.debug(var)
        # else:
        #     tensor = init_map[name]
        #     const = extract_constant(tensor)
        #     constants[name] = const
        #     logger.debug(const)

    return input_map


def extract_constant(tensor):
    # TODO pytorch model fails;str in rawdata?!
    tensor_dtype = tensor.data_type
    dims = tensor.dims
    np_dtype = TENSOR_TYPE_TO_NP_TYPE[tensor_dtype]
    if np_dtype.name == 'double':
        data = tensor.double_data
    elif np_dtype.name == 'float32':
        # data = tensor.external_data
        data = tensor.float_data
    elif np_dtype.name == 'int64':
        data = tensor.int64_data
    else:
        raise NotImplementedError("Constant extraction for dtype '{}'"
                                  "not implemented (yet).".format(np_dtype))

    if len(data) == 0:
        array = np.frombuffer(tensor.raw_data, dtype=np_dtype).reshape(dims)
    else:
        array = np.asarray(data, dtype=np_dtype).reshape(dims)

    return array


def _get_shape(inp) -> tuple:
    shape = tuple(d.dim_value for d in inp.type.tensor_type.shape.dim)
    return shape


class CasadiCallWrapper:
    """
    Class that wraps casadi calls to onnx operators
    """
    onnx_op = None
    arg_names = None
    arg_defaults = None

    def __init__(self):
        pass

    @classmethod
    def _get_argument_dict(cls, args):
        # logger.debug(args)
        args_dict = {}
        for idx, name in enumerate(cls.arg_names):
            # TODO wenn nur ein attribut proto übergeben wird -> fehler
            # print(cls.onnx_op)
            arg = next((arg for arg in args if arg.name == name), None)
            if arg is not None:
                if arg.type == arg.INT:
                    val = arg.i
                elif arg.type == arg.INTS:
                    val = arg.ints
                elif arg.type == arg.FLOAT:
                    val = arg.f
                elif arg.type == arg.TENSOR:
                    val = arg.t
                elif arg.type == arg.STRING:
                    val = arg.s
                else:
                    raise NotImplementedError("Type {} not supported yet"
                                              "".format(arg.type))
            else:
                val = cls.arg_defaults[idx]
            args_dict[name] = val

        return args_dict

    @classmethod
    def eval(cls, inputs, args):
        arg_dict = cls._get_argument_dict(args)
        return cls._impl(inputs, arg_dict)

    @staticmethod
    def _impl(inputs, arg_dict):
        pass


class Identity(CasadiCallWrapper):
    onnx_op = "Identity"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return inputs[0]


class MatMul(CasadiCallWrapper):
    onnx_op = "MatMul"
    arg_names = tuple()
    arg_defaults = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        A, B = inputs
        return A @ B


class Gemm(CasadiCallWrapper):
    onnx_op = "Gemm"
    arg_names = ("alpha", "beta", "transA", "transB")
    arg_defaults = (1.0, 1.0, 0, 0)

    @staticmethod
    def _impl(inputs, arg_dict):
        A, B, C = inputs
        a_mat = A.T if arg_dict["transA"] else A
        b_mat = B.T if arg_dict["transB"] else B
        p1 = arg_dict["alpha"] * a_mat @ b_mat
        c_mat = np.broadcast_to(C, p1.shape)
        p2 = arg_dict["beta"] * c_mat
        return p1 + p2


class Concat(CasadiCallWrapper):
    onnx_op = "Concat"
    arg_names = ("axis",)

    @staticmethod
    def _impl(inputs, arg_dict):
        axis = arg_dict["axis"]
        if axis == -1:
            op = ca.horzcat
        elif axis == -2:
            op = ca.vertcat
        else:
            raise NotImplementedError(
                "Unknown concat dimension: {}".format(axis))
        expr = op(*inputs)
        return expr


class Split(CasadiCallWrapper):
    onnx_op = "Split"
    arg_names = ("axis", "split")

    # arg_defaults = (0,[])
    @staticmethod
    def _impl(inputs, arg_dict):
        axis = arg_dict["axis"]
        split = arg_dict["split"]
        split = np.asarray(split, dtype=int)
        lengths = [0]

        for i in range(1, split.size + 1):
            lengths.append(sum(split[0:i]))

        if axis == -1:
            op = ca.horzsplit
        elif axis == -2:
            op = ca.vertsplit
        else:
            raise NotImplementedError(
                "Unknown split dimension: {}".format(axis))

        expr = op(*inputs, lengths)
        # TODO: which output should be returned?
        return expr


class Add(CasadiCallWrapper):
    onnx_op = "Add"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        input_is_3d_tensor = isinstance(inputs[0], list)

        if input_is_3d_tensor:
            A = inputs[0]
            B = inputs[1]
            result = []
            for idsx, sx_sym in enumerate(A[0]):
                result.append(sx_sym + np.full(sx_sym.shape, B[idsx][0]))
            result = [result]
        else:
            A, B = inputs
            try:
                result = A + B
            except:
                result = A + B.reshape(A.shape)
        return result


class Sub(CasadiCallWrapper):
    onnx_op = "Sub"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        A, B = inputs
        try:
            result = A - B
        except:
            result = A - B.reshape(A.shape)
        return result


class Flatten(CasadiCallWrapper):
    onnx_op = "Flatten"
    arg_names = ("axis",)

    @staticmethod
    def _impl(inputs, arg_dict):
        return inputs[0]


class Relu(CasadiCallWrapper):
    onnx_op = "Relu"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        try:
            result = ca.fmax(0, *inputs)
        except:
            result = []
            for sx_sym in inputs[0][0]:
                result.append(ca.fmax(0, sx_sym))
                # result = [result]
        return result


class Reshape(CasadiCallWrapper):
    onnx_op = "Reshape"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        try:
            result = ca.reshape(inputs[0], *(inputs[1]))
        except:
            try:
                result = np.reshape(inputs[0], tuple(inputs[1]))
                return result
            except:
                stacked = inputs[0][0][0]
                for idsx, sx_sym in enumerate(inputs[0][0]):
                    # shaped_sym = ca.reshape(sx_sym,(sx_sym.shape[0]*sx_sym.shape[0],1))
                    if idsx > 0:
                        stacked = ca.vertcat(sx_sym, stacked)
                stacked = ca.reshape(stacked, tuple(inputs[1]))
                return stacked


class Dropout(CasadiCallWrapper):
    onnx_op = "Dropout"
    arg_names = ("ratio",)
    arg_defaults = (0.5,)

    @staticmethod
    def _impl(inputs, arg_dict):
        return inputs


class Conv(CasadiCallWrapper):
    onnx_op = "Conv"
    arg_names = ("auto_pad", "dilations", "group",
                 "kernel_shape", "pads", "storage_order", "strides")

    arg_defaults = ("NOTSET", [1, 1], 1, "NOTSET", [0, 0, 0, 0], 0, [1, 1])

    @staticmethod
    def _impl(inputs, arg_dict):

        # try:
        #    image_shape = inputs[0][0][0].shape
        #    tensor_in = inputs[0]
        # except:
        input_is_matrix = False
        image_shape = inputs[0][0][0].shape
        tensor_in = inputs[0]
        try:
            num_channels = len(inputs[0])
        except:
            image_shape = inputs[0].shape
            tensor_in = [[inputs[0]]]
            input_is_matrix = True

        kernel_shape = (Kh, Kw) = tuple(
            arg_dict["kernel_shape"])  # kernel height, width
        pads = arg_dict["pads"]
        dilations = (Dh, Dw) = tuple(arg_dict["dilations"])
        group = arg_dict["group"]
        strides = tuple(arg_dict["strides"])
        pad_shapes = (pads[0] + pads[1], pads[2] + pads[3])
        # image_shape = inputs[0][0][0].shape
        # tensor_in = inputs[0]
        weights = inputs[1]
        num_feature_maps = len(weights)
        try:
            bias = inputs[2]
        except:
            bias = np.zeros(num_feature_maps)
        batch_size = len(tensor_in)

        output_spatial_shape = np.empty((2,), dtype=int)
        for i in range(0, 2):
            output_spatial_shape[i] = np.floor(
                (image_shape[i] + pad_shapes[i] -
                 ((kernel_shape[i] - 1) * dilations[i] + 1)) / strides[i] + 1)

            # create nested list for ca symbols
        # output tensor shape: batch_size x num_feature_maps x H x W
        outputs = []
        outputs = [outputs for _ in range(num_feature_maps)]
        outputs = [outputs for _ in range(batch_size)]

        batches = tensor_in
        for id_batch, channels in enumerate(batches):

            # dilate input image
            EKh = Dh * (Kh - 1) + 1  # enlarged kernel size (height)
            EKw = Dw * (Kw - 1) + 1  # enlarged kernel size (width)

            # increment (strides)
            hh = strides[0]
            hw = strides[1]

            if arg_dict["auto_pad"] == b'SAME_UPPER' or arg_dict[
                "auto_pad"] == 'SAME_UPPER':
                """
                padding0 = hh*(image_shape[0]-output_spatial_shape[0])
                padding2 = hw*(image_shape[1]-output_spatial_shape[1])
                pad1 = ca.SX(image_shape[0],padding0)
                pad2 = ca.SX(image_shape[0],pads[1])
                pad3 = ca.SX(pads[2], image_shape[1] + padding0)
                pad4 = ca.SX(pads[3], image_shape[1] + padding2)

                pad_shapes = (pads[0]+pads[1],pads[2]+pads[3])
                """
                padding2 = image_shape[0] - output_spatial_shape[0]
                padding4 = image_shape[1] - output_spatial_shape[1]

                pad1 = ca.SX(image_shape[0], 0)
                pad2 = ca.SX(image_shape[0], padding2)
                pad3 = ca.SX(0, image_shape[1] + padding2)
                pad4 = ca.SX(padding4, image_shape[1] + padding2)
                # image_filtered = ca.SX(image_shape[0],image_shape[1])
                output_spatial_shape[0] = image_shape[0]
                output_spatial_shape[1] = image_shape[1]
                image_filtered = ca.SX(output_spatial_shape[0],
                                       output_spatial_shape[1])
            if arg_dict["auto_pad"] == "NOTSET":
                pad1 = ca.SX(image_shape[0], pads[0])
                pad2 = ca.SX(image_shape[0], pads[1])
                pad3 = ca.SX(pads[2], image_shape[1] + pad_shapes[0])
                pad4 = ca.SX(pads[3],
                             image_shape[1] + pad_shapes[0])  # pad shapes 1?
                image_filtered = ca.SX(output_spatial_shape[0],
                                       output_spatial_shape[1])

            for id_feature_map in range(0, num_feature_maps):  # len(weights)
                for i in range(0, output_spatial_shape[0]):
                    for j in range(0, output_spatial_shape[1]):

                        single_channel_filtered = []
                        for id_channel, channel in enumerate(channels):

                            channel = ca.horzcat(pad1, channel)
                            channel = ca.horzcat(channel, pad2)

                            channel = ca.vertcat(pad3, channel)
                            channel = ca.vertcat(channel, pad4)
                            x = []
                            enlarged_kernel = channel[hh * i:EKh + hh * i,
                                              hw * j:EKw + hw * j]
                            for k in range(0, Kh):
                                for l in range(0, Kw):
                                    x.append(enlarged_kernel[k * Dh, l * Dw])
                            products = []
                            for index in range(0, len(x)):
                                # products.append(x[index]*weights[id_feature_map][id_channel].elements()[index])
                                products.append(x[index] *
                                                weights[id_feature_map][
                                                    id_channel].flatten().tolist()[
                                                    index])  # TODO: remove flatten, multiply matrices elementwise
                            single_channel_filtered.append(
                                ca_elem_sum(products))
                        image_filtered[i, j] = ca_elem_sum(
                            single_channel_filtered) + bias[id_feature_map]
                outputs[id_batch][id_feature_map] = image_filtered
        if input_is_matrix:
            result = outputs[0][0]
        else:
            result = outputs

        return result


class Pool(CasadiCallWrapper):
    # onnx_op = "MaxPool"
    arg_names = ("auto_pad", "ceil_mode", "dilations",
                 "kernel_shape", "pads", "storage_order", "strides")

    arg_defaults = ("NOTSET", 0, [1, 1], "NOTSET", [0, 0, 0, 0], 0, [1, 1])

    @staticmethod
    def _impl(inputs, arg_dict):
        pass

    @staticmethod
    def pooling(inputs, arg_dict, func):
        input_is_matrix = True
        # inputs = inputs[0]
        try:
            image_shape = inputs.shape
        except:
            image_shape = inputs[0][0].shape
            # image_shape = inputs[0][0][0].shape
            input_is_matrix = False

        # image_shape = inputs.shape
        # tensor_in = [[inputs]]

        kernel_shape = (Kh, Kw) = tuple(
            arg_dict["kernel_shape"])  # kernel height, width
        pads = arg_dict["pads"]
        dilations = (Dh, Dw) = tuple(arg_dict["dilations"])
        strides = tuple(arg_dict["strides"])
        pad_shapes = (pads[0] + pads[1], pads[2] + pads[3])
        # image_shape = inputs[0][0].shape

        output_spatial_shape = np.empty((2,), dtype=int)
        if arg_dict["ceil_mode"] == 1:
            for i in range(0, 2):
                output_spatial_shape[i] = np.ceil(
                    (image_shape[i] + pad_shapes[i] -
                     ((kernel_shape[i] - 1) * dilations[i] + 1)) / strides[
                        i] + 1)
        else:
            for i in range(0, 2):
                output_spatial_shape[i] = np.floor(
                    (image_shape[i] + pad_shapes[i] -
                     ((kernel_shape[i] - 1) * dilations[i] + 1)) / strides[
                        i] + 1)

        # image = inputs[0][0]

        # flatten inputs list in list
        # images = [item for sublist in inputs for item in sublist]

        if input_is_matrix:
            batches = outputs = [[inputs]]
        else:
            batches = outputs = inputs
        for id_batch, channels in enumerate(batches):
            for id_channel, image in enumerate(channels):
                # image = image[id_channel]

                # Pad input image
                pad1 = ca.SX(image_shape[0], pads[0])
                pad2 = ca.SX(image_shape[0], pads[1])
                image = ca.horzcat(pad1, image)
                image = ca.horzcat(image, pad2)

                pad3 = ca.SX(pads[2], image_shape[0] + pad_shapes[0])
                pad4 = ca.SX(pads[3], image_shape[0] + pad_shapes[0])
                image = ca.vertcat(pad3, image)
                image = ca.vertcat(image, pad4)

                # dilate input image
                EKh = Dh * (Kh - 1) + 1  # enlarged kernel size (height)
                EKw = Dw * (Kw - 1) + 1  # enlarged kernel size (width)

                # increment (strides)
                hh = strides[0]
                hw = strides[1]

                image_filtered = ca.SX(output_spatial_shape[0],
                                       output_spatial_shape[1])

                for i in range(0, output_spatial_shape[0]):
                    for j in range(0, output_spatial_shape[1]):
                        enlarged_kernel = image[hh * i:EKh + hh * i,
                                          hw * j:EKw + hw * j]
                        x = []
                        for k in range(0, Kh):
                            for l in range(0, Kw):
                                x.append(enlarged_kernel[k * Dh, l * Dw])
                        image_filtered[i, j] = func(x)
                outputs[id_batch][id_channel] = image_filtered
        if input_is_matrix:
            result = outputs[0][0]
        else:
            result = outputs
        return result

        # test = ca.fmax(test0)

        '''
        output_spatial_shape[0]=2
        output_spatial_shape[1]=2
        # result = np.empty((image_batchsize, image_channels,kernel_height, kernel_width))
        result = np.empty((image_batchsize, image_channels,
                           int(output_spatial_shape[0]),
                           int(output_spatial_shape[1])))
        image = np.pad(inputs, ((pads[0],pads[1]),(pads[2],pads[3])), ca.fmax) #pads
        result[0, 0] = block_reduce(image, block_size, np.max)
        for i in range(0, image_batchsize):
            for j in range(0, image_channels):
                test = inputs[i,j]
                image = np.pad(test, ((pads[0],pads[1]),(pads[2],pads[3])), 'mean') #pads
                result[i, j] = block_reduce(image, block_size, np.max)
        '''


class MaxPool(Pool):
    onnx_op = "MaxPool"

    @staticmethod
    def _impl(inputs, arg_dict):
        # TODO
        # inputs = inputs[0]
        inputs = inputs
        return Pool.pooling(inputs, arg_dict, ca_max)


class AveragePool(Pool):
    onnx_op = "AveragePool"

    @staticmethod
    def _impl(inputs, arg_dict):
        return Pool.pooling(inputs, arg_dict, ca_mean)


class Tanh(CasadiCallWrapper):
    onnx_op = "Tanh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.tanh(*inputs)


class Sinh(CasadiCallWrapper):
    onnx_op = "Sinh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.sinh(*inputs)


class Cosh(CasadiCallWrapper):
    onnx_op = "Cosh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.cosh(*inputs)


class Tan(CasadiCallWrapper):
    onnx_op = "Tan"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.tan(*inputs)


class Sin(CasadiCallWrapper):
    onnx_op = "Sin"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.sin(*inputs)


class Cos(CasadiCallWrapper):
    onnx_op = "Cos"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.cos(*inputs)


class Atan(CasadiCallWrapper):
    onnx_op = "Atan"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.atan(*inputs)


class Asin(CasadiCallWrapper):
    onnx_op = "Asin"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.asin(*inputs)


class Acos(CasadiCallWrapper):
    onnx_op = "Acos"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.acos(*inputs)


class Atanh(CasadiCallWrapper):
    onnx_op = "Atanh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.atanh(*inputs)


class Asinh(CasadiCallWrapper):
    onnx_op = "Asinh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.asinh(*inputs)


class Acosh(CasadiCallWrapper):
    onnx_op = "Acosh"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.acosh(*inputs)


class Transpose(CasadiCallWrapper):
    onnx_op = "Transpose"
    arg_names = ("perm",)

    @staticmethod
    def _impl(inputs, arg_dict):
        assert arg_dict["perm"] == [1, 0]
        return ca.transpose(*inputs)


class Constant(CasadiCallWrapper):
    onnx_op = "Constant"
    arg_names = ("value",)

    @staticmethod
    def _impl(inputs, arg_dict):
        c = extract_constant(arg_dict["value"])
        return c


class Abs(CasadiCallWrapper):
    onnx_op = "Abs"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.fabs(*inputs)


class Pow(CasadiCallWrapper):
    onnx_op = "Pow"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        base = inputs[0]
        exponent = inputs[1]
        return ca.power(base, exponent)


class Sqrt(CasadiCallWrapper):
    onnx_op = "Sqrt"
    arg_names = tuple()

    @staticmethod
    def _impl(inputs, arg_dict):
        return ca.sqrt(*inputs)


def _build_map() -> dict:
    """
    Build map of opclasses of CasadiCallWrapper as keys for functions
    """
    op_map = {}
    cls_members = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for name, cls in cls_members:
        if issubclass(cls, CasadiCallWrapper):
            op_map[cls.onnx_op] = cls.eval
    return op_map


onnx_casadi_map = _build_map()
