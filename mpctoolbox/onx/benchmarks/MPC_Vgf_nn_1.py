"""
Model predictive control of a vgf model 
"""
import numpy as np
import casadi as ca
import matplotlib.pyplot as plt
from mpctoolbox.core.mpc import MPCSolver
from mpctoolbox.core.system import System_onnx
from mpctoolbox.core.ocp import OptimalControlProblem

def create_system():
    # Set the system name.
    name = 'vgf_system'

    # Try to load system from "systems/sys_cache.dat".


    # Set dimensions.
    x_dim = 5   # dimension of system state
    u_dim = 5  # dimension of system input

    # CasADi symbolic variables (only CasADi SX and MX symbolics are supported)
    # Note: MX symbolics are preferable for saving a string representation of
    # the system's rhs via System.save(). Regardless, SX symbolics will be used
    # internally for all future calculations.
    x = ca.MX.sym('x', x_dim)   # symbolic variable for state
    u = ca.MX.sym('u', u_dim)   # symbolic variable for input


    # Collect all symbolic variables in dictionary sym_dict.
    # This dictionary must contain entries 'x' and 'u'.
    # Additional entries with user-defined keys will be treated as parameters.
    sym_dict = {'x': x, 'u': u}

    sxup_dict = {'name': name,
                'x_0': 'Temperatur zone 0', 'x_1': 'Temperatur zone 1',
                'x_2': 'Temperatur zone 2', 'x_3': 'Temperatur zone 3',
                'x_4': 'Temperatur zone 4', 
                'u_0': 'Power heater 1', 'u_1': 'Power heater 2',
                'u_2': 'Power heater 3', 'u_2': 'Power heater 4',
                'u_4': 'Power heater 5',
                }

    model_path = "mpctoolbox/test_models/first_vgf.onnx"
    sys = System_onnx(model_path, sym_dict, sxup_dict=sxup_dict)
    sys.save(name)
    return sys


def create_ocp(sys):
    # Set the ocp mode and name.
    mode = 'free'
    name = sys.sxup_dict['name'] + '_' + mode

    # Set initial system state and previous system input.
    x_0 = np.array([-3.776103783597248, -3.659223465285365, -3.673746156779485, -3.532503753088541, -2.117618426301434]).reshape((1,5))
    uprev = np.array([-2.2750267750208293, -2.5651907989735485, -2.492147866410402, -2.1839805605610434, -2.588265055564575]).reshape((1,5))

    x_ref = x_ref = np.array([-2, -2, -2, -1.5, -2]).reshape((1,5))
    #x_ref = np.array([0.6010896863118668, 0.2684460612685669, 0.2451869278317246, 0.18648712173995194, -0.3129775908077039]).reshape((1,5))
    u_ref = np.zeros((1, sys.n_u))
    #x_ref = np.array([0, 0, 0, 0, 0]).reshape((1,5))
    # Specify time grid for simulation and discretization via time step size T
    # and final time in seconds t_f.
    T = 0.05
    t_f = 7
    tgrid = np.linspace(0, t_f, int(t_f/T)+1)
    assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T

    # Set lower bounds for state 'x', input 'u' and change of input 'Du'.
    lb = {'x': np.array([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf]),
          'u':  np.array([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf]),
           'Du': np.array([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf])}
    # Set upper bounds for state 'x', input 'u' and change of input 'Du'.
    ub = {'x': np.array([np.inf, np.inf,np.inf, np.inf,np.inf]),
          'u': np.array([np.inf, np.inf,np.inf, np.inf,np.inf]),
          'Du': np.array([np.inf, np.inf,np.inf, np.inf,np.inf])}
    Q = np.eye(5)
    #Q[5,5] = 2.5
    # Create OptimalControlProblem object. See constructor docstring for
    # details.
    ocp = OptimalControlProblem(sys, mode, x_0, tgrid, x_ref, u_ref,
                                kwargs={'lb': lb, 'ub': ub, 'uprev': uprev, 'Q': Q})

    # Save the created ocp to the system cache file "ocps/ocp_cache.dat".
    # This cache file will contain all saved ocps. The filenames of all
    # saved ocps will be listed in "ocps/ocp_filenames.txt".
    # Note: Custom CasADi function objects are saved as string representation
    # due to problems saving SwigPyCasadiObject objects on Windows.
    # Unfortunately, this currently works only for certain MX expressions.
    ocp.save(name)

    return ocp


def create_solver(ocp):
    # Specify keyword arguments of MPCSolver constructor.
    kwargs = {'N':           3, #3
              'receding':   'unltd',
              'u_guess':    None,
              'x_guess':    None,
              'verbosity':  0}

    # Create MPCSolver object. See constructor docstring for details.
    solver = MPCSolver(ocp, **kwargs)

    return solver


def print_results(sol):

    print('{:<20} {:<10} {:<10}'.format('', 'J_f', 'e_f'))
    print(' {:<10.1f} {:<10.1e}'.format(sol['J_f'], sol['e_f']))


def plot_results(sol):

 
    # time vector
    T = 0.05
    t_f = 7
    tt = np.arange(0, t_f + T , T)
    tt = tt.reshape((tt.size,1))

    # simulation system = real system
    xx = sol['x_sim']
    uu = sol['u_sim']
    
    plt.subplot(5, 2, 1)
    plt.plot(tt,xx[:,0])
    plt.xlabel('Zeit')
    plt.ylabel('Temperatur')
    plt.grid()

    plt.subplot(5, 2, 2)
    plt.plot(tt,xx[:,1])
    plt.xlabel('Zeit')
    plt.ylabel('Temperatur')
    plt.grid()

    plt.subplot(5, 2, 3)
    plt.plot(tt,xx[:,2])
    plt.xlabel('Zeit')
    plt.ylabel('Temperatur')
    plt.grid()

    plt.subplot(5, 2, 4)
    plt.plot(tt,xx[:,3])
    plt.xlabel('Zeit')
    plt.ylabel('Temperatur')
    plt.grid()

    plt.subplot(5, 2, 5)
    plt.plot(tt,xx[:,4])
    plt.xlabel('Zeit')
    plt.ylabel('Temperatur')
    plt.grid()

    tt = np.arange(T, t_f+T  , T)
    tt = tt.reshape((tt.size,1))
    plt.subplot(5, 2, 6)
    plt.step(tt,uu[:,0])
    plt.xlabel('Zeit')
    plt.ylabel('Leistung')
    plt.grid()

    plt.subplot(5, 2, 7)
    plt.step(tt,uu[:,1])
    plt.xlabel('Zeit')
    plt.ylabel('Leistung')
    plt.grid()

    plt.subplot(5, 2, 8)
    plt.step(tt,uu[:,2])
    plt.xlabel('Zeit')
    plt.ylabel('Leistung')
    plt.grid()

    plt.subplot(5, 2, 9)
    plt.step(tt,uu[:,3])
    plt.xlabel('Zeit')
    plt.ylabel('Leistung')
    plt.grid()

    plt.subplot(5, 2, 10)
    plt.step(tt,uu[:,4])
    plt.xlabel('Zeit')
    plt.ylabel('Leistung')
    plt.grid()

    plt.show()

def main():

    # Create system, optimal control problem (ocp) and solver.
    sys = create_system()
    ocp = create_ocp(sys)
    solver = create_solver(ocp)
    sol = solver.solve()

    # Show results.
    print_results(sol)
    plot_results(sol)

if __name__ == '__main__':
    main()
