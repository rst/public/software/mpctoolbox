
import netron
import onnx

'''

    path(str):                  Path to the .onnx file.
    input_tuple:                list of tuples (input_name_n,input_shape_n). order matters!
    input_transform:            how input data were prepared for training.
    input_means:                
    input_variances:
    output_tuple:               list of tuples (output_name_n,output_shape_n). oder matters!
    output_transform:           how output data were prepared for training.
    output_means:                
    output_variances:
    max_pred_horizon:           number of steps for which the model predictions are useable.
    input_data_range:           values ranges of inputdata used for training

'''


meta_data = {
    "input_tuple":              "[ ('x', 6), ('u', 2) ]",
    "input_transform":          "none",
    "input_means":              "[[-1,0],[1]]",
    "input_variances":          "[[1,1],[2]]",
    "output_tuple":             "[ ('x_dot', 6)]",
    "output_transform":         "none",
    "output_means":             "[[1,-1]]",  
    "output_variances":         "[[1,0.1]]",
    "max_pred_horizon":         "dummy",        
    "input_data_restrictions":  "dummy"
    }

#path = 'mpctoolbox/test_models/pytorch.onnx'
#path = 'mpctoolbox/test_models/new.onnx'
#path = 'mpctoolbox/test_models/coil.onnx'
#path = 'mpctoolbox/test_models/complex.onnx'
#path = 'mpctoolbox/test_models/mnist_classification.onnx'
#path = 'mpctoolbox/test_models/spring_mass.onnx'
#path = 'mpctoolbox/test_models/spring_mass_transf.onnx'
#path = 'mpctoolbox/test_models/vgf.onnx'
#path = 'mpctoolbox/test_models/test.onnx'
#path = 'mpctoolbox/test_models/second_vgf.onnx'
#-----------------
#netron.start(path)

#model = onnx.load_model(path)
#model.graph.output[0].name = "x_dot"
#model.graph.node[-1].output[0] = "x_dot"
#onnx.helper.set_model_props(model, meta_data)
#onnx.save_model(model, path) 
#netron.start(path)

