import os
os.environ['TF_KERAS'] = '1'
import keras

from tensorflow.python.keras.models import load_model
import onnx
import keras2onnx
import netron

netron.start("mpctoolbox/test_models/spring_mass.onnx")
path_keras = "mpctoolbox/test_models/second_vgf.h5"
path_onnx = "mpctoolbox/test_models/second_vgf.onnx"
model = load_model(path_keras)
onnx_model = keras2onnx.convert_keras(model,"second_vgf.onnx")
onnx.save_model(onnx_model,"second_vgf.onnx")