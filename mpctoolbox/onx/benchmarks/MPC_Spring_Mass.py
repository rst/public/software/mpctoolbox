"""
TODO
"""
import numpy as np
import casadi as ca
import matplotlib.pyplot as plt

from mpctoolbox.core.mpc import MPCSolver
from mpctoolbox.core.system import System, System_onnx
from mpctoolbox.core.ocp import OptimalControlProblem
#from mpctoolbox.onnx_import import import_from_onnx


def create_system_analytic():
    name = 'spring_mass_analytic'

    x_dim = 2  # dimension of system state
    u_dim = 1  # dimension of system input

    x = ca.SX.sym('x', x_dim)  # symbolic variable for state
    u = ca.SX.sym('u', u_dim)  # symbolic variable for input

    sym_dict = {'x': x, 'u': u}

    sxup_dict = {'name': name,
                 'x_0': 'position', 'x_1': 'velocity',
                 'u_0': 'acceleration',
                 }

    x_dot_p = [x[1], u[0] - 1 * x[0] - 0.1 * x[1]]
    x_dot_p = ca.vertcat(*x_dot_p)
    sys = System(x_dot_p, sym_dict, sxup_dict=sxup_dict)
    sys.save(name)
    return sys


def create_system_nn():
    name = 'spring_mass_nn'

    x_dim = 2  # dimension of system state
    u_dim = 1  # dimension of system input

    x = ca.SX.sym('x', x_dim)  # symbolic variable for state
    u = ca.SX.sym('u', u_dim)  # symbolic variable for input

    sym_dict = {'x': x, 'u': u}

    sxup_dict = {'name': name,
                 'x_0': 'position', 'x_1': 'velocity',
                 'u_0': 'acceleration',
                 }


    model_path = "mpctoolbox/test_models/spring_mass.onnx"
    sys = System_onnx(model_path, sym_dict, sxup_dict=sxup_dict)
    sys.save(name)
    return sys


def create_ocps():
    # Set the ocp mode and name.
    mode = 'xN'
    name = sys.sxup_dict['name'] + '_' + mode

    # Try to load ocp from "ocps/sys_cache.dat".
    # try:
    #    ocp = OptimalControlProblem.load(name)
    #    return ocp
    # except (KeyError, FileNotFoundError):
    #    pass

    # Set initial system state and previous system input.
    x_0 = np.array([0, 0])
    uprev = np.zeros(sys.n_u)

    # Set constant state and input reference.
    x_ref = np.array([1, 0]).reshape((1, 2))
    u_ref = np.ones((1, sys.n_u))

    # Specify time grid for simulation and discretization via time step size T
    # and final time in seconds t_f.
    T = 0.04
    t_f = 20
    tgrid = np.linspace(0, t_f, int(t_f / T) + 1)
    assert tgrid[-1] == t_f and tgrid[1] - tgrid[0] == T

    # Set lower bounds for state 'x', input 'u' and change of input 'Du'.
    lb = {'x': np.array([-np.inf, -10]),
          'u': -2,
          'Du': -0.25}
    # Set upper bounds for state 'x', input 'u' and change of input 'Du'.
    ub = {'x': np.array([np.inf, 10]),
          'u': 2,
          'Du': 0.25}

    # Create OptimalControlProblem object. See constructor docstring for
    # details.
    ocp_sim = OptimalControlProblem(sys, mode, x_0, tgrid, x_ref, u_ref,
                                kwargs={'lb': lb, 'ub': ub, 'uprev': uprev})
    ocp = OptimalControlProblem(sys_nn, mode, x_0, tgrid, x_ref, u_ref,
                                    kwargs={'lb': lb, 'ub': ub, 'uprev': uprev})
    # Save the created ocp to the system cache file "ocps/ocp_cache.dat".
    # This cache file will contain all saved ocps. The filenames of all
    # saved ocps will be listed in "ocps/ocp_filenames.txt".
    # Note: Custom CasADi function objects are saved as string representation
    # due to problems saving SwigPyCasadiObject objects on Windows.
    # Unfortunately, this currently works only for certain MX expressions.
    ocp.save(name)

    return [ocp, ocp_sim]


def create_solver():
    # Specify keyword arguments of MPCSolver constructor.
    kwargs = {'N': 500,
              'receding': 'unltd', #unltd
              'u_guess': None,
              'x_guess': None,
              'verbosity': 0}

    # Create MPCSolver object. See constructor docstring for details.
    solver_ideal = MPCSolver(ocp_sim, **kwargs)
    solver_real = MPCSolver(ocp, ocp_sim, **kwargs)

    return [solver_ideal,solver_real]


def plot_results():

 
    # time vector
    T = 0.04
    tt = np.arange(0, 20 + T , T)
    tt = tt.reshape((tt.size,1))

    # simulation system = real system
    xx_ideal = sol_ideal['x_sim']
    uu_ideal = sol_ideal['u_sim']
    uu_ideal = np.insert(uu_ideal, 0, 0)
    
    # simulation system != real system
    xx_real = sol_real['x_sim']
    uu_real = sol_real['u_sim']
    uu_real = np.insert(uu_real, 0, 0)

    fehler_u = 0
    fehler_x1 = 0
    fehler_x2 = 0
    for i in range(uu_real.size):
        fehler_u = fehler_u + np.abs(uu_ideal[i]-uu_real[i])
        fehler_x1 = fehler_x1 + np.abs(xx_ideal[i,0]-xx_real[i,0])
        fehler_x2 = fehler_x2 + np.abs(xx_ideal[i,1]-xx_real[i,0])
    print("Fehler u:", fehler_u)
    print("Fehler x1:",fehler_x1)
    print("Fehler x2:",fehler_x1)
    plt.rcParams["figure.figsize"] = (18/2.54,12/2.54)
    plt.subplot(1, 1, 1)
    plt.plot(tt,xx_ideal[:,0], label = "Ideal")
    plt.plot(tt,xx_real[:,0], label = "KNN", linestyle='-.')
    #plt.plot(tt,xx_ideal[:,0]-xx_real[:,0], label = "Differenz")
    #plt.plot(tt,x1-x1_knn, label = "Abweichung")
    plt.legend(loc='lower right')
    plt.title('MPC-Regelung eines Feder-Masse-Systems')
    plt.ylabel('x1 ')
    plt.grid()
    '''
    plt.subplot(3, 1, 2)
    plt.plot(tt,xx_ideal[:,1],label = "Ideal")
    plt.plot(tt,xx_real[:,1],label = "KNN", linestyle='-.')
    #plt.plot(tt,xx_ideal[:,1]-xx_real[:,1], label = "Differenz")
    #plt.plot(tt,x1-x1_knn, label = "Abweichung")
    plt.ylabel('x2 in m/s')
    plt.legend(loc='upper right')
    plt.grid()

    plt.subplot(3, 1, 3)
    plt.step(tt,uu_ideal, label = "Ideal")
    plt.step(tt,uu_real, label = "KNN", linestyle='-.')
    #plt.plot(tt,uu_ideal-uu_real, label = "Differenz")
    plt.xlabel('Zeit in s')
    plt.ylabel('u in m/s$^2$')
    plt.legend(loc='lower right')
    plt.grid()'''
    
    plt.show()

def print_results():

    print('{:<20} {:<10} {:<10}'.format('Ideal', 'J_f', 'e_f'))
    print(' {:<10.1f} {:<10.1e}'.format(sol_ideal['J_f'], sol_ideal['e_f']))

    print('{:<20} {:<10} {:<10}'.format('Real', 'J_f', 'e_f'))
    print(' {:<10.1f} {:<10.1e}'.format(sol_real['J_f'], sol_real['e_f']))

if __name__ == '__main__':
    sys = create_system_analytic()
    sys_nn = create_system_nn()
    # for sys in [sys_nn,sys_analytic]:
    [ocp, ocp_sim] = create_ocps()
    [solver_ideal,solver_real] = create_solver()
    sol_ideal = solver_ideal.solve()
    sol_real = solver_real.solve()

    print_results()
    plot_results()
