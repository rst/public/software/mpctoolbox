"""
Methods to adapt input and output structures of ANNs to generic standard.
"""
from ast import literal_eval
import onnx


def get_meta_dict(model):
    metadata = model.metadata_props
    meta_dict = {}
    for i in range(len(metadata)):
        new_key = metadata[i].key
        new_value = metadata[i].value
        meta_dict[new_key] = new_value
    return meta_dict


def prepare_inputs(model):
    """
    Is the model prepared?
    - Yes -> output_new_node = model_inputs[0].name node
        new_input_by_onnx_preparation
    - No -> output_new_node = model_inputs[0].name

    Args:
        model:

    Returns:

    """
    nodes = model.graph.node
    model_inputs = model.graph.input

    # delete new model inputs and new concat nodes
    if nodes[0].name == "new_input_by_onnx_preparation":
        output_new_node = nodes[0].output[0]
    else:
        output_new_node = model_inputs[0].name

    while True:
        if nodes[0].name == "new_input_by_onnx_preparation":
            nodes.remove(nodes[0])
        else:
            break
    while True:
        if len(model_inputs) > 0:
            model_inputs.remove(model_inputs[0])
        else:
            break

    meta_dict = get_meta_dict(model)
    new_model_inputs = []
    new_model_input_names = []
    input_tuples = literal_eval(meta_dict["input_tuple"])
    for new_input in input_tuples:
        value_info = onnx.helper.make_tensor_value_info(new_input[0],
                                                        onnx.TensorProto.FLOAT,
                                                        [1, new_input[1]])
        new_model_inputs.append(value_info)
        new_model_input_names.append(new_input[0])

    # pow onnx op consists of constant and pow.
    # constant has no inputs
    for i in range(0, len(nodes)):
        if bool(nodes[i].input):
            nodes[i].input[0] = output_new_node
            break

    input_node = onnx.helper.make_node(
        op_type="Concat",
        inputs=new_model_input_names,
        outputs=[output_new_node],
        name='new_input_by_onnx_preparation',
        axis=-1
    )
    nodes.insert(0, input_node)

    for new_model_input in new_model_inputs:
        model_inputs.append(new_model_input)

    return model


def prepare_outputs(model):
    model_outputs = model.graph.output
    nodes = model.graph.node
    meta_dict = get_meta_dict(model)

    # delete new split nodes and model outputs
    while True:
        if nodes[-1].name == "new_output_by_onnx_preparation":
            nodes.remove(nodes[-1])
        else:
            break
    while True:
        if len(model_outputs) > 0:
            model_outputs.remove(model_outputs[0])
        else:
            break

    new_model_outputs = []
    new_model_output_names = []
    new_model_output_dimensions = []
    output_tuples = literal_eval(meta_dict["output_tuple"])
    for new_output in output_tuples:
        new_model_outputs.append(
            onnx.helper.make_tensor_value_info(new_output[0],
                                               onnx.TensorProto.FLOAT,
                                               [1, new_output[1]]))
        new_model_output_names.append(new_output[0])
        new_model_output_dimensions.append(new_output[1])

    if len(new_model_outputs) > 1:
        nodes[-1].output[0] = "result_to_split"
        output_node = onnx.helper.make_node(
            op_type="Split",
            inputs=["result_to_split"],
            outputs=new_model_output_names,
            name='new_output_by_onnx_preparation',
            axis=-1,
            split=new_model_output_dimensions
        )
        nodes.append(output_node)
    else:
        nodes[-1].output[0] = new_model_outputs[0].name

    for new_model_output in new_model_outputs:
        model_outputs.append(new_model_output)

    return model


def adapt(model):
    model = prepare_inputs(model)
    model = prepare_outputs(model)
    return model
