import os
import random
import unittest

import casadi as ca
import numpy as np

import onnx
import netron
import torch

from onx.tests import rnd_matrix, random_sequence
from mpctoolbox.onx.parser import _build_expr, MatMul, import_from_onnx
from mpctoolbox.onx.model_import import get_system


class OperationsTestCase(unittest.TestCase):

    @unittest.skip("This test should be removed")
    def test_Dropout(self):
        # x = [ca.SX.sym("test",2,2), ca.SX.sym("test2",2,2)]
        # y = ca.fmax(*x,0)
        # a = 0
        # b = ca.SX.ones(2,2)
        # result = ca.fmax(a,b)
        A = rnd_matrix()
        A = [round(x, 5) for x in A.flatten('F').tolist()]

        args = (onnx.helper.make_attribute("ratio", 0.5),)
        res = _build_expr("Dropout", A, args)
        self.assertSequenceEqual(A, res)
        raise AssertionError("This test is nonesense")

    @unittest.skip("skip")
    def test_MaxPool(self):
        # x1 = np.array([[[
        #    [1, 2, 3, 4],
        #    [5, 6, 7, 8],
        #    [9, 10, 11, 12],
        #    [13, 14, 15, 16],
        # ]]]).astype(np.float32)
        x1 = ca.SX.eye(5)  # shape 5x5 -> 1x1x5x5
        x2 = [[x1, x1]]
        # x1 = ca.SX.eye(5,5,3,2)
        y1 = np.array([
            [1, 1],
            [1, 1]]).astype(np.float32)
        y2 = [[y1, y1]]

        args = (onnx.helper.make_attribute("kernel_shape", (2, 2)),
                onnx.helper.make_attribute("pads", (0, 0, 0, 0)),
                onnx.helper.make_attribute("strides", (2, 2)),
                onnx.helper.make_attribute("dilations", (2, 2)))
        res = _build_expr("MaxPool", x1, args)
        self.assertSequenceEqual(res.elements(), y1.flatten().tolist())

        res = _build_expr("MaxPool", x2, args)
        self.assertSequenceEqual(res[0][0].elements(),
                                 y2[0][0].flatten().tolist())

    @unittest.skip("skip")
    def test_AveragePool(self):
        # x1 = np.array([[[
        #    [1, 2, 3, 4],
        #    [5, 6, 7, 8],
        #    [9, 10, 11, 12],
        #    [13, 14, 15, 16],
        # ]]]).astype(np.float32)
        x1 = ca.SX.eye(5)  # shape 5x5 -> 1x1x5x5
        # x1 = ca.SX.sym("x1",5,5,3,2)
        # x1 = ca.SX.eye(5,5,3,2)
        y1 = np.array([
            [0.5, 0.25],
            [0.25, 0.5]]).astype(np.float32)

        args = (onnx.helper.make_attribute("kernel_shape", (2, 2)),
                onnx.helper.make_attribute("pads", (0, 0, 0, 0)),
                onnx.helper.make_attribute("strides", (2, 2)),
                onnx.helper.make_attribute("dilations", (2, 2)))
        res = _build_expr("AveragePool", x1, args)
        self.assertSequenceEqual(res.elements(), y1.flatten().tolist())

    def test_Conv(self):
        # ]]]).astype(np.float32)
        # x1 = [[ca.SX.eye(5),ca.SX.eye(5),ca.SX.eye(5)],[ca.SX.eye(5),ca.SX.eye(5),ca.SX.eye(5)]]
        # x1 = [[ca.SX.eye(5),ca.SX.eye(5)],[ca.SX.eye(5),ca.SX.eye(5)]] # shape 2x2x5x5
        x1 = ca.SX.eye(5)  # shape 5x5 -> 1x1x5x5
        w = np.ones((1, 1, 2, 2))
        # bias = ca.SX.ones(3)
        bias = np.ones(1)
        # w = [[ca.SX.ones(2,2),ca.SX.ones(2,2)],[ca.SX.ones(2,2),ca.SX.ones(2,2)],[ca.SX.ones(2,2),ca.SX.ones(2,2)]] # shape 3x2x2x2

        y1 = np.array([
            [3, 2],
            [2, 3]]).astype(np.float32)

        args = (onnx.helper.make_attribute("kernel_shape", (2, 2)),
                onnx.helper.make_attribute("pads", (0, 0, 0, 0)),
                onnx.helper.make_attribute("strides", (2, 2)),
                onnx.helper.make_attribute("dilations", (2, 2)))
        res = _build_expr("Conv", [x1, w, bias], args)

        # flatten nested results
        # res = [item for sublist in res for item in sublist]
        self.assertSequenceEqual(res.elements(), y1.flatten().tolist())

        # x2 = [[ca.SX.eye(5),ca.SX.eye(5)],[ca.SX.eye(5),ca.SX.eye(5)]] # shape 2x2x5x5
        args = (onnx.helper.make_attribute("kernel_shape", (2, 2)),
                onnx.helper.make_attribute("auto_pad", "SAME_UPPER"),
                onnx.helper.make_attribute("pads", (0, 0, 0, 0)),
                onnx.helper.make_attribute("strides", (1, 1)),
                onnx.helper.make_attribute("dilations", (1, 1)))
        res = _build_expr("Conv", [x1, w, bias], args)
        y2 = np.array([
            [3, 2, 1, 1, 1],
            [2, 3, 2, 1, 1],
            [1, 2, 3, 2, 1],
            [1, 1, 2, 3, 2],
            [1, 1, 1, 2, 2],
        ]).astype(np.float32)
        self.assertSequenceEqual(res.elements(), y2.flatten().tolist())

    def test_Gemm(self):
        A = rnd_matrix()
        B = rnd_matrix()
        C = rnd_matrix()
        inputs = (A, B, C)
        alpha = random.random()
        beta = random.random()
        TransA = random.randint(0, 1)
        TransB = random.randint(0, 1)

        args = (onnx.helper.make_attribute('alpha', alpha),
                onnx.helper.make_attribute('beta', beta),
                onnx.helper.make_attribute('TransA', TransA),
                onnx.helper.make_attribute('TransB', TransB))
        res = _build_expr("Gemm", inputs, args)
        exp = [round(x, 5) for x in
               (alpha * A @ B + beta * C).flatten('F').tolist()]
        res = [round(x, 5) for x in res.flatten('F').tolist()]
        self.assertSequenceEqual(res, exp)

    def test_MatMul(self):
        A = rnd_matrix()
        B = rnd_matrix()
        inputs = (A, B)
        arg_dict = MatMul._get_argument_dict
        res = MatMul._impl(inputs, arg_dict)
        self.assertSequenceEqual(res.tolist(), (A @ B).tolist())

        inputs = (B, A)
        arg_dict = MatMul._get_argument_dict
        res = MatMul._impl(inputs, arg_dict)
        self.assertSequenceEqual(res.tolist(), (B @ A).tolist())

    def test_Relu(self):
        # r = random_sequence(4)
        A = rnd_matrix(shape=(5, 7), mult=1)
        B = rnd_matrix(shape=(5, 7), mult=-1)

        res = _build_expr("Relu", [A], None)
        self.assertSequenceEqual(res.elements(), A.flatten('F').tolist())
        res = _build_expr("Relu", [B], None)
        self.assertSequenceEqual(res.elements(), np.zeros((35,)).tolist())

    def test_Reshape(self):
        # r = random_sequence(4)
        A = rnd_matrix(shape=(5, 5), mult=1)
        A = [[A, A]]  # shape 1x2x5x5
        b = [1, 50]
        res = _build_expr("Reshape", (A, b), None)

        # self.assertSequenceEqual(res.elements(), A.flatten('F').tolist())

    def test_Add(self):
        a = random.random()
        b = random.random()
        inputs = (a, b)
        expr = _build_expr("Add", inputs, None)
        self.assertEqual(expr, a + b)
        inputs = (b, a)
        expr = _build_expr("Add", inputs, None)

        self.assertEqual(expr, a + b)
        A = rnd_matrix()
        B = rnd_matrix()
        inputs = (A, B)
        expr = _build_expr("Add", inputs, None)

        self.assertEqual(expr.tolist(), (A + B).tolist())
        inputs = (B, A)
        expr = _build_expr("Add", inputs, None)
        self.assertEqual(expr.tolist(), (A + B).tolist())

    def test_Abs(self):
        A = rnd_matrix(shape=(10,))
        B = A * (-1)
        res = _build_expr("Abs", [B], None)
        self.assertSequenceEqual(res.elements(), A.tolist())
        res = _build_expr("Abs", [A], None)
        self.assertSequenceEqual(res.elements(), A.tolist())

    def test_Tanh(self):
        A = rnd_matrix()
        exp = np.tanh(A)
        res = _build_expr("Tanh", [A], None)

        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Sinh(self):
        A = rnd_matrix()
        exp = np.sinh(A)
        res = _build_expr("Sinh", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Cosh(self):
        A = rnd_matrix()
        exp = np.cosh(A)
        res = _build_expr("Cosh", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Tan(self):
        A = rnd_matrix()
        exp = np.tan(A)
        res = _build_expr("Tan", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Sin(self):
        A = rnd_matrix()
        exp = np.sin(A)
        res = _build_expr("Sin", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Cos(self):
        A = rnd_matrix()
        exp = np.cos(A)
        res = _build_expr("Cos", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Atanh(self):
        A = rnd_matrix()
        exp = np.arctanh(A)
        res = _build_expr("Atanh", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Asinh(self):
        A = rnd_matrix()
        exp = np.arcsinh(A)
        res = _build_expr("Asinh", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Acosh(self):
        A = rnd_matrix(offset=1)
        exp = np.arccosh(A)
        res = _build_expr("Acosh", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Atan(self):
        A = rnd_matrix()
        exp = np.arctan(A)
        res = _build_expr("Atan", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Asin(self):
        A = rnd_matrix()
        exp = np.arcsin(A)
        res = _build_expr("Asin", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Acos(self):
        A = rnd_matrix()
        exp = np.arccos(A)
        res = _build_expr("Acos", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Pow(self):
        A = rnd_matrix()
        b = random.uniform(0, 10)
        inputs = (A, b)
        exp = np.power(A, b)
        res = _build_expr("Pow", inputs, None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)

    def test_Sqrt(self):
        A = rnd_matrix()
        exp = np.sqrt(A)
        res = _build_expr("Sqrt", [A], None)
        exp = [round(x, 5) for x in exp.flatten('F').tolist()]
        res = [round(x, 5) for x in res.elements()]
        self.assertSequenceEqual(exp, res)


class ImportTestCase(unittest.TestCase):

    def test_pytorch_model(self):

        class TestNet(torch.nn.Module):
            """ Simple pytorch model """
            def __init__(self):
                super(TestNet, self).__init__()
                self.l1 = torch.nn.Linear(3, 3)
                self.l2 = torch.nn.Linear(3, 3)

            def forward(self, x):
                x = self.l1(x)
                x = torch.abs(x)
                x = torch.tanh(x)
                x = self.l2(x)
                x = torch.relu(x)
                return x

        # Build the model
        test_net = TestNet()
        test_input = torch.randn(3, 1).T
        test_output = test_net(test_input)

        # Export the model
        model_path = get_model_path("torch_model.onnx")
        torch.onnx.export(
            test_net,
            test_input,
            model_path,
            export_params=True,
            opset_version=10,
            do_constant_folding=True,
            input_names=("x", ),
            output_names=("x_dot",),
        )

        # Import the model
        onnx_model = onnx.load(model_path)
        ca_inputs, ca_rhs = import_from_onnx(onnx_model)

        # Apply the same input
        test_input_np = np.array(test_input.tolist())
        res = ca.substitute(ca_rhs[0], ca_inputs[0], test_input_np)

        # Check the output
        test_output_np = np.array(test_output.tolist())
        for idx, val in enumerate(test_output_np.flatten()):
            self.assertAlmostEqual(val, float(res[idx]))

    @unittest.skip("TF not available fpr python 3.9 yet")
    def test_keras_model(self):
        import tensorflow as tf
        from tensorflow.keras.layers import Dense
        from tensorflow.keras import Model

        class TestModel(Model):
            def __init__(self):
                super(TestModel, self).__init__()
                self.d1 = Dense(3, activation='relu')
                self.d2 = Dense(30, activation='tanh')
                self.d3 = Dense(3, activation='tanh')

        def call(self, x):
            x = self.d1(x)
            x = self.d2(x)
            x = self.d4(x)
            return x

        # Build the model
        test_net = TestModel()

        test_input = torch.randn(3, 1).T
        test_output = test_net(test_input)

        # Export the model
        model_path = get_model_path("keras_model.onnx")

        # TODO complete this test

        # # Import the model
        # onnx_model = onnx.load(model_name)
        # ca_inputs, ca_rhs = import_from_onnx(onnx_model)
        #
        # # Apply the same input
        # test_input_np = np.array(test_input.tolist())
        # res = ca.substitute(ca_rhs[0], ca_inputs[0], test_input_np)
        #
        # # Check the output
        # test_output_np = np.array(test_output.tolist())
        # for idx, val in enumerate(test_output_np.flatten()):
        #     self.assertAlmostEqual(val, float(res[idx]))

    def test_complex_model(self):

        inputs, outputs = get_system(get_model_path("complex.onnx"))

        # TODO: why is output not a list? Gemm,tanh?
        x_dot_fun = ca.Function('x_dot_fun', inputs, outputs)

        x = np.array(random_sequence(100))
        u = np.array(random_sequence(100))
        #t = np.array(random_sequence(100))

        input_val = np.column_stack((x, u))
        ca_results = []
        num_outputs = len(outputs)

        model = torch.load(get_model_path("complex.pt"))
        model.eval()

        exp_results = model(torch.from_numpy(input_val).float())
        for i in range(0, x.size):
            ca_result = x_dot_fun.call(input_val[i])[0].elements()
            ca_results.append(ca_result)

        for ca_result, exp_result in zip(ca_results, exp_results):
            for i in range(0, len(ca_result)):
                self.assertAlmostEqual(ca_result[i],
                                       exp_result[i].detach().numpy(),
                                       num_outputs)

    def test_keras_model(self):
        #netron.start(get_model_path("new.onnx"))
        #inputs, outputs = import_from_onnx(get_model_path("new.onnx"))
        inputs, outputs = get_system(get_model_path("new.onnx"))

        x_dot_fun = ca.Function('x_dot_fun', inputs, outputs)
        a = np.array(random_sequence(100))
        b = np.array(random_sequence(100))

        input_val = np.column_stack((a, b))
        ca_results = []
        num_outputs = len(outputs)
        model = load_model(get_model_path("new.keras"))
        exp_results = model.predict(input_val)

        for i in range(0, a.size):
            ca_result = x_dot_fun.call(input_val[i])[0].elements()
            ca_results.append(ca_result)

        for ca_result, exp_result in zip(ca_results, exp_results):
            for i in range(0, len(ca_result)):
                self.assertAlmostEqual(ca_result[i],
                                       exp_result[i],
                                       num_outputs)

    def test_torch_model(self):
        # netron.start(self.path_pytorch_onnx)
        inputs, outputs = get_system(get_model_path("pytorch.onnx"))

        # TODO: why is output not a list? Gemm,tanh?
        x_dot_fun = ca.Function('x_dot_fun', inputs, outputs)

        x = np.array(random_sequence(100))
        u = np.array(random_sequence(100))
        t = np.array(random_sequence(100))
        input_val = np.column_stack((x, u, t))
        ca_results = []
        num_outputs = len(outputs)

        model = torch.load(get_model_path("pytorch.pt"))
        model.eval()

        exp_results = model(torch.from_numpy(input_val).float())
        for i in range(0, x.size):
            ca_result = x_dot_fun.call(input_val[i])[0].elements()
            ca_results.append(ca_result)

        for ca_result, exp_result in zip(ca_results, exp_results):
            for i in range(0, len(ca_result)):
                self.assertAlmostEqual(ca_result[i],
                                       exp_result[i].detach().numpy(),
                                       num_outputs)

    def test_keras_vgf2_model(self):
        #netron.start(get_model_path("new.onnx"))
        #inputs, outputs = import_from_onnx(get_model_path("new.onnx"))
        inputs, outputs = get_system(get_model_path("second_vgf.onnx"))

        x_dot_fun = ca.Function('x_dot_fun', inputs, outputs)
        x = rnd_matrix((100, 6))
        u = rnd_matrix((100, 2))


        input_val = np.column_stack((x,u))
        ca_results = []
        num_outputs = len(outputs)
        model = load_model(get_model_path("second_vgf.h5"))
        exp_results = model.predict(input_val)

        for i in range(0, 100):
            ca_result = x_dot_fun.call([x[i],u[i]])[0].elements()
            ca_results.append(ca_result)

        for ca_result, exp_result in zip(ca_results, exp_results):
            for i in range(0, len(ca_result)):
                self.assertAlmostEqual(ca_result[i],
                                       exp_result[i],
                                       num_outputs)

    def test_keras_vgf_1_model(self):
        #netron.start(get_model_path("new.onnx"))
        #inputs, outputs = import_from_onnx(get_model_path("new.onnx"))
        inputs, outputs = get_system(get_model_path("first_vgf.onnx"))

        x_dot_fun = ca.Function('x_dot_fun', inputs, outputs)
        x = rnd_matrix((100, 5))
        u = rnd_matrix((100, 5))


        input_val = np.column_stack((x,u))
        ca_results = []
        num_outputs = len(outputs)
        model = load_model(get_model_path("first_vgf.h5"))
        exp_results = model.predict(input_val)

        for i in range(0, 100):
            ca_result = x_dot_fun.call([x[i],u[i]])[0].elements()
            ca_results.append(ca_result)

        for ca_result, exp_result in zip(ca_results, exp_results):
            for i in range(0, len(ca_result)):
                self.assertAlmostEqual(ca_result[i],
                                       exp_result[i],
                                       num_outputs)

    @unittest.skip("kaputt")
    def test_mnist_classification_model(self):
        #netron.start(get_model_path("mnist_classification.onnx"))
        sess = rt.InferenceSession(get_model_path("mnist_classification.onnx"))
        input_name = sess.get_inputs()[0].name
        #matrix = rnd_matrix((28,28)).astype(np.float32)
        matrix = np.full((28,28),-1, dtype = np.float32)
        input_val = np.array(matrix).reshape((1,1,28,28))
        exp_result = sess.run(None, {input_name: input_val})[0][0]
        #exp_result = exp_result[0][0]
        # netron.start(self.path_complex_onnx)
        inputs, output = get_system(get_model_path("mnist_classification.onnx"))
        inputs = inputs[0][0][0]
        # TODO: why is output not a list? Gemm,tanh?

        x_dot_fun = ca.Function('x_dot_fun', inputs.elements(), output) # inputs shape: 1x1x28x28
        input_for_ca = ca.SX(matrix).elements()
        ca_result = x_dot_fun.call(input_for_ca)[0].elements()
        #ca_result = x_dot_fun.call(input_val[0][0])[0].elements()
        self.assertSequenceEqual(ca_result, exp_result.tolist())


def get_model_path(model_name):
    file_dir = os.path.abspath(os.path.dirname(__file__))
    model_dir = os.path.join(file_dir, ".test_models")
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)
    model_path = os.path.join(model_dir, model_name)
    return model_path
