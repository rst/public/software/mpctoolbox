import random

import numpy as np


def rnd_matrix(shape=(2, 2), mult=1, offset=0):
    length = 1
    for number in shape:
        length = length * number
    r = random_sequence(length, mult, offset)
    A = np.array(r).reshape(shape)
    # res = ca.SX(A)
    # return res
    return A


def random_sequence(length, mult=1, offset=0):
    sequence = []
    for _ in range(length):
        sequence.append(mult * random.uniform(0, 1) + offset)
    return sequence