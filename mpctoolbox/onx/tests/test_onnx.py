import unittest
import torch


class NetComplex(torch.nn.Module):
    def __init__(self):
        super(NetComplex, self).__init__()

    def forward(self, x):
        y = torch.sin(x**2)+x
        z = torch.pow((y-x), 0.5)+y
        return z


if __name__ == '__main__':
    unittest.main()
