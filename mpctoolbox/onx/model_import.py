import os
import sys

import onnx
import netron

from .verifier import (
    has_valid_metadata, is_consistend_to_metadata, is_adaptable
)
from .parser import import_from_onnx
from .adapter import adapt


def get_system(path):
    """
    Transforms ONNX-Model to Casadi function.
    """
    # TODO get rid of static paths
    path_adapted_models = "mpctoolbox/onnx/adapted_models/"
    onnx_model = onnx.load(path)
    onnx.checker.check_model(onnx_model)
    onnx.checker.check_model(onnx_model)
    netron.start(path)
    # ver.check_metadata_var_names(onnx_model)
    has_valid_metadata(onnx_model)
    if is_consistend_to_metadata(onnx_model):
        ca_inputs, ca_outputs = import_from_onnx(onnx_model)
    elif is_adaptable(onnx_model):
        print_consistency_error()

        # build path for adapted model
        model_fname = os.path.basename(path)
        name = model_fname.replace(".onnx", "")
        new_path = path_adapted_models + name + "_adapted.onnx"
        if os.path.exists(new_path):
            print("Adapted Model of ONNX-File ", model_fname,
                  " already exists. Existing model (see netron visualization) will be used to create the CasadiFunction.")
            netron.start(new_path)
            adapted_model = onnx.load(new_path)
        else:
            adapted_model = adapt(onnx_model)
            onnx.save_model(adapted_model, new_path)
            netron.start(new_path)
            user_in = input(
                "use adapted model(see netron) to create the CasadiFunction? Y/N:")
            if user_in == "N":
                sys.exit()
        ca_inputs, ca_outputs = import_from_onnx(adapted_model)
    else:
        print_consistency_error()
        print_model_adaption_error()
        netron.start(path)
        raise ValueError()
    return ca_inputs, ca_outputs


def print_consistency_error():  # input_transform     List of Tuples
    print("""
        ONNX Model is not consistend to its meta information. 
        Required key-value pairs:

        key                 value
        input_tuple         List of Tuples -> [('input1', in_dim1), ('input2', in_dim2),...]
        output_tuple        List of Tuples -> [('output1', out_dim1), ('output2', out_dim),...]
        
        Example:
        input_tuple:        [('x', 1), ('u', 1), ('t', 1)]
        output_tuple:       [('x_dt', 5)]"
        """)


def print_model_adaption_error():
    print("""     
        Adapting the ONNX Model to metadata information failed.
        The ONNX Model is adaptable by MPCTools, if there is only one input and one output and
        the sum of sizes of model inputs/outputs in the metadata equals the size of the model input/output""")
