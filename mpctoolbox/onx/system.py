import logging
import casadi as ca

import onnx

from mpctoolbox.core.system import System
from .model_import import get_system
from .parser import get_transformations, get_input_names


logger = logging.getLogger(__name__)


class ONNXSystem(System):
    """
    Wrapper class to provide the system dynamics based on an ONNX graph.

    Args:
        model_path (str): Path to .onnx-file to use.
    """
    def __init__(self, model_path):
        # input transforms and input tuple must be in same order
        means, variances, means_out, variances_out =\
            get_transformations(model_path)

        model = onnx.load_model(model_path)
        input_names = get_input_names(model)
        inputs, outputs = get_system(model_path)

        sym_dict = {}
        for idx, input_name in enumerate(input_names):
            if input_name in self.mandatory_symbols:
                val = ca.transpose(inputs[idx])
                sym_dict[input_name] = val
                if means is not None:
                    setattr(self, "means_" + input_name, means[idx])
                if variances is not None:
                    setattr(self, "variances_" + input_name, means[idx])
            if input_name == "t":
                logger.error("ONNX-Models with time as input are not "
                             "implemented yet.")
            if input_name == "p":
                logger.warning("ONNX-Models with parameters are not tested "
                               "yet.")

        # transform knn outputs back to reality
        if means_out is not None:
            outputs = self.S(outputs, means_out, variances_out)
        x_dot_p = ca.transpose(outputs[0])

        # call base class
        super().__init__(x_dot_p, sym_dict)

    @staticmethod
    def T(x, means, variances):
        z = ca.SX.sym("x_z", x.shape)
        for i in range(x.shape[1]):
            z[i] = (x[i]-means[i])/variances[i]
        return z

    @staticmethod
    def S(f_transformed, means, variances):
        f = ca.SX.sym("f", f_transformed.shape)
        for i in range(f.shape[1]):
            f[i] = (f_transformed[i]*variances[i])+means[i]
        return f