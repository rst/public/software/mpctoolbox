==========
mpctoolbox
==========

A frontend for casadi-mpctools to do mpc on a variety of models developed
at the Institute of Control Theory (RST_) at the TU Dresden, Germany.

The focus of this toolbox is the seamless application of the wonderful
Casadi-based ``mpctools`` to all possible dynamic models, including models built
with Sympy, Tensorflow, Pytorch, or others.

To do so, this package introduces a fine grained distinction between the different
parts of a model predictive control setup.

**Disclaimer: This toolbox is research software and will therefore probably
never reach a 'stable' state.**

Installation
------------
Pretty much basic, the cleanest way is to create a venv by issuing::

    python -m venv ENV_NAME

and after activation (`source ENV_PATH/bin/activate`)::

    pip install -r requirements.txt

Lastly, ``mpc-tools-casadi`` has to be installed, get it from here_,
clone and follow the given instructions.
After you have those installed, run::

    pip install .

and you are good to go. Depending on the use case, you may also install the
optional dependencies via::

    pip install ".[docs]"

if you want to build the docs locally or::

    pip install ".[ann]"

if you want full support for the in- and export of onnx graphs.


First steps
-----------
To get a feeling how the toolbox is supposed to work, have a look at the getting
started guide in the docs or have a look on the ``examples`` folder.
If the usage of some class should be unclear, the unit tests (``test`` folder in
each module) may also be helpful.


Is there any documentation?
---------------------------
Yes there is, even if it is quite sparse at the moment.
Go to the ``docs`` folder and run::

    make html

or::

    ./make.bat /html

depending on your os. This should create the html docs.


Contributors
------------
A special thanks to all authors that contributed their code and time so far:

* Patrick Rüdiger (Initial code basis)
* Tom Buchwald (ONNX Interface for the usage of ANNs as systems)


.. _RST: https://tud.de/rst
.. _here: https://bitbucket.org/rawlings-group/mpc-tools-casadi/downloads/
